<!doctype html>
<html lang="pt-br">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- cor barra mobile -->
        <meta name="msapplication-navbutton-color" content="#343a40">
        <meta name="apple-mobile-web-app-status-bar-style" content="#343a40">
        <meta name="theme-color" content="#343a40">
        <!-- icone site -->
        <link rel="shortcut icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_url')?>/img/favicon.png" />
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?php bloginfo('template_url')?>/css/bootstrap.css" type="text/css">
        <!-- fancybox -->
        <link rel="stylesheet" href="<?php bloginfo('template_url')?>/css/fancybox.css" type="text/css">
        <!-- CSS personalizado -->
        <link rel="stylesheet" href="<?php bloginfo('template_url')?>/css/style.css" type="text/css" />
        <!-- font awesome -->
        
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

        <?php wp_head(); ?>
        
        <title>Sala Jaú</title>
    </head>
    <?php $imagem = wp_get_attachment_image_src( get_post_thumbnail_id ( $post->ID ), 'full');?>
<body>

<div class="container-fluid">
<div id="absolute" class="fundo-artigo" style="background-image:url('<?php echo $imagem[0]?>');"></div>
</div>
<div class="container-fluid">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark py-5">
            <a class="navbar-brand p-0" href="/" data-linktrack="true" data-tracklinktext="">
                <img src="<?php bloginfo('template_url')?>/img/logo.png" class="img-fluid" alt="">
            </a>
            <a href="#!" target="_blank"><img src="<?php bloginfo('template_url')?>/img/facebook.png" class="img-fluid mr-2" alt=""></a>
            <a href="#!" target="_blank"><img src="<?php bloginfo('template_url')?>/img/instagram.png" class="img-fluid" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/" class="nav-link text-uppercase text-white <?php if(is_front_page()) { echo 'active'; };?>">Home</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/a-sala-jau" class="nav-link text-uppercase text-white <?php if(is_page('a-sala-jau')) { echo 'active'; };?>">A Sala Jaú</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/atividades" class="nav-link text-uppercase text-white <?php if(is_page('atividades')) { echo 'active'; };?>">Atividades</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/curso" class="nav-link text-uppercase text-white <?php if(is_page('cursos')) { echo 'active'; };?>">Cursos</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/professores" class="nav-link text-uppercase text-white <?php if(is_page('professores')) { echo 'active'; };?>">Professores</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/blog" class="nav-link text-uppercase text-white <?php if( is_single() && 'cursos' != get_post_type() || 'atividades' != get_post_type() ) { echo 'active'; };?>">Blog</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/contato" class="nav-link text-uppercase text-white <?php if(is_page('contato')) { echo 'active'; };?>">Contato</a></li>
                </ul>
            </div>
        </nav>
        <div class="container text-white text-uppercase small">
            <?php echo do_shortcode('[flexy_breadcrumb]');?>
        </div>
    </div>
    
</div>
        <!-- CONTEUDO -->
        <?php //if (have_posts()) : while (have_posts()) : the_post();  ?>
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <div class="col-md-8 col-12 py-4">
                            <h3 class="text-white mb-4 d-none d-md-block"><?php the_title(); ?></h3>
                            <h4 class="text-white mb-4 d-block d-md-none"><?php the_title(); ?></h4>
                            <span class="mb-3 text-white"><?php the_date(); ?></span>
                        </div>
                </div>  
                <div class="row mt-5">
                    <div class="col-md-3 col-12 mt-5 mt-md-4 py-4 d-none d-md-block">
                        <h6 class="cor-principal my-5">Compartilhe</h6>
                        <div class="row">
                            <div class="col-12 mb-3">
                                <a target="_blank" class="" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"><img src="<?php bloginfo('template_url');?>/img/face-artigo.jpg" class="img-fluid" alt=""></a>
                            </div>
                            <div class="col-12 mb-3">
                                <a target="_blank" class="" href="https://twitter.com/"><img src="<?php bloginfo('template_url');?>/img/twitter-artigo.jpg" class="img-fluid" alt=""></a>
                            </div>
                            <div class="col-12 mb-3">
                                <a target="_blank" class="" href="https://api.whatsapp.com/send?text=<?php the_title();?> - <?php echo get_the_excerpt(); ?> - <?php the_permalink();?>"><img src="<?php bloginfo('template_url');?>/img/whatsapp-artigo.jpg" class="img-fluid" alt=""></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-12 mt-5 mt-md-4 py-md-4 py-2">
                        <div class="mt-5" id="post-<?php the_ID(); ?>">
                            <div class="w-100 d-block my-2">
                                <p><?php  echo wpautop(get_the_content(null, false)); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-12 d-block d-md-none">
                        <h6 class="cor-principal mb-3">Compartilhe</h6>
                        <div class="row">
                            <div class="col-12 mb-3">
                                <img src="<?php bloginfo('template_url');?>/img/face-artigo.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="col-12 mb-3">
                                <img src="<?php bloginfo('template_url');?>/img/twitter-artigo.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="col-12 mb-3">
                                <img src="<?php bloginfo('template_url');?>/img/whatsapp-artigo.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                    </div>
                    <?php
                    endwhile;
                endif;
                ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    //endwhile; endif;
    ?>
<!-- rodape do site -->
<?php get_footer(); ?>
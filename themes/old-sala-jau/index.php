<?php get_header(); ?>
<!-- conteudo -->
    <?php if( is_front_page() ): ?>
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-12 py-4">
                    <!-- <img src="<?php bloginfo('template_url');?>/img/banner.png" class="img-fluid" alt=""> -->
                    <?php putRevSlider("home") ?>
                </div>
                <div class="col-12 py-3">
                    <hr class="pb-4">
                </div>
                <?php 
                    global $post;
                    $args = array( 'post_type' => 'conteudo_home');
                    $the_query = new WP_Query( $args ); 
                    if ($the_query->have_posts()) : $the_query->the_post();
                            $link_home = get_post_meta($post->ID, 'Link home', true);
                            $imagem_home = wp_get_attachment_image_src( get_post_thumbnail_id ( $post->ID ), 'full'); 
                ?>
                    <div class="col-md-6 col-12">
                        <img src="<?php echo $imagem_home[0]; ?>" class="img-fluid" alt="">
                    </div>
                    <div class="col-md-6 col-12">
                        <h2 class="cor-principal mb-4"><?php the_title();?></h2>
                        <p class="mb-5"><?php echo $post->post_content; ?></p>
                        <a href="<?php echo $link_home; ?>" class="border-cor-principal btn-post-destaque btn-vermelho h5">Saiba mais <span class="seta"></span></a>
                    </div>
                <?php
                    endif;
                ?>
                <div class="col-12 pt-5 pb-4">
                    <hr />
                </div>
                <div class="col-12">
                    <h2 class="cor-principal float-left m-0"><a href="<?php bloginfo('wpurl')?>/cursos" style="color:#fc4513;">Cursos</a></h2>
                    <ul class="nav nav-tabs float-right border-0 mt-45" id="myTab" role="tablist">
                        <li class="nav-item border-0">
                        <a class="nav-link text-uppercase active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Recentes</a>
                        </li>
                        <li class="nav-item border-0 d-none">
                        <a class="nav-link text-uppercase" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Literatura</a>
                        </li>
                        <li class="nav-item border-0 d-none">
                        <a class="nav-link text-uppercase" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Filosofia</a>
                        </li>
                        <li class="nav-item border-0 d-none">
                            <a class="nav-link text-uppercase" id="history-tab" data-toggle="tab" href="#history" role="tab" aria-controls="contact" aria-selected="false">História</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 pt-5">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row">
                            <?php 
                                global $post;
                                $args = array( 'post_type' => 'cursos', 'posts_per_page' => 1 );
                                $the_query = new WP_Query( $args ); 
                                $cats = get_terms( array(
                                    'taxonomy' => 'disciplinas',
                                ) );
                                $name = $cat->name;
                                if ($the_query->have_posts()) : 
                                    while ($the_query->have_posts()) : $the_query->the_post();
                                        $terms = wp_get_post_terms( $post->ID, 'disciplinas');
                                        $professor2 = get_post_meta($post->ID, 'Professor', true);
                                        $data = get_post_meta($post->ID, 'Data', true);
                                        $pagseguro = get_post_meta($post->ID, 'Pagseguro', true);
										$valor = get_post_meta($post->ID, 'Valor', true);
										$valor_exaluno = get_post_meta($post->ID, 'Valor_exaluno', true);
										$valor_estudante = get_post_meta($post->ID, 'Valor_estudante', true);
                                        $imagem2 = wp_get_attachment_image_src( get_post_thumbnail_id ( $post->ID ), 'full'); 
                                        if ( !is_sticky() ): ?>

                                            
                                <div class="col-md-8 col-12">
                                    <img src="<?php echo $imagem2[0] ?>" class="img-fluid" alt="">
                                </div>
                                <div class="col-md-4 col-12">
                                    <div class="border-cor-principal py-4 px-md-5 px-3 h-100 d-inline-block">
                                        <p class="small font-weight-bold"><?php echo $terms[0]->name ?></p>
                                        <h5 class="cor-principal"><?php the_title()?></h5>
                                        <p class="small">Com: <?php echo $professor2;?></p>
                                        <hr>
                                        <h6 class="cor-principal titulo-cursos">Início: <?php echo $data; ?></h6>
                                        <hr>
                                        <div class="row">
                                            <div class="col-6 p-1">
                                                <a href="<?php the_permalink() ?>" class="btn w-100 d-inline-block border-cor-principal  btn-vermelho p-2 rounded-0"><span class="data"></span> Informações</a>
                                            </div>
											<form id="comprar" action="https://pagseguro.uol.com.br/checkout/v2/payment.html" method="post" onsubmit="PagSeguroLightbox(this); return false;">
							<input type="hidden" name="code" id="code" value="" />
							<input type="hidden" name="itemAmount1" id="itemAmount1" value="<?php echo $valor; ?>">
							<input type="hidden" name="itemAmount2" id="itemAmount2" value="<?php echo $valor_exaluno; ?>">
							<input type="hidden" name="itemAmount3" id="itemAmount3" value="<?php echo $valor_estudante; ?>">
							<input type="hidden" name="itemDescription1" id="itemDescription1" value="<?php echo the_title()?>">
							</form>
							<script type="text/javascript" src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>
                                            <div class="col-6 p-1">
                                                <a data-curso="<?php the_title(); ?>" data-href="" class="btn bg-verde w-100 d-inline-block text-white p-2 rounded-0 btn-pagamento" data-toggle="modal" data-target="#inscrevase"><img src="<?php bloginfo('template_url')?>/img/icon-inscricao.png" class="mr-2" style="margin-top: -5px;" alt="">Inscreva-se</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                            
                                <?php 
                                        endif; 
                                    endwhile; 
                                endif;
                                ?>
                                
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">2</div>
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">3</div>
                        <div class="tab-pane fade" id="history" role="tabpanel" aria-labelledby="history-tab">4</div>
                    </div>
                </div>
                <div class="col-12 mt-4">
                    <div class="row">
                        <?php
                            $postslist = get_posts( array(
                                'numberposts'      => 3,
                                'offset' => 1,
                                'orderby'        => 'date',
                                'post_type'        => 'cursos'
                            ) );
                            
                            if ( $postslist ) {
                                foreach ( $postslist as $post ) :
                                    $terms2 = wp_get_post_terms( $post->ID, 'disciplinas');
                                    $data_abreviada = get_post_meta($post->ID, 'Data_abreviada', true);
                                    $professor = get_post_meta($post->ID, 'Professor', true);
                                    setup_postdata( $post );
                                    $imagem5 = wp_get_attachment_image_src( get_post_thumbnail_id ( $post->ID ), 'full');
                                    ?>
                        <div class="col-md-4 col-12 mb-3">
                            <div class="destaques w-100 d-block px-4 pt-4 position-relative" style="background-image: url('<?php echo $imagem5[0]; ?>'); background-size:cover;">
                                <div class="row">
                                    <div class="col-5">
                                        <p class="text-white mt-2"><strong><?php echo $terms2[0]->name ?></strong></p>
                                    </div>
                                    <div class="col-7 text-right">
                                        <img src="<?php bloginfo('template_url')?>/img/seta-data.png" class="mr-2" alt="">
                                        <span class="h4 text-white"><?php echo $data_abreviada; ?></span>
                                    </div>
                                    <div class="col-12 position-absolute pb-5 pr-4" style="bottom:0">
                                        <h5 class="text-white musica"><?php the_title()?></h5>
                                        <p class="text-white small">com <?php echo $professor;?></p>
                                        <a href="<?php the_permalink()?>" class="btn rounded-0 text-white border-white btn-musica bg-transparent py-2 px-4">Saiba mais</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                            endforeach; 
                            wp_reset_postdata();
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid bg-linha-escura py-5 mt-5">
        <div class="container pb-md-5">
            <div class="row pb-md-5">
                <div class="col-12 mb-4">
                    <h2 class="text-white"><a href="<?php bloginfo('wpurl')?>/atividades" class="text-white">Atividades</a></h2>
                </div>
                <div class="col-12 mb-4">
                <?php 
                    global $post;
                    $args = array( 'post_type' => 'atividades', 'posts_per_page' => 1 );
                    $the_query = new WP_Query( $args ); 
                    $cats = get_terms( array(
                        'taxonomy' => 'visita',
                    ) );
                    $name = $cat->name;
                    if ($the_query->have_posts()) : 
                        while ($the_query->have_posts()) : $the_query->the_post();
                            $terms3 = wp_get_post_terms( $post->ID, 'visita');
                            $professor3 = get_post_meta($post->ID, 'Professor', true);
                            $imagem4 = wp_get_attachment_image_src( get_post_thumbnail_id ( $post->ID ), 'full'); 
                            if ( !is_sticky() ): ?>

                                <div class="atividades w-100 d-block position-relative">
                                    <img src="<?php echo $imagem4[0]; ?>" class="img-fluid" alt="">
                                    <div class="atividades">
                                        <div class="bg-cor-principal atividades-card text-white p-3 mb-4">
                                            <p class="text-white mt-2"><strong><?php echo $terms3[0]->name ?></strong></p>
                                            <h5 class="text-white"><?php the_title()?></h5>
                                            <div class="col-md-8 col-12 float-left">
                                                <p class="text-white small">com <?php echo $professor3; ?></p>
                                            </div>
                                            <div class="col-md-4 col-12 float-left text-right">
                                                <a href="<?php echo the_permalink()?>"><img src="<?php bloginfo('template_url')?>/img/icon-seta-branca.png" class="img-fluid mt-2" alt=""></a>
                                            </div>
                                        </div>
                                        <a href="<?php bloginfo('wpurl')?>/atividades" class="btn rounded-0 text-white border-white float-right btn-atividades bg-transparent py-2 px-4">Ver todas as atividades</a>
                                    </div>
                                </div>

                    <?php 
                            endif; 
                        endwhile; 
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>















    <div class="container-fluid bg-linha-vermelha py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                    <?php
                        $postslist = get_posts( array(
                            'numberposts'      => 2,
                            'orderby'        => 'date'
                        ) );
                        
                        if ( $postslist ) {
                            foreach ( $postslist as $post ) :
                                setup_postdata( $post );
                                $imagem = wp_get_attachment_image_src( get_post_thumbnail_id ( $post->ID ), 'full');
                                ?>
                        <div class="col-12 mb-4">
                            <div class="img-post w-100" style="background-image: url('<?php echo $imagem[0]; ?>'); height:432px; background-size: cover;"></div>
                            <div class="w-100">
                                <a href="<?php the_permalink() ?>" class="w-100 float-left btn-art-destaques bg-white p-4">
                                    <span class="my-3 small helvetica"><?php the_date(); ?></span> - <span class="helvetica small">por <?php the_author()?></span></p>
                                    <h6 class="mb-3"><?php echo the_title()?></h6>
                                </a>
                            </div>
                        </div>
                            <?php
                            endforeach; 
                            wp_reset_postdata();
                        }
                        ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">

                        <div class="col-12 mb-4">
                            <div class="text-white border-white bg-transparent text-center p-5">
                                <h3 class="py-5"><a href="<?php bloginfo('wpurl')?>/blog" class="text-white">Blog</a></h3>
                            </div>
                        </div>

                        <?php
                        $postslist = get_posts( array(
                            'numberposts'      => 1,
                            'offset' => 2,
                            'orderby'        => 'date'
                        ) );
                        
                        if ( $postslist ) {
                            foreach ( $postslist as $post ) :
                                setup_postdata( $post );
                                $imagem = wp_get_attachment_image_src( get_post_thumbnail_id ( $post->ID ), 'full');
                                ?>
                        <div class="col-12 mb-4">
                            <div class="img-post w-100" style="background-image: url('<?php echo $imagem[0]; ?>'); height:432px; background-size: cover;"></div>
                            <div class="w-100">
                                <a href="<?php the_permalink() ?>" class="w-100 float-left btn-art-destaques bg-white p-4">
                                    <span class="my-3 small helvetica"><?php the_date(); ?></span> - <span class="helvetica small">por <?php the_author();?></span></p>
                                    <h6 class="mb-3"><?php echo the_title()?></h6>
                                </a>
                            </div>
                        </div>
                            <?php
                            endforeach; 
                            wp_reset_postdata();
                        }
                        ?>

 <div class="col-12 mt-5 pt-4 mb-4 text-center">
                            <a href="<?php bloginfo('wpurl')?>/blog" class="text-white h4 btn-artigos p-5 border-white bg-transparent w-100 float-left">Veja todos os artigos</a>
                        </div>                       

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">

                        <?php
                            $postslist = get_posts( array(
                                'numberposts'      => 2,
                                'offset' => 3,
                                'orderby'        => 'date'
                            ) );
                            
                            if ( $postslist ) {
                                foreach ( $postslist as $post ) :
                                    setup_postdata( $post );
                                    $imagem = wp_get_attachment_image_src( get_post_thumbnail_id ( $post->ID ), 'full');
                                    ?>
                             <div class="col-12 mb-4">
                            <div class="img-post w-100" style="background-image: url('<?php echo $imagem[0]; ?>'); height:432px; background-size: cover;"></div>
                            <div class="w-100">
                                <a href="<?php the_permalink() ?>" class="w-100 float-left btn-art-destaques bg-white p-4">
                                    <span class="my-3 small helvetica"><?php the_date(); ?></span> - <span class="helvetica small">por <?php the_author();?></span></p>
                                    <h6 class="mb-3"><?php echo the_title()?></h6>
                                </a>
                            </div>
                        </div>
                                <?php
                                endforeach; 
                                wp_reset_postdata();
                            }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <?php else:
        while(have_posts()):
            the_post();
            the_content();
        endwhile;
    endif; ?>
    
    
<!-- fim conteudo -->
<?php get_footer();?>
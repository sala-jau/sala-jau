<!doctype html>
<html lang="pt-br">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- cor barra mobile -->
        <meta name="msapplication-navbutton-color" content="#343a40">
        <meta name="apple-mobile-web-app-status-bar-style" content="#343a40">
        <meta name="theme-color" content="#343a40">
        <!-- icone site -->
        <link rel="shortcut icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_url')?>/img/favicon.png" />
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?php bloginfo('template_url')?>/css/bootstrap.css" type="text/css">
        <!-- fancybox -->
        <link rel="stylesheet" href="<?php bloginfo('template_url')?>/css/fancybox.css" type="text/css">
        <!-- CSS personalizado -->
        <link rel="stylesheet" href="<?php bloginfo('template_url')?>/css/style.css" type="text/css" />
        <!-- font awesome -->
        
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
				<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>
function enviaPagseguro(){
$.post('<?php bloginfo('template_url')?>/pagseguro.php',{itemDescription1:$("input[name='itemDescription1']").val(), itemAmount1:$("input[name='itemAmount1']").val()},function(data){
$('#code').val(data);
$('#comprar').submit();
})
}
</script>

        <?php wp_head(); ?>
        
        <title>Sala Jaú</title>
    </head>
    <?php $imagem = wp_get_attachment_image_src( get_post_thumbnail_id ( $post->ID ), 'full');?>
<body class="bg-site">
<div class="container-fluid">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark py-5">
            <a class="navbar-brand p-0" href="<?php bloginfo('wpurl')?>/" data-linktrack="true" data-tracklinktext="">
                <img src="<?php bloginfo('template_url')?>/img/logo.png" class="img-fluid" alt="">
            </a>
            <a href="#!" target="_blank"><img src="<?php bloginfo('template_url')?>/img/facebook.png" class="img-fluid mr-2" alt=""></a>
            <a href="#!" target="_blank"><img src="<?php bloginfo('template_url')?>/img/instagram.png" class="img-fluid" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/" class="nav-link text-uppercase text-white <?php if(is_front_page()) { echo 'active'; };?>">Home</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/a-sala-jau" class="nav-link text-uppercase text-white <?php if(is_page('a-sala-jau')) { echo 'active'; };?>">A Sala Jaú</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/atividades" class="nav-link text-uppercase text-white <?php if(is_singular( 'atividades' )) { echo 'active'; };?>">Atividades</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/cursos" class="nav-link text-uppercase text-white <?php if(is_page('cursos')) { echo 'active'; };?>">Cursos</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/professores" class="nav-link text-uppercase text-white <?php if(is_page('professores')) { echo 'active'; };?>">Professores</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/blog" class="nav-link text-uppercase text-white <?php if(is_category('blog')) { echo 'active'; };?>">Blog</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/contato" class="nav-link text-uppercase text-white <?php if(is_page('contato')) { echo 'active'; };?>">Contato</a></li>
                </ul>
            </div>
        </nav>
        <div class="container text-white text-uppercase small">
        <div class="fbc fbc-page">

            <div class="fbc-wrap">

                <ol class="fbc-items" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <span itemprop="name">
                            <a itemprop="item" href="http://salajau.com.br/novo">Home</a>
                        </span>
                        <meta itemprop="position" content="1"><!-- Meta Position-->
                    </li>
                    /<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                        <a itemprop="item" fbc-cursos"="" href="http://salajau.com.br/novo/cursos/" title="Cursos">
                        <span itemprop="name">Cursos</span></a><meta itemprop="position" content="2"></li>
                    /<li class="active" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemprop="item" href="#!" title="teste 4"><span itemprop="name" title="<?php the_title()?>"><?php the_title();?></span></a><meta itemprop="position" content="3"></li>					</ol>
                <div class="clearfix"></div>
            </div>
            </div>
        </div>
    </div>
    
</div>
        <!-- CONTEUDO -->
        <?php //if (have_posts()) : while (have_posts()) : the_post();  ?>
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                <div class="col-12 py-4">
                    <h1 class="text-white">Atividades</h1>
                </div>
                <div class="col-12 pt-3 pb-5">
                <?php //mostrar autores 
                global $post;
               $cats = get_terms( array(
                    'taxonomy' => 'visita',
                ) );
            ?> 
                </div>
                <?php //mostrar autores 
                    
                    if (have_posts()) : 
                            while (have_posts()) : the_post();
                            $terms = wp_get_post_terms( $post->ID, 'disciplinas');
                            $professor = get_post_meta($post->ID, 'Professor', true);
                            $semestre = get_post_meta($post->ID, 'Semestre', true);
                            $data = get_post_meta($post->ID, 'Data', true);
                            $data_abreviada = get_post_meta($post->ID, 'Data_abreviada', true);
                            $duracao = get_post_meta($post->ID, 'Duracao', true);
                            $dias = get_post_meta($post->ID, 'Dias', true);
                            $horario = get_post_meta($post->ID, 'Horario', true);
                            $valor = get_post_meta($post->ID, 'Valor', true);
							$valor_exaluno = get_post_meta($post->ID, 'Valor_exaluno', true);
							$valor_estudante = get_post_meta($post->ID, 'Valor_estudante', true);
							$parcelamento = get_post_meta($post->ID, 'Parcelamento', true);

                            $imagem2 = wp_get_attachment_image_src( get_post_thumbnail_id ( $post->ID ), 'full'); ?>
                        <div class="col-md-8 col-12" style="background-image: url('<?php echo $imagem2[0]; ?>'); background-size: cover;"></div>
                            <!-- <img src="<?php //echo $imagem2[0]; ?>" class="img-fluid" alt=""> -->

                        <div class="col-md-4 col-12">
                            <div class="border-cor-principal bg-white py-5 px-md-5 px-3 h-100 d-inline-block">
                                <p class="small font-weight-bold mb-4"><?php echo $terms[0]->name;?></p>
                                <h5 class="cor-principal"><?php echo the_title()?></h5> 
                                <div class="row">
                                    <div class="col-5 p-1" style="align-self: center;">
                                        <span class="small">Com: <?php echo $professor;?></span>
                                    </div>
                                    <div class="col-7 p-1">
                                        <a href="<?php bloginfo('wpurl')?>/professores" class="btn w-100 d-inline-block border-cor-principal  btn-vermelho p-2 rounded-0"><span class="curriculo"></span> Ver currículo</a>
                                    </div>
                                </div>                               
                                <hr>
                                <h6 class="cor-principal titulo-cursos">Início: <?php echo $data;?></h6>
                                <hr>
                            </div>
                        </div>
                </div>  
                <div class="row mt-5">
                    <div class="col-md-3 col-12 mt-45 mt-md-4 pr-5 d-none d-md-block mb-5">                
                        <div class="row">
                            <div class="col-12 mb-3 border-cor-principal p-4">
                                <h6 class="cor-principal mb-3">Informações</h6>
                                <p><span class="cor-principal font-weight-bold">Início:</span><span> <?php echo $data; ?></span></p>
                                <p><span class="cor-principal font-weight-bold">Duração:</span><span> <?php echo $duracao; ?></span></p>
                                <p><span class="cor-principal font-weight-bold">Dias:</span><span> <?php echo $dias; ?></span></p>
                                <p><span class="cor-principal font-weight-bold">Horário:</span><span> <?php echo $horario; ?></span></p>
                                <p><span class="cor-principal font-weight-bold">Inscrição Gratuita</span></p>
								
                            </div>
																			   
                            <div class="col-12 p-0">
								<form id="comprar" action="https://pagseguro.uol.com.br/checkout/v2/payment.html" method="post" onsubmit="PagSeguroLightbox(this); return false;">
							<input type="hidden" name="code" id="code" value="" />
							<input type="hidden" name="itemAmount1" id="itemAmount1" value="<?php echo $valor; ?>">
							<input type="hidden" name="itemAmount2" id="itemAmount2" value="<?php echo $valor_exaluno; ?>">
							<input type="hidden" name="itemAmount3" id="itemAmount3" value="<?php echo $valor_estudante; ?>">
							<input type="hidden" name="itemDescription1" id="itemDescription1" value="<?php echo the_title()?>">
							</form>
							<script type="text/javascript" src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>
																																			 
                                <a data-curso="<?php the_title(); ?>" data-href="#" class="btn bg-verde w-100 d-inline-block text-white p-2 rounded-0 btn-pagamento" data-toggle="modal" data-target="#inscrevaseatividade"><img src="<?php bloginfo('template_url')?>/img/icon-inscricao.png" class="mr-2" style="margin-top: -5px;" alt="">Inscreva-se</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-12 py-md-4 py-2 mb-5">
                        <div id="post-<?php the_ID(); ?>">
                            <div class="w-100 post-curso d-block my-2 mb-5">
                                <?php echo wpautop( $post->post_content ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-12 d-block d-md-none mb-5">
                        <div class="row">
                            <div class="col-12 mb-3 border-cor-principal p-4">
                                <h6 class="cor-principal mb-3">Informações</h6>
                                <p><span class="cor-principal font-weight-bold">Início:</span><span> <?php echo $data; ?></span></p>
                                <p><span class="cor-principal font-weight-bold">Duração:</span><span> <?php echo $duracao; ?></span></p>
                                <p><span class="cor-principal font-weight-bold">Dias:</span><span> <?php echo $dias; ?></span></p>
                                <p><span class="cor-principal font-weight-bold">Horário:</span><span> <?php echo $horario; ?></span></p>
                                <p><span class="cor-principal font-weight-bold">Inscrição Gratuita</span></p>
                            </div>
                            <div class="col-12 p-0">
                                <a data-curso="<?php the_title(); ?>" data-href="#" class="btn bg-verde w-100 d-inline-block text-white p-2 rounded-0 btn-pagamento" data-toggle="modal" data-target="#inscrevaseatividade"><img src="<?php bloginfo('template_url')?>/img/icon-inscricao.png" class="mr-2" style="margin-top: -5px;" alt="">Inscreva-se</a>
                            </div>
                        </div>
                    </div>
                    <?php
                    endwhile;
                endif;
                ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    //endwhile; endif;
    ?>
<!-- rodape do site -->
<?php get_footer(); ?>
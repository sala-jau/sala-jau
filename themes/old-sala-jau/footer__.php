<div class="container-fluid py-5 mt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2 col-12 border-cor-principal p-5">
                    <h3 class="cor-principal mb-3">Assine nossa Newsletter</h3>
                    <p class="mb-3">Saiba antes sobre cursos, palestras, artigos e atividade da Sala Jaú</p>
					
					
                    <!-- <form class="form-inline w-100 d-none">
                        <input type="text" class="form-control mb-2 mr-sm-2 rounded-0 w-75" id="" placeholder="E-mail">
                        <button type="submit" id="assinar" class="btn bg-cor-principal rounded-0 btn-assinar mb-2 px-4 text-white">Assinar</button>
                    </form> -->
                </div>
                <div class="col-12 py-4">
                    <div class="row py-4">
                        <div class="col-5 text-md-right text-center">
                            <img src="<?php bloginfo('template_url')?>/img/localizacao.jpg" class="img-fluid" alt="">
                        </div>
                        <div class="col-7 text-left">
                            <p class="mt-3 mb-0">Al. Ministro Rocha Azevedo 456, Cj 901</p>
                            <p class="m-0">São Paulo - SP</p>
                            <p class="m-0">Whatsapp - 11 972677465 - 11 996399386</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- rodape do site -->
<footer class="container-fluid bg-site">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark py-5">
            <a class="navbar-brand p-0" href="/" data-linktrack="true" data-tracklinktext=""">
                <img src="<?php bloginfo('template_url')?>/img/logo.png" class="img-fluid" alt="">
            </a>
            <a href="https://www.facebook.com/salajau.sp" target="_blank"><img src="<?php bloginfo('template_url')?>/img/facebook.png" class="img-fluid mr-2" alt=""></a>
            <a href="https://www.instagram.com/sala_jau/" target="_blank"><img src="<?php bloginfo('template_url')?>/img/instagram.png" class="img-fluid" alt=""></a>
            <button class="navbar-toggler d-none" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse d-none d-md-block" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/" class="nav-link text-uppercase text-white <?php if(is_front_page()) { echo 'active'; };?>">Home</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/a-sala-jau" class="nav-link text-uppercase text-white <?php if(is_page('a-sala-jau')) { echo 'active'; };?>">A Sala Jaú</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/atividades" class="nav-link text-uppercase text-white <?php if(is_page('atividades')) { echo 'active'; };?>">Atividades</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/cursos" class="nav-link text-uppercase text-white <?php if(is_page('cursos')) { echo 'active'; };?>">Cursos</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/professores" class="nav-link text-uppercase text-white <?php if(is_page('professores')) { echo 'active'; };?>">Professores</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/blog" class="nav-link text-uppercase text-white <?php if(is_category('blog')) { echo 'active'; };?>">Blog</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/contato" class="nav-link text-uppercase text-white <?php if(is_page('contato')) { echo 'active'; };?>">Contato</a></li>
                </ul>
            </div>
        </nav>

    </div>
</footer>

    <!-- Modal -->
<div class="modal fade" id="inscrevase" tabindex="-1" role="dialog" aria-labelledby="inscrevase" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content px-3">
      <div class="modal-header">
        <h5 class="modal-title cor-principal" id="inscrevase">Inscreva-se</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group mb-3">
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="customSwitch1">
                <label class="custom-control-label" for="customSwitch1">Já possuo cadastro</label>
            </div>
        </div>
        <p class="d-none">Entre em contato por e-mail ou whatsapp.</p>
        <p class="d-none"><a href="mailto:contato@salajau.com.br" class="cor-principal cor-principal-hover">contato@salajau.com.br<br /><a href="https://api.whatsapp.com/send?phone=5511972677465" class="cor-principal cor-principal-hover" target="_blank">11 972677465</a> - <a href="https://api.whatsapp.com/send?phone=5511996399386" class="cor-principal cor-principal-hover" target="_blank">11 996399386</a></p>
        <?php echo do_shortcode('[contact-form-7 id="181" title="Inscrição PagSeguro"]'); ?>
      </div>
    </div>
  </div>
</div>


<!-- jQuery primeiro, depois Popper.js e Bootstrap JS, por fim scripts adicionais -->
<script src="<?php bloginfo('template_url')?>/js/jquery.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url')?>/js/popper.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url')?>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url')?>/js/fancybox.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url')?>/js/script.js" type="text/javascript"></script>
<script>
    $('#assinar').click(function(){
        $(this).val('Concluído     ').removeClass('bg-cor-principal').addClass('btn-news')
    })

        $('#customSwitch1').click(function(){
            if($(this).is(":checked")){
                $('.cmp-hide').fadeOut()
            }
            else if($(this).is(":not(:checked)")){
                $('.cmp-hide').fadeIn()
            }
        });
</script>
<?php wp_footer();?>
</body>
</html>
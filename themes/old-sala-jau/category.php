<?php
/*
function listar_posts_tendencias() {
    global $wpdb;

    $sql = "select p.ID, p.post_title, p.post_excerpt, p.post_name, 
            (select guid from wp_posts p_2 inner join wp_postmeta m on p_2.ID=m.meta_value where m.meta_key='_thumbnail_id' and m.post_id=p.ID) as img_destacada 
            from wp_posts p 
            inner join wp_term_relationships  r on r.object_id=p.ID 
            inner join wp_terms t on r.term_taxonomy_id=t.term_id 
            where p.post_status='publish' 
            order by p.post_date desc";

    return $wpdb->get_results($sql);
}

$posts = listar_posts_tendencias();*/
?>
<!DOCTYPE html>
<!--[if IE 8]>               
<html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!-->

<html class="no-js" lang="pt">
<!--<![endif]-->

<head>
    <?php get_header(); ?>
</head>

<body>
    <!-- CONTEUDO -->
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-12 py-4">
                    <h1 class="text-white mb-4">Blog</h1>
                    <div class="row">
                        <div class="col-12">
                            <?php
                            if (have_posts()) : 
                                while (have_posts()) : the_post();
                                $imagem = wp_get_attachment_image_src( get_post_thumbnail_id ( $post->ID ), 'full');
                                    if ( is_sticky() ): 
                            ?>
                                        <div class="row">
                                            <!-- destaque -->
                                            <div class="col-12 py-4 artigo-destaque  overlay red position-relative" style="background-image: url('<?php echo $imagem[0]; ?>');">
                                                <div class="w-100 d-block position-absolute art">
                                                    <a href="<?php the_permalink(); ?>">
                                                        <h3 class="text-white d-none d-md-block"><?php the_title(); ?></h3>
                                                        <h6 class="text-white d-block d-md-none"><?php the_title(); ?></h6>
                                                        <p class="text-white mb-0"><small class="cor-secundaria text-white"><i class="far fa-calendar-alt"></i> <?php the_date(); ?></small> - por <?php the_author(); ?></p>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- fim destaque -->
                                        </div>
                            <?php 
                                    endif;
                                endwhile; 
                            endif;
                            ?>
                            <div class="row">
                            <?php 
                            if (have_posts()) : 
                                while (have_posts()) : the_post();
                                    $imagem2 = wp_get_attachment_image_src( get_post_thumbnail_id ( $post->ID ), 'full'); 
                                    if ( !is_sticky() ): ?>

                                        
                                    <div class="col-md-6 col-12 p-2 mt-3 mb-md-3 artigos position-relative" style="background-image: url('<?php echo $imagem2[0]; ?>');">
                                        <div class="w-100 d-block position-absolute art">
                                            <a href="<?php the_permalink(); ?>">
                                                <h4 class="text-white d-none d-md-block"><?php the_title(); ?></h4>
                                                <h6 class="text-white d-block d-md-none"><?php the_title(); ?></h6>
                                                <p class="text-white"><small class="text-white"><i class="far fa-calendar-alt"></i> <?php the_date(); ?></small> - por <?php the_author(); ?></p>
                                            </a>
                                        </div>
                                        
                                    </div>
                                        
                            <?php 
                                    endif; 
                                endwhile; 
                            endif;
                            ?>
                            </div>
                            <div class="col-12 d-inline-block p-4 text-center">
                                <?php previous_posts_link(); ?><?php next_posts_link(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php get_footer(); ?>
        <?php wp_footer(); ?>

</body>

</html>
<!doctype html>
<html lang="pt-br">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- cor barra mobile -->
        <meta name="msapplication-navbutton-color" content="#343a40">
        <meta name="apple-mobile-web-app-status-bar-style" content="#343a40">
        <meta name="theme-color" content="#343a40">
        <!-- icone site -->
        <link rel="shortcut icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_url')?>/img/favicon.png" />
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?php bloginfo('template_url')?>/css/bootstrap.css" type="text/css">
        <!-- fancybox -->
        <link rel="stylesheet" href="<?php bloginfo('template_url')?>/css/fancybox.css" type="text/css">
        <!-- CSS personalizado -->
        <link rel="stylesheet" href="<?php bloginfo('template_url')?>/css/style.css" type="text/css" />
        <!-- font awesome -->
        
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
		
		<script>
function enviaPagseguro(){
$.post('<?php bloginfo('template_url')?>/pagseguro.php',{itemDescription1:$("input[name='itemDescription1']").val(), itemAmount1:$("input[name='itemAmount1']").val()},function(data){
$('#code').val(data);
$('#comprar').submit();
})
}
</script>

        <?php wp_head(); ?>
        
        <title>Sala Jaú</title>
    </head>
<body class="bg-site">
<div class="container-fluid">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark py-5">
            <a class="navbar-brand p-0" href="<?php bloginfo('wpurl')?>/" data-linktrack="true" data-tracklinktext="">
                <img src="<?php bloginfo('template_url')?>/img/logo.png" class="img-fluid" alt="">
            </a>
            <a href="https://www.facebook.com/salajau.sp" target="_blank"><img src="<?php bloginfo('template_url')?>/img/facebook.png" width="20" height="20" class=" mr-2" alt=""></a>
            <a href="https://www.instagram.com/sala_jau/" target="_blank"><img src="<?php bloginfo('template_url')?>/img/instagram.png" width="20" height="20" class="" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/" class="nav-link text-uppercase text-white <?php if(is_front_page()) { echo 'active'; };?>">Home</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/a-sala-jau" class="nav-link text-uppercase text-white <?php if(is_page('a-sala-jau')) { echo 'active'; };?>">A Sala Jaú</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/atividades" class="nav-link text-uppercase text-white <?php if(is_page('atividades')) { echo 'active'; };?>">Atividades</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/cursos" class="nav-link text-uppercase text-white <?php if(is_page('cursos')) { echo 'active'; };?>">Cursos</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/professores" class="nav-link text-uppercase text-white <?php if(is_page('professores')) { echo 'active'; };?>">Professores</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/blog" class="nav-link text-uppercase text-white <?php if(is_category('blog') ) { echo 'active'; };?>">Blog</a></li>
                    <li class="nav-item"><a href="<?php bloginfo('wpurl')?>/contato" class="nav-link text-uppercase text-white <?php if(is_page('contato')) { echo 'active'; };?>">Contato</a></li>
                </ul>
            </div>
        </nav>
        <div class="container text-white text-uppercase small">
            <?php echo do_shortcode('[flexy_breadcrumb]');?>
        </div>
    </div>
    
</div>
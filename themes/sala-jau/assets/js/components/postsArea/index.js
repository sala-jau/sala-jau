import { Fragment, useState, useEffect } from '@wordpress/element';
import { __ } from '@wordpress/i18n';
import Filters from './helpers/Filters';
import PostsList from './helpers/PostsList';
import SliderCourses from './helpers/SliderCourses';

// Cards
import CourseCard from './helpers/cards/CourseCard';
import PostCard from './helpers/cards/PostCard';
import GenericPostCard from './helpers/cards/GenericPostCard';
import ResourceCard from './helpers/cards/ResourceCard';
import GenericSelector from './helpers/filters/GenericSelector';
import VideoCard from './helpers/cards/VideoCard';

import './scss/posts-area.scss';
import './scss/specific-project.scss';

if (typeof useful_info !== 'undefined' && typeof useful_info.posts_per_page !== 'undefined') {
    window.POSTS_PER_PAGE = useful_info.posts_per_page;
} else {
    window.POSTS_PER_PAGE = 10;
}

const sorterFilter = {
	type: "simple-selector",
    placeholder: __('Sort by:', 'buddyx'),
    filterSettings: {
        defaultOption: {
            label: __('Latests', 'buddyx'),
            value: 0,
        },
        options: [
            {
                label: __('Latests', 'buddyx'),
                modifyQuery: {
                    order: 'desc',
                    orderby: 'date'
                }
            },

            {
                label: __('Oldests', 'buddyx'),
                modifyQuery: {
                    order: 'asc',
                    orderby: 'date'
                }
            },

            {
                label: __('Title', 'buddyx'),
                modifyQuery: {
                    order: 'asc',
                    orderby: 'title'
                }
            }
        ]
    }
};

const testingFilters = [ 
    {   
        type: "taxonomy",
        placeholder: 'Category', 
        filterSettings: { 
            taxonomy: 'course_category', 
            multipleSelection: false, 
        } 
    }
];

const cardModels = {
    'CourseCard': CourseCard,
    'GenericPostCard': GenericPostCard,
    'PostCard': PostCard,
    'ResourceCard': ResourceCard,
    'VideoCard': VideoCard
}

function FacetedPostsSearch( props ) {
    const [ totalAvaliablePosts, setTotalAvaliablePosts ] = useState(0);
    const [ totalLoadedPosts, setTotalLoadedPosts ] = useState(0); // get total posts from props
    const [ queryParams, setQueryParams ] = useState({ 'per_page': POSTS_PER_PAGE }); // get base info from props
    const [ filters, setFilters ] = useState( props.filters? props.filters : testingFilters );
    const [ primaryFilters, setPrimaryFilters ] = useState( props.primaryFilters? props.primaryFilters : [] );


    // const [ count, setCount ] = useState(0);
    return (
        <div className="faceted-posts-search" data-filters-count={ filters.length }>
            {/* Those filters are supposed to be less generic (more specific)*/}
            { primaryFilters.length >= 1 && <div className="faceted-posts-search__generic-sort">
            <Filters filters={ primaryFilters } queryParams={ queryParams } setQueryParams={ ( queryParams ) => setQueryParams(queryParams) }/>
            </div> }
            {/* Informative and sorting  */}
            <div className="faceted-posts-search__info-sort">
                {/* Bad practice, replace by a proper html parser */}
                <div className="quantity-info" dangerouslySetInnerHTML={ { __html: __('Showing', 'buddyx') + ` <strong>${ totalLoadedPosts }</strong> ` + __('of', 'buddyx') + ` <strong>${ totalAvaliablePosts }</strong> ` + __('results', 'buddyx') } }></div>
                <div className="sorting-area"> 
                    <GenericSelector filter={ sorterFilter } queryParams={ queryParams } setQueryParams={ setQueryParams }/>
                </div>
            </div>
            {/* Filtering area */}
            <Filters filters={ filters } queryParams={ queryParams } setQueryParams={ ( queryParams ) => setQueryParams(queryParams) }/>
            {/* {Posts List} */}
            <PostsList CardModel={ cardModels[props.cardModel] } useInfiniteLoader={ props.useInfiniteLoader } addicionalCardMeta={ props.addicionalCardMeta } queryParams={ queryParams } baseURL={ props.baseURL? props.baseURL : baseURL } setQueryParams={ ( queryParams ) => setQueryParams(queryParams) } additionalQueryParams={ props.additionalQueryParams } setTotalLoadedPosts={ setTotalLoadedPosts } setTotalAvaliablePosts={ setTotalAvaliablePosts }/>
            {/* {Slider OnGoing} */}
            <SliderCourses queryParams={ queryParams } baseURL={ props.baseURL? props.baseURL : baseURL } setQueryParams={ ( queryParams ) => setQueryParams(queryParams) } additionalQueryParams={ props.additionalQueryParams } />
        </div>
    );
}

const facedElement = document.getElementById( 'faceted-posts-search' );

wp.element.render(
    wp.element.createElement( FacetedPostsSearch, JSON.parse(facedElement.getAttribute('data-building')) ),
    facedElement
);
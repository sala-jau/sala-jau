import { Fragment, useState, useEffect } from '@wordpress/element';
import GenericPostCard from './cards/GenericPostCard';
import { __ } from '@wordpress/i18n';

let placeholder = window.location.href.indexOf("ativades") > -1 ?  __('Carregar mais atividades', 'buddyx')  : __('Carregar mais cursos', 'buddyx');

const defaultProps = {
    useInfiniteLoader: false,
    queryParams: {},
    CardModel: GenericPostCard,
    buttonPlaceholder: placeholder
}

let firstRequest = true;

function PostsList( props ) {
    const [ posts, setPosts ] = useState([]); // posts objects
    const [ page, setPage ] = useState({ current: 1 });  // current page index
    const [ totalPages, setTotalPages ] = useState(1); // last page from given queryParams
    const [ doneLoadingPosts, setDoneLoadingPosts ] = useState(false);
    const [ scrollTop, setScrollTop ] = useState(false);

    // Set default params
    const mergedProps = {...defaultProps, ...props };
    const { useInfiniteLoader, queryParams, CardModel, buttonPlaceholder, setTotalLoadedPosts, baseURL, addicionalCardMeta, additionalQueryParams, setTotalAvaliablePosts } = mergedProps;
    useEffect(() => {
        // const targetURL = new URL(queryParams.targetURL);
        const targetURL = new URL(`${ baseURL }`);
        Object.keys(queryParams).forEach(key => targetURL.searchParams.append(key, queryParams[key]))

        // Add language param to support WPML
        // if(languageParams && languageParams.currentLang) {
        //     targetURL.searchParams.set('lang', languageParams.currentLang);
        // }

        // Set meta values to filter courses
        targetURL.searchParams.set( 'meta_key', '_lp_custom_course_status' );
        targetURL.searchParams.set( 'meta_value', ['open','on_going'] );

        if (additionalQueryParams && (additionalQueryParams.taxonomy && additionalQueryParams.term)) {
            targetURL.searchParams.set( additionalQueryParams.taxonomy, additionalQueryParams.term );
        }

        // Set state.page.current to URL param
        targetURL.searchParams.set('page', page.current);

        // Set embed param to get all addicional info 
        targetURL.searchParams.set('_embed', true);

        // Set loading
        setDoneLoadingPosts(false);

        // Fetch posts based on params
        fetch(targetURL)
            .then( ( response ) => {
                // Set max pages and request response in json format
                setTotalPages(parseInt(response.headers.get("x-wp-totalpages")));

                if(firstRequest) {
                    firstRequest = false;
                    setTotalAvaliablePosts(response.headers.get("x-wp-total"));
                }

                return response.json();
            } )
            .then( data => {                
                setPosts([ ...posts, ...data ]);
                return [...posts, ...data ]
            })
            .catch( err => {
                // Remove: Too noisy 
                // alert("Error while loading posts check console");
                console.log(err);
            })
            .then( () => {
                setDoneLoadingPosts(true);
            });

        
    }, [ page ] );


    useEffect(() => {
        // If we receive new query params we can clear the posts and make a new request to make sure the filters are made
        setDoneLoadingPosts(false);
        setPosts([]);
        setPage({ current: 1 });        
    }, [ queryParams ] );

    useEffect(() => {
        if(doneLoadingPosts) {
            setTotalLoadedPosts(posts.length);
        }
    }, [ posts, doneLoadingPosts ]);

    useEffect(() => {
        if(useInfiniteLoader) {
            window.addEventListener('scroll',  function() {
                const currentOffset = window.pageYOffset;

                if((document.documentElement.scrollHeight - currentOffset) <= 1000)  {
                    setScrollTop(currentOffset);
                }
            });
        }
    }, []);

    useEffect(() => {
        if(page.current + 1 <= totalPages ) {
            setPage({ current: page.current + 1} )
        }
    }, [scrollTop]);

    return (
        <>  
            <div className="faceted-posts-search__posts-area-wrapper">
                <div className={ `faceted-posts-search__posts-list ${( doneLoadingPosts && posts.length == 0 ? "no-results" : "")}`}>
                    {/* Posts cards */}
                    { ( doneLoadingPosts || posts.length > 0) && posts.map(postObject => <CardModel post={ postObject } addicionalCardMeta={ addicionalCardMeta } additionalQueryParams={ additionalQueryParams } /> ) }

                    {/* Posts skeleton */}
                    { !doneLoadingPosts && Array(POSTS_PER_PAGE).fill(0).map( __ => <CardModel skeleton={ true } post={ false } addicionalCardMeta={ addicionalCardMeta } /> ) }
                </div>
                {/* Load more button (if query supports next page) || Infinite load */}
                
                { !useInfiniteLoader && page.current + 1 <= totalPages &&  <button className="load-btn" aria-label={ __("Click in this button to load more results", "buddyx") } action="load-more-posts" onClick={ () => { setPage({ current: page.current + 1} ) } }>{ buttonPlaceholder }</button>}
            </div>
        </>

    );
}

export default PostsList;
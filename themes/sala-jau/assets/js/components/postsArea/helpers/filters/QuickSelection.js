import { Fragment, useState, useEffect } from '@wordpress/element';
import './scss/quick-select.scss';
import { __ } from '@wordpress/i18n';


const PAGE_LIMIT = 10; // if per_page is 10, 100 terms will be fetched 


export default function QuickSelection({ filter, queryParams, setQueryParams }) {
    const [taxonomyTermsOptions, setTaxonomyTermsOptions] = useState([]);
    const [ isLoading, setIsLoading ] = useState(false);
    const [ activeItem, setActiveItem ] = useState(-1);

    // First mount
    useEffect(() => {

        const targetURL = new URL(document.location.origin + `/wp-json/wp/v2/${getUrlRelativeName(filter.filterSettings.route)}`);

        // Add language param to support WPML
        if(!typeof languageParams === 'undefined' && languageParams.currentLang) {
            targetURL.searchParams.set('lang', languageParams.currentLang);
        }

        if(filter.filterSettings.params)  
            addSearchParams(targetURL, filter.filterSettings.params);
        
        if(queryParams) 
            addSearchParams(targetURL, queryParams);
        
        
            // Fetch terms based on the filter taxonomy
            setIsLoading(true);
            fetch(targetURL)
            .then(async (response) => {
                const jsonResponse = await response.json();
                return { data: jsonResponse, totalPages: response.headers.get("x-wp-totalpages") };
            })
            .then(({ data, totalPages }) => {
                // Save first page results
                let cumulativeTerms = data;
                
                // If we only have one page we can return right here
                if (totalPages == 1) {
                    setTaxonomyTermsOptions(cumulativeTerms.map(termToOption));
                    // Add ready (sucess) state (animation flag)
                    setIsLoading(false);
                    return;
                }
                
                
                // Keep requesting to get to the last page
                for (let i = 2; i <= totalPages; i++) {
                    // Break to avoid exceeding page limiting
                    if (i >= PAGE_LIMIT) {
                        break;
                    }
                    
                    fetch(targetURL + "?page=" + i)
                    .then(response => { return response.json() })
                    .then(moreresults => {
                        cumulativeTerms = [...cumulativeTerms, ...moreresults];
                        
                        if (i == totalPages) {
                            // Add ready state (animation flag)
                            setTaxonomyTermsOptions(cumulativeTerms.map(termToOption));
                            setIsLoading(false);
                            // console.log(cumulativeTerms.map(termToOption));
                        }
                    });
                    
                }
            })
            .catch(err => {
                // Remove: Too noisy 
                // alert("Error while loading posts check console");
                console.log(err);
            })
            .then(() => {
                // Add ready state (animation flag)
                // Set done status anyway error or success ()
                setIsLoading(false);
            });
            
        }, []);
        
        const handleChange = (values) => {
            const newQueryParams = { ...queryParams };
            //const taxonomy = filter.filterSettings.taxonomy;
            //  const taxonomy = filter.filterSettings.taxonomy;
            const taxonomy = filter.filterSettings.params.taxonomy;
            let urlRelativeName = getUrlRelativeName(taxonomy);
            
            // If handle returns undefined values then the select is empty
            if (!values || ( values && values.length && values[0].value == -1 )) {
                delete newQueryParams[urlRelativeName];
                setQueryParams(newQueryParams);
                setActiveItem(-1)
                return;
            }
            
            newQueryParams[urlRelativeName] = values.reduce(joinValuesReducer, "")
            setActiveItem(values[0].value)
            
            // console.log(newQueryParams);
            setQueryParams(newQueryParams);
        }
        return (
            <div className="faceted-posts-search__quick-select-field">
            <button class={ activeItem == -1? "active" : ""} onClick={ () => handleChange([ { value: -1 } ]) }> { __("Todos", "buddyx") } </button>

            { taxonomyTermsOptions.map(item => 
                <button class={ activeItem == item.value? "active" : ""} onClick={ () => handleChange([ item ]) }> { item.label } </button>
                ) }
        </div>
    )
}


// Helper functions
const addSearchParams = function(targetURL, params){
    
    Object.keys(params).forEach(function(key) {
        targetURL.searchParams.set(key, params[key])
    });
    
    return targetURL;
}


const joinValuesReducer = function (accumulator, currentValue, index, array) {
    return (index == array.length - 1) ? accumulator + currentValue.value : accumulator + currentValue.value + ',';
}

const relativeUrlNames = {
    'category': 'categories',
    'post_tag': 'tags',
};

const getUrlRelativeName = (taxonomy) => {
    if (relativeUrlNames.hasOwnProperty(taxonomy)) {
        return relativeUrlNames[taxonomy];
    }

    return taxonomy;
}

const selectTheme = theme => ({
    ...theme,
    // borderRadius: 0,
    colors: {
        ...theme.colors,
        primary25: 'var(--primary-25)',
        primary: 'var(--primary-color)',
    },
});

const termToOption = ({ id, name }) => {
    return { value: id, label: name };
}
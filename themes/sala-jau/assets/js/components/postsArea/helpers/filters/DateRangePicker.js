import { __ } from '@wordpress/i18n';
import { Fragment, useState, useEffect, useCallback, useRef } from '@wordpress/element';
//import 'bootstrap-daterangepicker/daterangepicker.css';
import DateRangePicker from 'react-bootstrap-daterangepicker';

// import debounce from 'lodash.debounce';
import './scss/date-range-picker.scss';

// {   
//     type: "taxonomy",
//     placeholder: 'Category', 
//     filterSettings: { 
//                         useDateSelector: false, 
//                         taxonomy: 'category', 
//                         multipleSelection: true, 
//                         useSimpleSelector: false,
//                         options: [{value: 17, label: 'categoria 1'}, {value: 18, label: 'category 2'}, {value: 19, label: 'category 3'}]

//                     } 
// } 


const initialSettings = { autoUpdateInput: false, locale: { cancelLabel: 'Clear' } };

export default function ADateRangePicker({ filter, queryParams, setQueryParams }) {
    const [inputValue, setInputValue ] = useState("");
    const dataPickerReference = useRef();

    const handleChange = (ev, picker ) => {
        //console.log(ev);
        const dateOptions = [ undefined, { year:"2-digit", month:"2-digit", day:"2-digit" } ];
        const newQueryParams = { ...queryParams };

        if(ev.type === "apply") {
            setInputValue(
                picker.startDate.toDate().toLocaleDateString( ...dateOptions ) +
                ' - ' +
                picker.endDate.toDate().toLocaleDateString( ...dateOptions ),
            );

            setQueryParams({ ...newQueryParams, after: picker.startDate.toISOString(), before: picker.endDate.toISOString() } );
        } else {
            if(newQueryParams.after) {
                delete newQueryParams.after;
            }

            if(newQueryParams.before) {
                delete newQueryParams.before;
            }

            dataPickerReference.current.setStartDate(moment());
            dataPickerReference.current.setEndDate(moment());
            
            setInputValue("");
            setQueryParams(newQueryParams);
        }
        // console.log(picker);    
    }


    return (
        <div className="faceted-posts-search__date-range-picker-field">
            <DateRangePicker initialSettings={ initialSettings } onApply={ handleChange } onCancel={ handleChange } ref={ dataPickerReference }>
                <input
                    placeholder={ filter.placeholder }
                    readOnly="true"
                    value={ inputValue }
                    type="text"
                ></input>
            </DateRangePicker>
        </div>
    )
}
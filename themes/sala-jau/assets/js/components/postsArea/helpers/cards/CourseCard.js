import { Fragment, useState, useEffect } from '@wordpress/element';
import { sanitize } from 'dompurify';

// This filters which taxonomies will be displayed by default
let taxonomiesThatShouldBeListed = [
    'course_category',
    'course_tag',
    'format',
    'modality'
];

export default function CourseCard({ skeleton, post, addicionalCardMeta, additionalQueryParams }) {

    const [ hasFeaturedImage, setHasFeaturedImage ] = useState(false);
    const [ properImageURL, setProperImageURL ] = useState(false);
    const [ areaTerms, setAreaTerms ] = useState([]);
    const [ formatTerms, setFormatTerms ] = useState([]);
    const [ modalityTerms, setModalityTerms ] = useState([]);

    useEffect(() => {
        // If is a skeleton request we dont have data to process
        if(skeleton) return;

        const usefullEmbedData = post._embedded? post._embedded : {};

        if(usefullEmbedData.hasOwnProperty("wp:featuredmedia")) {
            setHasFeaturedImage(true);

            const firstFeaturedImageItem = usefullEmbedData["wp:featuredmedia"][0];
            const baseFeaturedImageObject = firstFeaturedImageItem.media_details;

            if (baseFeaturedImageObject) {
                // Take medium image always if set otherwise use generic image
                if(baseFeaturedImageObject.sizes && baseFeaturedImageObject.sizes.hasOwnProperty('full')) {
                    setProperImageURL(baseFeaturedImageObject.sizes.full.source_url);
                } else {
                    setProperImageURL(firstFeaturedImageItem.source_url);
                }
            }

            if(usefullEmbedData.hasOwnProperty("wp:term") && taxonomiesThatShouldBeListed) {

                const groupedTerms = usefullEmbedData["wp:term"];
                let ungroupedFilteredTerms = [];

                groupedTerms.forEach( taxonomyGroup => {
                    ungroupedFilteredTerms = [ ...ungroupedFilteredTerms, 
                        ...taxonomyGroup.filter( term => {
                                return taxonomiesThatShouldBeListed.includes(term.taxonomy);
                        } ) ]
                })

                let courseTag = ungroupedFilteredTerms.filter( term => {
                    return term.taxonomy === 'course_tag';
                } );

                let courseCategory = ungroupedFilteredTerms.filter( term => {
                    return term.taxonomy === 'course_category';
                } );

                let courseFormat = ungroupedFilteredTerms.filter( term => {
                    return term.taxonomy === 'format';
                } );

                let courseModality = ungroupedFilteredTerms.filter( term => {
                    return term.taxonomy === 'modality';
                } );

                setFormatTerms(courseTag);
                setAreaTerms(courseCategory);
                setFormatTerms(courseFormat);
                setModalityTerms(courseModality);
            }

        }

    }, [ skeleton ]);

    if(skeleton) {
        return (
            <>
                <div className={ `card-course col-sm-${addicionalCardMeta.full ? '12' : '6'} course-id has-image skeleton` }>
                    <div className="card-course--content">
                        <div className="course-img"></div>
                        <div className="card-course--text">
                            <div className="header">
                                <div className="option-row">
                                    <div className="course-terms"></div>
                                    <div className="format">
                                        <ul className="list-terms tax-format"></ul>
                                    </div>
                                </div>
                                <h3 className="course-title">
                                    <a href=""></a>
                                </h3>
                                <div className="authorship">
                                    <div className="author-wrapper">
                                        <div className="author-name">
                                            <h3>
                                                <span></span>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="side">
                                <a className="btn-bg-white" href=""></a>
                                <div className="metabox-container">
                                    <span className="formated-date-course format-default"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    } else {
        return (
            <>
                <div className={ `card-course col-sm-${addicionalCardMeta.full ? '12' : '6'} course-${post.id} ${(hasFeaturedImage? "has-image" : "")} ${(additionalQueryParams.termSlug? additionalQueryParams.termSlug : "default")}-card` }>
                    <div className="card-course--content">
                        {   hasFeaturedImage && 
                            <div className="course-img">
                                <img src={ properImageURL } alt={ post.title.rendered }/>
                            </div>
                        }
                        <div className="card-course--text">
                            <div className="header">
                                <div className="option-row">
                                    {   additionalQueryParams.termSlug === 'atividades' &&
                                        modalityTerms.length >= 1 &&
                                        <div className="course-terms">
                                                { modalityTerms.map(term => {
                                                    return <li className={`term-${term.slug}`}> <a /* href={ term.link } */> { term.name } </a> </li>
                                                }) }
                                        </div>
                                    }

                                    {   additionalQueryParams.termSlug != 'atividades' &&
                                        areaTerms.length >= 1 && 
                                        <div className="course-terms">
                                                { areaTerms.map(term => {
                                                    return <li className={`term-${term.slug}`}> <a /* href={ term.link } */> { term.name } </a> </li>
                                                }) }
                                        </div>
                                    }

                                    {   formatTerms.length >= 1 && 
                                        <div className="format">
                                                { formatTerms.map(term => {
                                                    return <li className={`term-${term.slug}`}>  <a /* href={ term.link } */> { term.name } </a> </li>
                                                }) }
                                        </div>
                                    }
                                </div>
                                <h3 className="course-title">
                                    <a href={ post.link } dangerouslySetInnerHTML={ { __html: sanitize(post.title.rendered)  }}></a>
                                </h3>
                                <div className="authorship">
                                    <div className="author-wrapper">
                                        <div className="author-name">
                                            <h3>
                                                <span>com</span> { post.author_name }
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {   additionalQueryParams.termSlug === 'atividades' &&
                                <div className="excerpt" dangerouslySetInnerHTML={{ __html: sanitize(post.excerpt.rendered.split(' ', 20).join(' ')+' ...') }}></div>
                            }

                            <div className="side">
                                {   additionalQueryParams.termSlug === 'cursos-gravados' &&
                                    <div className="metabox-container" dangerouslySetInnerHTML={{ __html: sanitize(post._lpr_custom_avaliable_until)}}></div>
                                }
                                {   additionalQueryParams.termSlug !== 'cursos-gravados' &&
                                    <div className="metabox-container" dangerouslySetInnerHTML={{ __html: sanitize(post.formated_date_course) }}>
                                    </div>
                                }
                                <a className="btn-bg-white" href={ post.link }>informações</a>                                
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
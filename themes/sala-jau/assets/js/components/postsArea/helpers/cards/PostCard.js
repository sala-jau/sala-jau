import { Fragment, useState, useEffect } from '@wordpress/element';
import './scss/item-card.scss';

// This filters which taxonomies will be displayed by default
let taxonomiesThatShouldBeListed = [
    'course_category'
];

export default function PostCard({ skeleton, post, addicionalCardMeta }) {    
    const [ hasFeaturedImage, setHasFeaturedImage ] = useState(false);
    const [ properImageURL, setProperImageURL ] = useState(false);
    const [ cardFormat, setCardFormat ] = useState(addicionalCardMeta.format? addicionalCardMeta.format : 'horizontal');
    const [ listedTerms, setListedTerms] = useState([]);

    useEffect(() => {
        // If is a skeleton request we dont have data to process
        if(skeleton) return; 

        const usefullEmbedData = post._embedded? post._embedded : {};

        if(usefullEmbedData.hasOwnProperty("wp:featuredmedia")) {
            setHasFeaturedImage(true);

            const firstFeaturedImageItem = usefullEmbedData["wp:featuredmedia"][0];
            const baseFeaturedImageObject = firstFeaturedImageItem.media_details;

            if (baseFeaturedImageObject) {
                // Take medium image always if set otherwise use generic image
                if(baseFeaturedImageObject.sizes && baseFeaturedImageObject.sizes.hasOwnProperty('medium')) {
                    setProperImageURL(baseFeaturedImageObject.sizes.medium.source_url);
                } else {
                    setProperImageURL(firstFeaturedImageItem.source_url);
                }
            }

        }

        if (addicionalCardMeta) {
            taxonomiesThatShouldBeListed = addicionalCardMeta.taxonomies_listed;
        }

        if(usefullEmbedData.hasOwnProperty("wp:term") && taxonomiesThatShouldBeListed) {
            const groupedTerms = usefullEmbedData["wp:term"];
            let ungroupedFilteredTerms =  [];

            groupedTerms.forEach( taxonomyGroup => {
                ungroupedFilteredTerms = [ ...ungroupedFilteredTerms, 
                    ...taxonomyGroup.filter( term => {
                            return taxonomiesThatShouldBeListed.includes(term.taxonomy);
                    } ) ]
            })

            setListedTerms(ungroupedFilteredTerms);
        }
    }, [ skeleton ]);


    // Ghost component
    if(skeleton) {
        return (
            <>
                <div className={ `item-card ${cardFormat} has-image skeleton` }>
                    <div className="item-card--thumbnail"></div>
                    <div className="item-card--content">
                        <div className="terms"></div>
                        <div className="title"></div>
                        <div className="additional-meta"></div>
                        <p className="excerpt"></p>
                    </div>
                </div>
            </>
        );
    } else {
        const dateOptions = { year: 'numeric', month: 'long', day: 'numeric' };
        const postDate = new Date( post.date ).toLocaleDateString(undefined, dateOptions);

        return (
            <>
                <div className={`item-card ${cardFormat} ${(hasFeaturedImage? " has-image" : "")}`}>
                    {   hasFeaturedImage && 
                        <div className="item-card--thumbnail">
                            <img src={ properImageURL } alt=""/>
                        </div>
                    }

                    <div className="item-card--content">
                        {/* Main terms */}
                        {   listedTerms.length >= 1 && 
                            <div className="terms">
                                { listedTerms.map(term => {
                                    return <a href={ term.link }>{ term.name }</a>
                                }) }
                            </div>
                        }

                        {/* Title */}
                        <a href={ post.link } className="title" dangerouslySetInnerHTML={ { __html: post.title.rendered  }}></a>

                        {/* exemple additional meta */}
                        <div className="additional-meta">
                            <div className="meta-item">
                                { postDate }
                            </div>
                            {/* <div className="meta-item">
                                format: <a href="#">video</a>
                            </div> */}
                        </div>

                        {/* Bad practice, replace with a proper html parser */}
                        { post.excerpt.rendered && <p className="excerpt" dangerouslySetInnerHTML={ { __html: post.excerpt.rendered }}></p> }
                    </div>
                </div>
            </>
        )

    }


}


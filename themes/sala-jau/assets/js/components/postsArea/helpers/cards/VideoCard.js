import { Fragment, useState, useEffect } from '@wordpress/element';
import { __ } from '@wordpress/i18n';

import './scss/item-card.scss';
import './scss/video-card.scss';

// This filters which taxonomies will be displayed
const taxonomiesThatShouldBeListed = [
    // 'category',
    'post_tag',
    // 'resource_type'
];

export default function VideoCard({ skeleton, post, addicionalCardMeta }) {    
    useEffect(() => {
        // If is a skeleton request we dont have data to process
        if(skeleton) return; 

        const usefullEmbedData = post._embedded;

    }, [ skeleton ]);


    // Ghost component
    if(skeleton) {
        return (
            <>
                <div className="item-card video-card vertical skeleton">
                    <div className="item-card--thumbnail"></div>
                    <div className="item-card--content">
                        <div className="title"></div>
                        <div className="additional-meta"></div>
                        <p className="excerpt"></p>
                    </div>
                </div>
            </>
        );
    } else {
        const dateOptions = { year: 'numeric', month: 'long', day: 'numeric' };
        const postDate = new Date( post.date ).toLocaleDateString(undefined, dateOptions);

        return (
            <>
                <div className={"item-card video-card vertical"}>
                    <div className="item-card--thumbnail" dangerouslySetInnerHTML={ { __html: post.content.rendered  }}>
                    </div>

                    <div className="item-card--content">
                        {/* Title */}
                        <a className="title" dangerouslySetInnerHTML={ { __html: post.title.rendered  }}></a>

                        {/* additional meta */}
                        <div className="additional-meta">
                            <div className="meta-item">
                                { postDate }
                            </div>

                        </div>

                        {/* Bad practice, replace with a proper html parser */}
                        <p className="excerpt" dangerouslySetInnerHTML={ { __html: post.excerpt.rendered }}></p>
                    </div>
                </div>
            </>
        )

    }


}


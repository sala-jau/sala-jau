export default function GenericPostCard({ skeleton, post }) {
    if(skeleton) {
        return "";
    } else {
        return (<p> { post.title.rendered } </p>);
    }
}


import { Fragment, useState, useEffect } from '@wordpress/element';
import CourseCard from './cards/CourseCard';
import { __ } from '@wordpress/i18n';

const defaultProps = {
    queryParams: {},
}

let firstRequest = true;

let slider = false;

let isActivity = window.location.href.indexOf("atividades") > -1;
let isMobile = window.innerWidth < 800;

let isSinglePost = (isActivity || isMobile) ? true : false;

function SliderCourses( props ) {
    const [ posts, setPosts ] = useState([]); // HTML objects

    // Set default params
    const mergedProps = {...defaultProps, ...props };
    const { queryParams, baseURL, additionalQueryParams, addicionalCardMeta } = mergedProps;
    const [ doneLoadingPosts, setDoneLoadingPosts ] = useState(false);
    const [ activeItem, setActiveItem ] = useState(0);
    const [ itemsKeys, setItemsKeys ] = useState(0);
    
    useEffect(() => {

        let url = window.location.pathname;

        if (url.match("/tipo/cursos-gravados")) {
            return;
        }
            
        const targetURL = new URL(`${ baseURL }`);
        Object.keys(queryParams).forEach(key => targetURL.searchParams.append(key, queryParams[key]))
        
        targetURL.searchParams.set( 'per_page', 99 );
        targetURL.searchParams.set( 'meta_key', '_lp_custom_course_status' );
        targetURL.searchParams.set( 'meta_value', ['on_going'] );
        targetURL.searchParams.set('_embed', true);
       
        additionalQueryParams.term ? targetURL.searchParams.set('course_tag', additionalQueryParams.term) : '';
        queryParams.course_category ? targetURL.searchParams.set('course_category', queryParams.course_category) : '';


        fetch(targetURL)
            .then( ( response ) => {
                return response.json();
            }).then( data => {                
                isSinglePost ? setSinglePosts(data) : setPairsPosts(data);
            })
            .catch( err => {
                console.log(err);
            })
            .then( () => {
                setDoneLoadingPosts(true);
            });
            
        }, [ queryParams ] );
    
    const setSinglePosts = (posts) => {
        setPosts(posts);
        setItemsKeys(posts.length - 1);
    }

    useEffect(() => {
        // If we receive new query params we can clear the posts and make a new request to make sure the filters are made
        setPosts([]);
    }, [ queryParams ] );

    const setPairsPosts = (posts) => {
        
        let postsPairs = posts.reduce((list, _, index, source) => {
            if (index % 2 === 0) 
                list.push(source.slice(index, index + 2));
           
            return list;
        }, []);
        
        setPosts(postsPairs);
        setItemsKeys(postsPairs.length - 1);
            
        return postsPairs;
    }
    
    const handleClickLeft = (activeItem) => {
        (activeItem > 0) ? setActiveItem(activeItem - 1) : setActiveItem(itemsKeys);
    }
    
    const handleClickRight = (activeItem) => {
        (activeItem < itemsKeys) ? setActiveItem(activeItem + 1) : setActiveItem(0);
    }

    if(posts.length > 0){
        return (
            <div className={"courses-on-going container-slider"}>
                <div class="container">
                   <h2 className="on-going-title" > <a href ="/em-andamento">
                        {__( 'Em andamento', 'buddyx')}
                        </a></h2>
                        <a class="btn-bg-white" href = "/em-andamento"> Ver mais </a>
                </div>
                <div id="courses-slider-on-going">
                    {(posts.length > 1) && <div className="controls">
                        <button className={"control-l"} onClick={() => handleClickLeft(activeItem)}>
                            <i class="fas fa-chevron-left"></i>
                        </button>
                        <button className={"control-r"} onClick={() => handleClickRight(activeItem)}>
                            <i class="fas fa-chevron-right"></i>
                        </button>
                    </div>} 
    
                    { !doneLoadingPosts && Array(isSinglePost ? 1 : 2).fill(0).map(postObject => <CourseCard  skeleton={ true } addicionalCardMeta={ {full: isSinglePost} } />)}
                
                    { !isSinglePost && doneLoadingPosts && posts.map((pair, index) => 
                        <div className={(index == activeItem) ? 'fade-in active' : 'hidden'}>
                            <CourseCard post={ pair[0] } addicionalCardMeta={ {} } additionalQueryParams={ additionalQueryParams } />
                            {pair[1] && <CourseCard post={ pair[1] } addicionalCardMeta={ {} } additionalQueryParams={ additionalQueryParams } />}
                        </div>
                    )}
    
                    {  isSinglePost && doneLoadingPosts && posts.map((post, index) => 
                        <div className={(index == activeItem) ? 'fade-in active' : 'hidden'}>
                            <CourseCard post={ post } addicionalCardMeta={ {full: isSinglePost} } additionalQueryParams={ additionalQueryParams } />
                        </div>
                    )}
    
                </div> 
            </div>
        );
    }

    return null;
}

export default SliderCourses;
// Filtro usado na página de cursos em andamento
jQuery(function() {
    var selectedClass = "";
    jQuery(".cards-courses-list").find('.card-course').addClass("scale-anm all")
    
    jQuery(".filtro-area").on('click',function(){ 
        jQuery(this).addClass('active').siblings().removeClass('active');
        selectedClass = jQuery(this).attr("data-rel"); 
        jQuery(".cards-courses-list").fadeTo(100, 0.1);
        jQuery(".cards-courses-list .card-course").not("."+selectedClass).fadeOut().removeClass('scale-anm');
        setTimeout(function() {
            jQuery("."+selectedClass).fadeIn().addClass('scale-anm');
            jQuery(".cards-courses-list").fadeTo(300, 1);
        }, 300); 
        
    });
});
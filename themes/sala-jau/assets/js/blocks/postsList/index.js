import { registerBlockType } from "@wordpress/blocks";
import PostListingEdit from './edit';


import { __ } from "@wordpress/i18n";
import "./scss/editor.scss";

registerBlockType("buddyx/posts-list", {
    title: __("Adaptive post listing", "buddyx"),
    icon: "list-view",
    category: "common",
    keywords: [__("post", "buddyx"), __("list", "buddyx"), __("card", "buddyx")],
    supports: {
        reusable: false,
        html: false,
        customClassName: false,
        labelColor: true,
    },
    attributes: {
        className: {
            type: "string",
            default: ""
        },
        productType: {
            type: "string",
            default: false
        },
        productStatus: {
            type: "string",
            default: ""
        },
        templateModel: {
            type: "string",
            default: "complete"
        }
    },

    // styles: [
    //     {
    //         name: "default",
    //         label: __("Cards with background", "buddyx"),
    //         isDefault: true,
    //     },
    //     {
    //         name: "with_description",
    //         label: __("Cards with description", "buddyx"),
    //     },
    // ],

    edit: PostListingEdit, // end withSelect

    save: (props) => {
        return null;
    },
});

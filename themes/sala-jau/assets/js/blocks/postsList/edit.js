import { InspectorControls } from "@wordpress/block-editor";

import ServerSideRender from '@wordpress/server-side-render';

import { __ } from "@wordpress/i18n";

import "./scss/server-side-render.scss";

import {
    Disabled,
    SelectControl,
    PanelBody,
    PanelRow,
} from "@wordpress/components";

const PostListingEdit = (props) => {
    const {
        className,
        attributes: { productType, templateModel, productStatus },
        setAttributes,
    } = props;

    const termsOptions = useful_info.product_types.map(term => {
        return {
            value: term.term_id,
            label: term.name,
        }
    });

    const templateOptions = [
        { label: 'Completo', value: 'complete' },
        { label: 'Reduzido', value: 'reduced' }
    ];

    const statusOptions = [
        { value: 'open', label: 'Novo' },
        { value: 'on_going', label: 'Em andamento' },
        { value: 'finished', label: 'Finalizado' }
    ];

    return (
        <>
            <div className={ className }>
                {
                    <InspectorControls>
                        <PanelBody
                            title={__("Opções de conteúdo", "buddyx")}
                            initialOpen={true} >
                            
                            <PanelRow>
                                <SelectControl
                                    label={__("Modelo:", "buddyx")}
                                    value={ templateModel } 
                                    onChange={(template) => {
                                        setAttributes({ ...props.attributes, templateModel: template });
                                    }}
                                    options={ [
                                        {
                                            value: null,
                                            label: __("Selecione um modelo", "buddyx"),
                                            disabled: true,
                                        },
                                        ...templateOptions
                                    ] }
                                />
                            </PanelRow>
                            <PanelRow>
                                <SelectControl
                                    label={__("Tipo de produto:", "buddyx")}
                                    value={ productType } 
                                    onChange={(term) => {
                                        setAttributes({ ...props.attributes, productType: term });
                                    }}
                                    options={ [
                                        {
                                            value: null,
                                            label: __("Selecione um termo", "buddyx"),
                                            disabled: true,
                                        },
                                        ...termsOptions
                                    ] }
                                />
                            </PanelRow>

                            <PanelRow>
                                <SelectControl
                                    label={__("Status do produto:", "buddyx")}
                                    value={ productStatus }
                                    onChange={(status) => {
                                        setAttributes({ ...props.attributes, productStatus: status });
                                    }}
                                    options={ [
                                        {
                                            value: null,
                                            label: __("Selecione um status", "buddyx"),
                                            disabled: true,
                                        },
                                        ...statusOptions
                                    ] }
                                />
                            </PanelRow>
                        </PanelBody>
                    </InspectorControls>
                }

                <Disabled>
                    <ServerSideRender
                        block="buddyx/posts-list"
                        attributes={props.attributes}
                    />
                </Disabled>
            </div>
        </>
    );
};

export default PostListingEdit;

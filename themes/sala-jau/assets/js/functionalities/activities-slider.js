var hasSlider = document.getElementById('term-atividades');

if (hasSlider) {
    var slider = tns({
        container: '#term-atividades',
        items: 1,
        controlsContainer: '.slider-activity .controls',
        nav: false,
        arrowKeys: true,
        lazyload: false
    });
}
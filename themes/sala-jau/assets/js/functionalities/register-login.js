var formLogin = document.querySelector('.learn-press-form-login.learn-press-form');
var formRegister = document.querySelector('.learn-press-form-register.learn-press-form');
var buttonToggle = document.getElementById('toggle-form-register');

formRegister.classList.add('visuallyhidden', 'hidden');

buttonToggle.addEventListener('click', function(e) {

    e.preventDefault();

    if (formLogin.classList.contains('hidden')) {
        buttonToggle.innerHTML = 'Ainda não tem uma conta? <a href="javascript: void(0);">Crie sua conta!</a>';
        formLogin.classList.remove('hidden');
        setTimeout(() => {
            formLogin.classList.remove('visuallyhidden');
        }, 0);
    } else {
        buttonToggle.innerHTML = 'Já tem uma conta? <a href="javascript: void(0);">Acesse sua conta!</a>';
        formLogin.classList.add('visuallyhidden');
        formLogin.addEventListener('transitionend', (e) => {
            formLogin.classList.add('hidden');
        }, {
            capture: false,
            once: true,
            passive: false
        });
    }

    if (formRegister.classList.contains('hidden')) {
        formRegister.classList.remove('hidden');
        setTimeout(() => {
            formRegister.classList.remove('visuallyhidden');
        }, 0);
    } else {
        formRegister.classList.add('visuallyhidden');
        formRegister.addEventListener('transitionend', (e) => {
            formRegister.classList.add('hidden');
        }, {
            capture: false,
            once: true,
            passive: false
        });
    }

}, false);
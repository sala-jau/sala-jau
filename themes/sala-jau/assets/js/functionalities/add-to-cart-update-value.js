/**
 * Update cart counter when add new item
 */
jQuery(document).ready(function($) {
    $( document ).ajaxComplete(function( event, request, settings ) {
        // check if is the right ajax
        if ( 'POST' == settings.type && settings.data.includes( 'action=lpWooAddCourseToCart' ) ) {

            // get DOM element / check if exists
            $element = $( '.menu-icons-wrapper.cart sup' );
            if ( 0 != $element.length ) {
                // convert value to int
                var cart_items = parseInt( $element.html() );
                // update HTML element with new value
                $element.html( cart_items + 1 );
                var base_url = window.location.origin;

                window.location = base_url + '/cart/';
            }
        }
    });
});

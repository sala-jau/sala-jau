var slider = tns({
    container: '#home-slider',
    items: 1,
    controlsContainer: '.controls',
    nav: false,
    arrowKeys: true,
    lazyload: false,
    autoplay: 1,
    autoplayButtonOutput: 0,
    autoplayTimeout: 7000

});
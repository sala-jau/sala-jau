document.addEventListener('DOMContentLoaded', function() {
    let bodyElement = document.body;
    if (bodyElement.classList.contains('single-lp_course')) {
        let messageElements = document.getElementsByClassName('learn-press-message');
        if (messageElements.length > 0) {
            let messageElement = messageElements[0];
            if (messageElement.innerText.includes('curso encerrado')) {
                bodyElement.classList.add('hidden-continue-button');
            }
        }
    }
});
function cleanYears(){
    // clean years
    years = document.getElementsByClassName('year');    
    for (var i=0; i<years.length; i+=1){
        years[i].style.display = "none";
    }
}

function cleanButtons(){
    // clean buttons
    buttons = document.getElementsByClassName('btnYear');
    for (var i=0; i<buttons.length; i+=1){
        buttons[i].classList.remove('active');
    }
}

// shows the history of the selected year
function changeYear(event){
    cleanYears();
    cleanButtons();
    button = event.currentTarget;
    year = button.getAttribute('ano');  
    button.classList.add('active');

    // Shows selected year
    page = document.querySelector('[showyear="'+year+'"]');
    page.style.display = "block";
}

// shows the history of the current year
document.addEventListener("DOMContentLoaded", function() {
    cleanYears();
    actualYear = new Date().getFullYear();
    page = document.querySelector('[showyear="'+actualYear+'"]');
    page.style.display = "block";
});
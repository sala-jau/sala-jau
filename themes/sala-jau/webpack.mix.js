const path = require( 'path' );
let mix = require('laravel-mix');
const defaultConfig = require( './node_modules/@wordpress/scripts/config/webpack.config' );
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

const root_dir = './';
const assets_dir = root_dir + '/assets';
const dist_dir = root_dir + '/dist';
const css_dir =  dist_dir +  '/css';

mix.sass(assets_dir + '/scss/app.scss', 'css/sala-jau.css')
	.sourceMaps();

mix.react('./assets/js/blocks/postsList/index.js', 'postsList.js');
mix.react('./assets/js/components/postsArea/index.js', 'postsArea.js');
mix.react('./assets/js/app.js', 'app.js');

mix.webpackConfig({
    ...defaultConfig,
    entry: {
		//imageBlock: './assets/javascript/blocks/imageBlock/index.js',
    },
    
    output: {
        chunkFilename: dist_dir + '/[name].js',
        path: path.resolve( __dirname, './dist/' ),
        publicPath: dist_dir,
        filename: '[name].js',
    },

    module: {
		
    },
  
	devtool: "inline-source-map" 
});


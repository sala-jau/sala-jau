<?php
/**
 * Class to filter user by course (table wp_learnpress_user_items )
 */
class Hacklab_Filter_User_By_Course {
    public function __construct() {
        // filter query
        add_action('pre_user_query', array( $this, 'pre_get_query' ) );

        // add link to column
        add_action( 'manage_lp_course_posts_custom_column' , array( $this, 'before_column' ), 9, 2 );
        add_action( 'manage_lp_course_posts_custom_column' , array( $this, 'after_column' ), 11, 2 );
    }

    /**
     * Add link to filter on column in post type
     */
    public function before_column( $column, $post_id ) {
        if ( 'students' != $column ) {
            return;
        }
        $url = admin_url( "users.php?filter_by_course=$post_id" );
        echo "<a href='$url'>";
    }
    /**
     * Closes <a>;
     */
    public function after_column( $column, $post_id ) {
        if ( 'students' != $column ) {
            return;
        }
        echo '</a>';
    }

    /**
     * Add filter to SQL query
     */
    public function pre_get_query( $query ) {
        global $wpdb;
        if ( ! is_admin() ) {
            return;
        }
        $screen = get_current_screen();
        //var_dump( $_SERVER['HTTP_REFERER'] );
        if ( ( ! is_object( $screen ) || 'users' != $screen->base ) && ! isset( $_REQUEST[ 'eudc_export_csv' ] ) ) {
            return;
        }
        if ( ! isset( $_GET[ 'filter_by_course'] ) || empty( $_GET[ 'filter_by_course'] ) ) {
            return;
        }
        $table = $wpdb->base_prefix . 'learnpress_user_items';
        $item_id = absint( $_GET[ 'filter_by_course'] );
        $query->query_where =
			"WHERE 1=1 AND {$wpdb->users}.ID IN (
				SELECT {$table}.user_id FROM $table
					WHERE {$table}.item_id = $item_id )";
	}
}
new Hacklab_Filter_User_By_Course();
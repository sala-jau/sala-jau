<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
/**
 * Add Courses Tab in WooCommerce My Account 
 * @author        hacklab
 *  
 */
class Hacklab_WC_LearnPress_My_Account {
    public function __construct() {
        // add endpoint 
        add_action( 'init', array( $this, 'add_endpoint' ) );

        // query var
        add_filter( 'query_vars', array( $this, 'add_query_var' ), 0 );

        // Add link
        add_filter( 'woocommerce_account_menu_items', array( $this, 'add_link' ), 11, 1 );

        // content
        add_action( 'woocommerce_account_courses_endpoint', array( $this, 'content' ) );
    }

    /**
     * Add Endpoint
     */
    public function add_endpoint(){
        add_rewrite_endpoint( 'courses', EP_ROOT | EP_PAGES );
    }

    /**
     * Add Query var
     */
    public function add_query_var( $vars ) {
        $vars[] = 'courses';
        return $vars;
    }
    /**
     * Add link
     */ 
    public function add_link( $menu_links ) {
        $new = array( 'courses' => 'Cursos' );
        // or in case you need 2 links
        // $new = array( 'link1' => 'Link 1', 'link2' => 'Link 2' );
        // array_slice() is good when you want to add an element between the other ones
        $menu_links = array_slice( $menu_links, 0, 1, true ) + $new + array_slice( $menu_links, 1, NULL, true );
        return $menu_links;
    }    
    
    /**
     * Add Content and list courses
     */ 
    public function content() {
        echo '<h3>Meus Cursos</h3>';
        if ( ! defined('LP_PLUGIN_FILE') ) {
            return;
        }
		$user_id        = get_current_user_id();
		$status         = '';
		$paged          = 1;
		$query_type     = 'purchased';
		$layout         = 'grid';

        //print_r(get_defined_constants(true));
        wp_enqueue_style( 'learnpress' );
        ob_start();
        //include plugin_dir_path( LP_PLUGIN_FILE ) . 'templates/profile/tabs/courses/course-grid.php';
        $profile = learn_press_get_profile( $user_id );

        $query = $profile->query_courses(
            $query_type,
            apply_filters(
                'learnpress/rest/frontend/profile/course_tab/query',
                array(
                    'status' => $status,
                    'limit'  => 9999,
                    'paged'  => $paged,
                )
            )
        );

		// LP_User_Item_Course.
        $query_items = $query->get_items();
		$course_item_objects = ! empty( $query_items ) ? $query_items : false;
		if ( empty( $course_item_objects ) ) {

            wc_print_notice( 'Você não adquiriu nenhum dos nossos cursos.', 'notice' );
            return;
		}

		$course_ids = array_map(
			function( $course_object ) {
				return ! is_object( $course_object ) ? absint( $course_object ) : $course_object->get_id();
			},
			$course_item_objects
		);

        $course_ids = array_filter(
            $course_ids,
            function( $course_id ) {
                return ! has_term( 'atividades', 'course_tag', $course_id );
            }
        );

        $num_pages    = $query->get_pages();
        $current_page = $query->get_paged();
        $user = learn_press_get_user( $user_id );
        $content = $layout === 'grid' ? 'profile/tabs/courses/course-grid' : 'profile/tabs/courses/course-list';

        echo learn_press_get_template_content(
            $content,
            array(
                'user'         => $user,
                'course_ids'   => $course_ids,
                'num_pages'    => absint( $num_pages ) > 1 ? absint( $num_pages ) : 1,
                'current_page' => absint( $current_page ),
            )
        );
        $html = ob_get_contents();
        ob_end_clean();

        $wc_my_acc_url = get_permalink( wc_get_page_id( 'myaccount' ) ) . 'courses/';
        $html = str_replace( $profile->get_tab_link(), $wc_my_acc_url, $html );
        echo $html;
    }
}
new Hacklab_WC_LearnPress_My_Account();
/**
 * Add Courses Tab in WooCommerce My Account 
 * @author        hacklab
 *  
 */
class Hacklab_WC_LearnPress_My_Account_Certs {
    public function __construct() {
        // add endpoint 
        add_action( 'init', array( $this, 'add_endpoint' ) );

        // query var
        add_filter( 'query_vars', array( $this, 'add_query_var' ), 0 );

        // Add link
        add_filter( 'woocommerce_account_menu_items', array( $this, 'add_link' ), 12, 1 );

        // content
        add_action( 'woocommerce_account_certificates_endpoint', array( $this, 'content' ) );
    }

    /**
     * Add Endpoint
     */
    public function add_endpoint(){
        add_rewrite_endpoint( 'certificates', EP_ROOT | EP_PAGES );
    }

    /**
     * Add Query var
     */
    public function add_query_var( $vars ) {
        $vars[] = 'certificates';
        return $vars;
    }
    /**
     * Add link
     */ 
    public function add_link( $menu_links ) {
        $new = array( 'certificates' => 'Certificados' );
        // or in case you need 2 links
        // $new = array( 'link1' => 'Link 1', 'link2' => 'Link 2' );
        // array_slice() is good when you want to add an element between the other ones
        $menu_links = array_slice( $menu_links, 0, 3, true ) + $new + array_slice( $menu_links, 3, NULL, true );
        return $menu_links;
    }    
    
    /**
     * Add Content and list courses
     */ 
    public function content() {
        if ( ! defined('LP_PLUGIN_FILE') ) {
            return;
        }
        //print_r(get_defined_constants(true));
        wp_enqueue_style( 'learnpress' );
        ob_start();

        $plugin_url = plugin_dir_url( LP_ADDON_CERTIFICATES_FILE );
		include_once plugin_dir_path( LP_ADDON_CERTIFICATES_FILE ) . '/inc/load.php';

        include_once plugin_dir_path( LP_ADDON_CERTIFICATES_FILE ) . '/inc/class-lp-certificate-database.php';
		include_once plugin_dir_path( LP_ADDON_CERTIFICATES_FILE ) . '/inc/class-lp-certificate-post-type.php';
		include_once plugin_dir_path( LP_ADDON_CERTIFICATES_FILE ) . '/inc/class-lp-certificate.php';
		include_once plugin_dir_path( LP_ADDON_CERTIFICATES_FILE ) . '/inc/class-lp-user-certificate.php';
		include_once plugin_dir_path( LP_ADDON_CERTIFICATES_FILE ) . '/inc/layers/class-lp-certificate-layer.php';
		include_once plugin_dir_path( LP_ADDON_CERTIFICATES_FILE ) . '/inc/layers/_datetime.php';
		include_once plugin_dir_path( LP_ADDON_CERTIFICATES_FILE ) . '/inc/layers/class-lp-course-name-layer.php';
		include_once plugin_dir_path( LP_ADDON_CERTIFICATES_FILE ) . '/inc/layers/class-lp-student-name-layer.php';
		include_once plugin_dir_path( LP_ADDON_CERTIFICATES_FILE ) . '/inc/class-lp-certificate-ajax.php';
		include_once plugin_dir_path( LP_ADDON_CERTIFICATES_FILE ) . '/inc/class-lp-certificate-order.php';
		include_once plugin_dir_path( LP_ADDON_CERTIFICATES_FILE ) . '/inc/class-lp-certificate-product-woo.php';
		include_once plugin_dir_path( LP_ADDON_CERTIFICATES_FILE ) . '/inc/class-lp-certificate-woo.php';
		include_once plugin_dir_path( LP_ADDON_CERTIFICATES_FILE ) . '/inc/functions.php';



        $profile = learn_press_get_profile();
        //LP_ADDON_CERTIFICATES_FILE
        //LP_Addon::load( 'LP_Addon_Certificates', 'inc/load.php', __FILE__ );
        $v_rand = uniqid();

		$localize_cer = array(
			'base_url'        => home_url(),
			'url_upload_cert' => home_url( 'upload' ),
			'url_ajax'        => admin_url( 'admin-ajax.php' )
		);

		//$assets = learn_press_assets();

		wp_register_script( 'pdfjs', $plugin_url .  'assets/js/pdf.js', array(), '1.5.3', true );
		wp_register_script( 'fabric', $plugin_url . 'assets/js/fabric.min.js', array(), '1.4.13', true );
		wp_register_script( 'downloadjs', $plugin_url . 'assets/js/download.min.js', array(), '4.2', true );

		wp_register_style( 'certificates-css', $plugin_url . 'assets/css/certificates.min.css',
			array(), LP_ADDON_CERTIFICATES_VER );
		wp_register_script( 'certificates-js', $plugin_url . 'assets/js/certificates.min.js',
			array( 'jquery' ), LP_ADDON_CERTIFICATES_VER, true );

		wp_localize_script( 'certificates-js', 'localize_lp_cer_js', $localize_cer );

        wp_enqueue_script( 'pdfjs' );
        wp_enqueue_script( 'fabric' );
        wp_enqueue_script( 'downloadjs' );
        wp_enqueue_script( 'certificates-js' );
        wp_enqueue_style( 'certificates-css' );

		global $wp;
		if ( ! empty( $wp->query_vars['act'] ) && ! empty( $wp->query_vars['cert-id'] ) ) {
			$key = $wp->query_vars['cert-id'];
			if ( $certificate = LP_Certificate::get_cert_by_key( $key ) ) {
				if ( $certificate->get_id() ) {
					learn_press_certificate_get_template( 'details.php', array( 'certificate' => $certificate ) );
				}
			}
		} else {
			$certificates = LP_Certificate::get_user_certificates( $profile->get_user()->get_id() );
			learn_press_certificate_get_template( 'list-certificates.php', array( 'certificates' => $certificates ) );
            if( empty( $certificates  ) ) {
                wc_print_notice( 'Você não possui certificados.', 'notice' );
            }
		}

        $html = ob_get_contents();
        ob_end_clean();
        $profile = learn_press_get_profile();
        $wc_my_acc_url = get_permalink( wc_get_page_id( 'myaccount' ) ) . 'certificates/';
        $html = str_replace( $profile->get_tab_link(), $wc_my_acc_url, $html );
        echo $html;
    }
}

/**
 * Include tab certificates on account page only when plugin Learnpress Certificates is activated
 */
if ( is_plugin_active( 'learnpress-certificates/learnpress-certificates.php' ) ) {
    new Hacklab_WC_LearnPress_My_Account_Certs();
}

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
/**
 * Add Courses Tab in WooCommerce My Account 
 * @author        hacklab
 *  
 */
class Hacklab_WC_LearnPress_My_Account_Atividades {
    var $term = null;

    public function __construct() {
        // add endpoint 
        add_action( 'init', array( $this, 'add_endpoint' ) );

        // query var
        add_filter( 'query_vars', array( $this, 'add_query_var' ), 0 );

        // Add link
        add_filter( 'woocommerce_account_menu_items', array( $this, 'add_link' ), 11, 1 );

        // content
        add_action( 'woocommerce_account_atividades_endpoint', array( $this, 'content' ) );
    }

    /**
     * Add Endpoint
     */
    public function add_endpoint(){
        add_rewrite_endpoint( 'atividades', EP_ROOT | EP_PAGES );
    }

    /**
     * Add Query var
     */
    public function add_query_var( $vars ) {
        $vars[] = 'atividades';
        return $vars;
    }
    /**
     * Add link
     */ 
    public function add_link( $menu_links ) {
        $new = array( 'atividades' => 'Atividades' );
        // or in case you need 2 links
        // $new = array( 'link1' => 'Link 1', 'link2' => 'Link 2' );
        // array_slice() is good when you want to add an element between the other ones
        $menu_links = array_slice( $menu_links, 0, 2, true ) + $new + array_slice( $menu_links, 2, NULL, true );
        return $menu_links;
    }
    /**
     * 
     */
    public function filter_query( $query, $user_id, $args ) {
        global $wpdb;
        $this->term = get_term_by( 'slug', 'atividades', 'course_tag', 'OBJECT' );
        $query[ 'join'] .= "
            INNER JOIN {$wpdb->term_relationships} t ON ui.item_id = t.object_id AND t.term_taxonomy_id = {$this->term->term_taxonomy_id}";
        return $query;
        //die();
    }
    /**
     * Add Content and list courses
     */ 
    public function content() {
        add_filter( 'learn-press/query/user-purchased-courses', array( $this, 'filter_query' ), 10, 3 );
        echo '<h3>Minhas Atividades</h3>';
        if ( ! defined('LP_PLUGIN_FILE') ) {
            return;
        }
		$user_id        = get_current_user_id();
		$status         = '';
		$paged          = 1;
		$query_type     = 'purchased';
		$layout         = 'grid';

        //print_r(get_defined_constants(true));
        wp_enqueue_style( 'learnpress' );
        ob_start();
        //include plugin_dir_path( LP_PLUGIN_FILE ) . 'templates/profile/tabs/courses/course-grid.php';
        $profile = learn_press_get_profile( $user_id );

        $query = $profile->query_courses(
            $query_type,
            apply_filters(
                'learnpress/rest/frontend/profile/course_tab/query',
                array(
                    'status' => $status,
                    'limit'  => 9999,
                    'paged'  => $paged,
                )
            )
        );

		// LP_User_Item_Course.

        $data = $query->get();
		$course_item_objects = ! empty( $data['items'] ) ? $data['items'] : false;

		if ( empty( $course_item_objects ) ) {

            wc_print_notice( 'Você não possui nenhuma atividade.', 'notice' );
            return;
		}

		$course_ids = array_map(
			function( $course_object ) {
				return ! is_object( $course_object ) ? absint( $course_object ) : $course_object->get_id();
			},
			$course_item_objects
		);

        $num_pages    = $query->get_pages();
        $current_page = $query->get_paged();
        $user = learn_press_get_user( $user_id );
        $content = $layout === 'grid' ? 'profile/tabs/courses/course-grid' : 'profile/tabs/courses/course-list';

        echo learn_press_get_template_content(
            $content,
            array(
                'user'         => $user,
                'course_ids'   => $course_ids,
                'num_pages'    => absint( $num_pages ) > 1 ? absint( $num_pages ) : 1,
                'current_page' => absint( $current_page ),
            )
        );
        $html = ob_get_contents();
        ob_end_clean();

        $wc_my_acc_url = get_permalink( wc_get_page_id( 'myaccount' ) ) . 'courses/';
        $html = str_replace( $profile->get_tab_link(), $wc_my_acc_url, $html );
        echo $html;
    }
}
new Hacklab_WC_LearnPress_My_Account_Atividades();

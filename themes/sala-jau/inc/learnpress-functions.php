<?php

if ( ! function_exists( 'hl_modify_course_tag' ) ) {

    /**
     * Changes the LearnPress course_tag taxonomy to display the column in the admin panel course listing
     * 
     * @link https://developer.wordpress.org/reference/functions/register_taxonomy/
     */
    function hl_modify_course_tag() {

        // get the arguments of the already-registered taxonomy
        $args = get_taxonomy( 'course_tag' );

        if ( isset( $args->show_admin_column ) ) {
            // make changes to the args
            $args->show_admin_column = true;

            // re-register the taxonomy
            register_taxonomy( 'course_tag', 'lp_course', (array) $args );
        }

    }

    // priority 11 so that it overrides the original register_taxonomy function
    add_action( 'init', 'hl_modify_course_tag', 11 );

}

if ( ! function_exists( 'hl_filter_lp_course_api' ) ) {

    /**
     * Add post type `open_lesson` on response lp_course endpoint
     * 
     * @link https://developer.wordpress.org/reference/hooks/rest_this-post_type_query/
     */
    function hl_filter_lp_course_api( $args, $request ) {

        $activity = get_term_by( 'slug', 'atividades', 'course_tag' );

        if ( $activity && $request->get_param('course_tag') && $request->get_param('course_tag')[0] == $activity->term_id ) {

            // add post type `open_lesson` to query
            $args['post_type'] = [$args['post_type'], 'open_lesson'];

        }

        $args['meta_query'] = [
            [ 'key' => '_lpr_custom_init_date' ]
        ];

        // Order courses by meta_value `_lpr_custom_init_date`
        $args['meta_key'] = '_lpr_custom_init_date';
        $args['orderby']  = 'meta_value';
        $args['order']    = 'ASC';

        return $args;

    }
    add_filter( 'rest_lp_course_query', 'hl_filter_lp_course_api', 10, 2 );

}

if ( ! function_exists( 'learn_press_co_instructor_get_instructors' ) ) {
	/**
	 * Get course co-instructors.
	 *
	 * @param null $course_id
	 *
	 * @return mixed
	 */
	function learn_press_co_instructor_get_instructors( $course_id = null ) {
		if ( ! $course_id ) {
			$course_id = learn_press_get_course_id();
		}

		return get_post_meta( $course_id, '_lp_co_teacher' );
	}
}

if ( ! function_exists( 'hl_get_instructors' ) ) {
    /**
     * Return all course instructor IDs
     */
    function hl_get_instructors( $course_id = null ) {

        if ( ! $course_id ) {
			$course_id = learn_press_get_course_id();
		}

        $instructors = [];

        if ( 'open_lesson' === get_post_type( $course_id ) ) {

            $activity_instructors = get_post_meta( $course_id, '_lp_custom_post_author' );

            if ( is_array( $activity_instructors ) ) {
                foreach ( $activity_instructors as $value ) {
                    $instructors[] = $value['ID'];
                }
            }

        } else {

            $instructors[] = get_post_field( 'post_author', $course_id );

        }

        if ( function_exists( 'learn_press_co_instructor_get_instructors' ) ) {

            $co_instructors = learn_press_co_instructor_get_instructors( $course_id );

            if ( is_array( $co_instructors ) ) {
                foreach ($co_instructors as $key => $value) {
                    $instructors[] = $value;
                }
            }

        }

        $instructors = array_filter( $instructors );

        return $instructors;

    }
}

if ( ! function_exists( 'hl_print_instructors' ) ) {
    /**
     * Print the instructors html list
     * 
     * @see _c-instructors.scss     to styling
     * @param $format               define output in name list or complete with avatar and biography []
     */
    function hl_print_instructors( $course_id = null, $format = '' ) {

        if ( ! $course_id ) {
			$course_id = learn_press_get_course_id();
		}

        $instructors = hl_get_instructors( $course_id );

        ?>
        <div class="authors instructors-list format-<?php echo sanitize_title( $format ?: 'list' ); ?>">
            <?php if ( $instructors ) : ?>

                <?php if ( 'complete' === $format ) : ?>

                    <?php foreach( $instructors as $instructor_id ) :
                        $display_name = get_the_author_meta( 'display_name', $instructor_id );
                        $description  = get_the_author_meta( 'description', $instructor_id );
                        $avatar       = get_avatar_url( $instructor_id ); ?>

                        <div class="instructor">
                            <div class="instructor--avatar">
                                <img src="<?php echo esc_url( $avatar ); ?>" alt="<?php echo esc_attr( $display_name ); ?>">
                            </div>
                            <div class="instructor--about">
                                <h3><?php echo esc_attr( $display_name ); ?></h3>
                                <?php if ( ! empty( $description ) ) : ?>
                                    <?php echo apply_filters( 'the_content', $description ); ?>
                                <?php endif; ?>
                            </div>
                        </div><!-- /.instructor -->

                    <?php endforeach; ?>

                <?php else : ?>

                    <span class="instructor-prefix"><?= __( 'com', 'buddyx' ) ?></span>
                    <span class="instructors-names">
                        <?php
                        $count_instructors = count( $instructors );
                        $iterator          = 0;

                        foreach( $instructors as $value ) {
                            $iterator++;

                            echo get_the_author_meta( 'display_name', $value );

                            if ( $count_instructors > 2 && $iterator < $count_instructors ) {
                                if ( ( $iterator + 1 ) != $count_instructors ) {
                                    _e( ', ', 'buddyx' );
                                }
                            }

                            if ( $count_instructors >= 2 && ( $iterator + 1 ) == $count_instructors ) {
                                _e( ' and ', 'buddyx' );
                            }

                        } ?>
                    </span>
                <?php endif; // $format ?>

            <?php endif; // $instructors ?>
        </div><!-- /.instructors-list -->
        <?php

    }

}

if ( ! function_exists( 'get_history_years' ) ) {
    /**
     * Return years of the all courses finished
     */
    function get_history_years() {
        $args = [
            'post_type' => ['lp_course', 'open_lesson'],
            'posts_per_page' => -1,
            'meta_query' => [
                [
                    'key'     => '_lp_custom_course_status',
                    'value'   => 'finished',
                    'compare' => '='
                ]
            ]
        ];
    
        $histories = get_posts( $args );
        $years = [];
    
        foreach( $histories as $history ) {
            $start_date_init = ( get_post_meta( $history->ID, '_lpr_custom_init_date', true ));
            if (!empty($start_date_init)) {
                $year = date( 'Y', strtotime( get_post_meta( $history->ID, '_lpr_custom_init_date', true ) ) );
            }else{
                continue;
            }
            if ( $year && ! in_array( $year, $years ) ) {
                $years[] = $year;
            }
    
        }
    
        array_filter( $years );
        rsort( $years );
    
        return array_values( $years );
    }
}

add_action( 'learn-press/single-course-summary', 'hl_add_header_single_course', 0 );

function hl_add_header_single_course() {
    get_template_part( 'template-parts/header-single-course' );
}

function hl_remove_learnpress_actions() {
    if ( class_exists( 'LearnPress' ) ) {
        LP()->template( 'course' )->remove_callback( 'learn-press/course-content-summary', 'single-course/meta-primary', 10 );
        LP()->template( 'course' )->remove_callback( 'learn-press/course-content-summary', 'single-course/meta-secondary', 10 );
        LP()->template( 'course' )->remove_callback( 'learn-press/course-content-summary', 'single-course/title', 10 );
    }
}

add_action( 'after_setup_theme', 'hl_remove_learnpress_actions' );

function hl_apply_prefix_price( $price ) {

    if ( is_singular( 'lp_course' ) ) {
        $prefix = '<span class="label">' . __( 'Price:', 'buddyx' ) . '</span>';
        $price = '<span class="value">' . $price . '</span>';
        $price = $prefix . $price;
    } 

    return $price;
}

add_filter( 'learn_press_course_price_html', 'hl_apply_prefix_price' );
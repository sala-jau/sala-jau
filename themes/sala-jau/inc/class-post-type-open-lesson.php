<?php
/**
 * 
 * Classe for manage post type open_lesson (atividades avulsas)
 * 
 */
class Hacklab_Open_lesson {
    /**
     * Init class
     */
    public function __construct() {
        add_action( 'save_post_open_lesson', array( $this, 'set_term' ), 10, 2 );
        //add_action( 'pre_get_posts', array( $this, 'filter_query_type_of_product' ), 20, 1 );
    }

    /**
     * Set term 'atividade' in all posts from this post type
     */
    public function set_term( $post_id, $post ) {
        $term = get_term_by( 'slug', 'atividades', 'course_tag', OBJECT );
        if ( $post_id && $term ) {
            wp_set_object_terms( $post_id, $term->term_id, 'course_tag' );
        }
    }
}
new Hacklab_Open_lesson();
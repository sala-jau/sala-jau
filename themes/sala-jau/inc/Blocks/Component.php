<?php
/**
 * BuddyX\Buddyx\Blocks\Component class
 *
 * @package buddyx
 */

namespace BuddyX\Buddyx\Blocks;

use BuddyX\Buddyx\Blocks\PostsList;
use BuddyX\Buddyx\Component_Interface;

/**
 * Class for managing Gutenberg block.
 *
 */
class Component implements Component_Interface {
	/**
	 * Gets the unique identifier for the theme component.
	 *
	 * @return string Component slug.
	 */
	public function get_slug() : string {
		return 'blocks';
	}

	/**
	 * Adds the action and filter hooks to integrate with WordPress.
	 */
	public function initialize() {
        add_action('init', [$this, 'initialize_blocks'], 13);
	}

    public function initialize_blocks() {
        $blocks = $this->return_blocks();
        
		foreach ( $blocks as $block ) {
            $block->initialize();
        }
    }

    protected function return_blocks() {
        return [
            new PostsList\Component(),
        ];
    }





}

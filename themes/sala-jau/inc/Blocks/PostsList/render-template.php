<?php
    $block_params = $args;
    $choosen_term = isset( $block_params['productType'] ) ? get_term( $block_params['productType'] ) : [];
    $product_status = isset( $block_params['productStatus'] ) ? $block_params['productStatus'] : null;
    $total        = -1;
    $taxonomy     = "course_tag";

    if ( 'complete' == $block_params['templateModel'] ) {
        $total = 4;
    } elseif( 'reduced' == $block_params['templateModel'] ) {
        $total = 3;
    }

    $args = [
        'post_type'      => 'lp_course',
        'posts_per_page' => $total,
        'tax_query'      => [
            [
                'taxonomy' => $taxonomy,
                'field'    => 'term_id',
                'terms'    => $choosen_term->term_id,
            ]
        ],
    ];

    if ( !is_null($product_status) ) {
        $args['meta_query'] = [
            [
                'key'     => '_lp_custom_course_status',
                'value'   => $product_status,
                'compare' => '=',
            ]
        ];
    } 

    $the_query = new \WP_Query( $args );

    // If term `atividades`, merge posts with `open_lesson` post type

    if ( sanitize_title( $choosen_term->name ) == 'atividades' ) {

        $open_lesson_args = [
            'post_type'      => 'open_lesson',
            'posts_per_page' => $total,
            'fields'         => 'ids',
            'meta_key'       => '_lp_custom_course_status',
            'meta_value'     => ['open', 'on_going']
        ];

        $open_lesson_query = new \WP_Query( $open_lesson_args );

        $merge_ids = array_merge( wp_list_pluck( $the_query->posts, 'ID' ), $open_lesson_query->posts );

        $the_query = new \WP_Query([
            'post_type'      => ['lp_course', 'open_lesson'],
            'posts_per_page' => $total,
            'post__in'       => $merge_ids,
            'orderby'        => '_lpr_custom_init_date',
            'meta_key'       => '_lpr_custom_init_date',
            'order'          => 'ASC',
            'meta_query'    =>
            [
                [
                    'key'     => '_lp_custom_course_status',
                    'value'   => ['finished','ongoing'],
                    'compare' => 'NOT IN',
                ],
                [
                    'key'       => '_lp_custom_course_status',
                    'meta_value'=> ['open']
                ],
            ],
        ]);
    }
    elseif ( sanitize_title( $choosen_term->name ) == 'cursos-gravados' ) {
        $the_query = new \WP_Query([
            'post_type'      => ['lp_course'],
            'posts_per_page' => $total,
            'tax_query'      => [
                [
                    'taxonomy' => $taxonomy,
                    'field'    => 'term_id',
                    'terms'    => $choosen_term->term_id,
                ]
            ],
            'meta_query'    =>
            [
                [
                    'key'     => '_lpr_custom_avaliable_until',
                    'value' => array(''),
                    'compare' => 'NOT IN'
                ],
                [
                    'key'     => '_lp_custom_course_status',
                    'value'   => ['finished','ongoing'],
                    'compare' => 'NOT IN',
                ],
                [
                    'key'       => '_lp_custom_course_status',
                    'meta_value'=> ['open']
                ],
            ],
            'orderby'        => 'meta_value',
            'meta_key'       => '_lpr_custom_avaliable_until',
            'order'    => 'DESC'
        ]);

    }

    elseif ( sanitize_title( $choosen_term->name ) == 'cursos-online' && ( $block_params['productStatus'] == 'open' ) ) {
        $the_query = new \WP_Query([
            'post_type'      => ['lp_course'],
            'posts_per_page' => $total,
            'tax_query'      => [
                [
                    'taxonomy' => $taxonomy,
                    'field'    => 'term_id',
                    'terms'    => $choosen_term->term_id,
                ]
            ],
            'meta_query'    =>
            [
                [
                    'key'     => '_lpr_custom_init_date',
                    'value' => array(''),
                    'compare' => 'NOT IN'
                ],
                [
                    'key'     => '_lp_custom_course_status',
                    'value'   => ['finished','ongoing'],
                    'compare' => 'NOT IN',
                ],
                [
                    'key'       => '_lp_custom_course_status',
                    'meta_value'=> ['open']
                ],
            ],
            'orderby'        => 'meta_value',
            'meta_key'       => '_lpr_custom_init_date',
            'order'    => 'ASC'
        ]);
    }

    elseif ( sanitize_title( $choosen_term->name ) == 'cursos-presenciais' ) {
        $the_query = new \WP_Query([
            'post_type'      => ['lp_course'],
            'posts_per_page' => $total,
            'tax_query'      => [
                [
                    'taxonomy' => $taxonomy,
                    'field'    => 'term_id',
                    'terms'    => $choosen_term->term_id,
                ]
            ],
            'meta_query'    =>
            [
                [
                    'key'     => '_lpr_custom_init_date',
                    'value' => array(''),
                    'compare' => 'NOT IN'
                ],
                [
                    'key'     => '_lp_custom_course_status',
                    'value'   => ['finished','ongoing'],
                    'compare' => 'NOT IN',
                ],
                [
                    'key'       => '_lp_custom_course_status',
                    'meta_value'=> ['open']
                ],
            ],
            'orderby'        => 'meta_value',
            'meta_key'       => '_lpr_custom_init_date',
            'order'    => 'ASC'
        ]);
    }

    else {
        $the_query = new \WP_Query([
            
            'post_type'      => ['lp_course'],
            'posts_per_page' => $total,
            'tax_query'      => [
                [
                    'taxonomy' => $taxonomy,
                    'field'    => 'term_id',
                    'terms'    => $choosen_term->term_id,
                ]
            ],
            
            'meta_query'    =>
            [
                [
                    'key'     => '_lpr_custom_avaliable_until',
                    'value' => array(''),
                    'compare' => 'NOT IN'
                ],
                [
                    'key'     => '_lp_custom_course_status',
                    'value'   => ['finished','open'],
                    'compare' => 'NOT IN',
                ],
                [
                    'key'       => '_lp_custom_course_status',
                    'meta_value'=> ['ongoing']
                ],
            ],
            
            'orderby'        => 'meta_value',
            'meta_key'       => '_lpr_custom_avaliable_until',
            'order'    => 'DESC'
        ]);
    }

    $models = [
        'cursos-online',
        'cursos-gravados',
        'cursos-de-ferias',
        'cursos-presenciais',
        'atividades'
    ];

    $template_matching = [
        'cursos-online' => function( $index ) use ( $block_params ) {

            set_query_var( 'cursos-online', true );
            set_query_var( 'full-width', false );

            if ( 'complete' == $block_params['templateModel'] ) {
                if ( $index == 0 ) {
                    set_query_var( 'full-width', true );
                }
            }

            $block_params['index'] = $index;
            get_template_part( 'template-parts/cards/card', 'course', $block_params );
            set_query_var( 'full-width', false );

        },

        'cursos-gravados' => function( $index ) use ( $block_params ) {

            set_query_var( 'cursos-gravados', true );
            set_query_var( 'full-width', false );

            if ( 'complete' == $block_params['templateModel'] ) {
                if ( $index == 0 ) {
                    set_query_var( 'full-width', true );
                }
            }

            $block_params['index'] = $index;
            get_template_part( 'template-parts/cards/card', 'course', $block_params );
            set_query_var( 'full-width', false );

        },
        'cursos-de-ferias' => function( $index ) use ( $block_params ) {

            set_query_var( 'cursos-de-ferias', true );
            set_query_var( 'full-width', false );

            if ( 'complete' == $block_params['templateModel'] ) {
                if ( $index == 0 ) {
                    set_query_var( 'full-width', true );
                }
            }

            $block_params['index'] = $index;
            get_template_part( 'template-parts/cards/card', 'course', $block_params );
            set_query_var( 'full-width', false );

        },
        'cursos-presenciais' => function( $index ) use ( $block_params ) {

            set_query_var( 'cursos-presenciais', true );
            set_query_var( 'full-width', false );

            if ( 'complete' == $block_params['templateModel'] ) {
                if ( $index == 0 ) {
                    set_query_var( 'full-width', true );
                }
            }

            $block_params['index'] = $index;
            get_template_part( 'template-parts/cards/card', 'course', $block_params );
            set_query_var( 'full-width', false );

        },
        'atividades' => function( $index ) {
            set_query_var( 'index', $index );
            get_template_part( 'template-parts/cards/card', 'activity', ['index' => $index] );
        },
        'default' => function( $index ) {
            get_template_part( 'template-parts/cards/card', 'default', ['index' => $index] );
        }
    ]; ?>

    <?php 
    set_query_var( 'term', $choosen_term );

    // The Loop
    if ( $the_query->have_posts() ) {

        if ( sanitize_title( $choosen_term->name ) == 'atividades' && $choosen_term->count > 1 ) {
            echo '<div class="container-slider slider-activity">';
            get_template_part( 'template-parts/slider-controls' );
        }

        echo '<div id="term-' . sanitize_title( $choosen_term->name ) . '" class="row row-term model-' . $block_params['templateModel'] . '">';

        $index = 0;
        while ( $the_query->have_posts() ) {
            $the_query->the_post(); 
            $model_name = sanitize_title( $choosen_term->name );

            if ( $index === 1 && $model_name !== 'atividades' && 'complete' == $block_params['templateModel'] ) {
                echo '<div class="wrap-second-line">';
            }

            if ( in_array( $model_name, $models ) ) {
                $template_matching[$model_name]($index);
            } else {
                $template_matching['default']($index);
            }

            $index++;

            if ( $index === $the_query->query['posts_per_page'] && $model_name !== 'atividades' && 'complete' == $block_params['templateModel'] ) {
                echo '</div>';
            }
        }

        echo '</div>';

        if ( sanitize_title( $choosen_term->name ) == 'atividades' && $choosen_term->count > 1 ) {
            echo '</div><!-- /.container-slider -->';
            get_template_part( 'template-parts/cards/card', 'activity-footer', ['choosen_term' => $choosen_term] );
        }

    } else {
        // no posts found
    }

    /* Restore original Post Data */
    wp_reset_postdata();
?>

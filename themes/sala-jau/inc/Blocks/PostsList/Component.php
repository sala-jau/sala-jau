<?php
/**
 * BuddyX\Buddyx\Blocks\ Component class
 *
 * @package buddyx
 */

namespace BuddyX\Buddyx\Blocks\PostsList;

use BuddyX\Buddyx\Component_Interface;

/**
 * Class for managing PostsList block.
 *
 */
class Component implements Component_Interface {
	/**
	 * Gets the unique identifier for the theme component.
	 *
	 * @return string Component slug.
	 */
	public function get_slug() : string {
		return 'block_postslist';
	}

	/**
	 * Adds the action and filter hooks to integrate with WordPress.
	 */
	public function initialize() {
        // automatically load dependencies and version
        $asset_file = include(get_stylesheet_directory() . '/dist/postsList.asset.php');

        wp_register_script(
            'posts-list-block',
            get_stylesheet_directory_uri() . '/dist/postsList.js',
            $asset_file['dependencies'],
            $asset_file['version']
            //filemtime(get_stylesheet_directory() . '/dist/imageBlock.js')
        );

        $post_types = get_post_types(
            [
                'public'       => true,
                'show_in_rest' => true,
            ]
        );

        $product_types = get_terms( 'course_tag', [
            'hide_empty' => false,
        ] );

        wp_localize_script(
            'posts-list-block',
            'useful_info',
            [
                'product_types' => $product_types
            ]
        );

		register_block_type('buddyx/posts-list', [
            'editor_script'   => 'posts-list-block',
            'render_callback' => [$this, 'posts_list_render'],
            'attributes'      => [
                'className' => [
                    'type' => 'string'
                ],
                'productType' => [
                    'type' => 'string'
                ],
                'productStatus' => [
                    'type' => 'string'
                ],
                'templateModel' => [
                    'type' => 'string'
                ]
            ]
        ]);
	}

    /**
     * @link https://developer.wordpress.org/reference/functions/get_template_part/
     */
    public function posts_list_render( $attributes ) {

        if ( ! isset( $attributes['productType'] ) ) {
            return '';
        }

        if ( ! isset( $attributes['templateModel'] ) ) {
            $attributes['templateModel'] = 'complete';
        }

        ob_start();
        get_template_part( 'inc/Blocks/PostsList/render', 'template', $attributes );
        $output = ob_get_clean();

        return $output;
    }
}

<?php

add_action( 'init', 'active_categories' );
//add_action( 'rest_api_init', 'api_custom_endpoints' );

function active_categories(){
    
    add_action( 'rest_api_init', function () {
        $taxonomies = ['course_category', 'modality'];
        
        foreach($taxonomies as $taxonomy){
                register_rest_route( 'wp/v2', '/'.$taxonomy.'/active(?:/(?P<id>\d+))?',  [
                    'methods' => 'GET',
                    'callback' => 'active_terms_filter',
                    'permission_callback' => '__return_true',
                ]);
            }
        });
}

function active_terms_filter($request){
    
    $params = $request->get_params();
    
    $args = [
        "post_type" => ["lp_course", "open_lesson"],
        "tax_query" => 
            [
                [
                    "taxonomy" => "course_tag",
                    "field" => "slug",
                    //"terms" => "curso" | "atividade" | "ferias"
                    "terms" => $params["term"]
                ]
            ],
        'meta_query'    => 
            [
                [
                    'key'       => '_lp_custom_course_status',
                    'value'     => ['open', 'on_going'],
                    'compare'   => 'IN',
                ]
            ]
    ];
            
    $courses = new WP_Query($args);
    
    $active_categories = [];
    $term_names = [];

    foreach($courses->posts as $course){
        $terms  = get_the_terms($course, $params["taxonomy"]);
        
        if($terms){
            $term_name = $terms[0]->name;
            if(!in_array($term_name, $term_names)){
                array_push($term_names, $term_name);
                $term = (array) $terms[0];
                $term['id'] = $term['term_id'];
                unset($term['term_id']);
                array_push($active_categories, $term);
            }

        }
    }
    
    $reponse = new WP_REST_Response($active_categories);
    $reponse->header('X-WP-TotalPages', 1);

    return $reponse;
}




<?php

//header('Content-Type: text/html; charset=utf-8');

function verifica_recaptcha( $url_google ) {
    $ch = curl_init();

    curl_setopt( $ch, CURLOPT_HEADER, 0 );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt( $ch, CURLOPT_URL, $url_google );

    $data = curl_exec( $ch );

    //curl_close($ch);

    return $data;
}


function sendDataGoogleSheets($name, $email, $whatsapp){
    
    require 'googleApi/autoload.php';

    date_default_timezone_set('America/Sao_Paulo');
    $dataCadastro = date('Y-m-d H:i:s');

    $client = new \Google_Client();
    //var_dump($client);
    
    $client->setApplicationName('Google Sheets com PHP'); //nome de referencia
    
    $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
    $client->setAccessType('offline');
    $client->setAuthConfig('salajauleads-354814-b29a8943475f.json'); // autentica pra poder escrever na planilha

    $service = new Google_Service_Sheets($client);
    
    //var_dump($service);
    $spreadsheetId = "1cbW3Ln5ugR7s3Sd-zn7_6hh6PuIwUBNPshQHpX8NVGI"; // ID da planilha (pega na URL)

    $range = 'Site'; //nome da aba da planilha
    $values = [ //valores que serão inseridos, coluna por coluna
        [
            $name,
            $email,
            $whatsapp,
            $dataCadastro
            //"thiago",
            //"thiagonegrp@gmail.com",
            //"(11)99898-9898",
            //"2022-06-29 15:47:00"
        ]
    ];

    $body = new Google_Service_Sheets_ValueRange([
        'values' => $values
    ]);

    $params = [
        'valueInputOption' => 'RAW'
    ];

    $insert = [
        'insertDataOption' => "INSERT_ROWS"
    ];

    $result = $service->spreadsheets_values->append(
        $spreadsheetId,
        $range,
        $body,
        $params,
        $insert
    );
    
    //return $result;
    
}

if ( isset( $_POST['email'] ) ) {

    if ( isset( $_POST['recaptcha'] ) ) {

        // Build POST request:
        $recaptcha_url      = 'https://www.google.com/recaptcha/api/siteverify';
        $recaptcha_secret   = '6Lfjja0gAAAAAP2HYshMoTQ7bOsenPz_nFtpUZvl';
        $recaptcha_response = $_POST['recaptcha'];

        // Make and decode POST request:
        $recaptcha = verifica_recaptcha( $recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response );
        $recaptcha = json_decode( $recaptcha );

        // Take action based on the score returned:
        if ( $recaptcha->score >= 0.5 ) {

            $name    = $_POST['name'];
            $email   = $_POST['email'];
            $whatsapp = $_POST['whatsapp'];
            $aceite  = 1;
            
            require_once 'phpmailer/class.phpmailer.php';

            // Inicia a classe PHPMailer
            $mail = new PHPMailer();
            $mail->IsSMTP(); // Define que a mensagem será SMTP
            $mail->SMTPSecure = 'tsl';
            $mail->Host       = "br88.hostgator.com.br"; // Endereço do servidor SMTP
            $mail->SMTPAuth   = true; // Usa autenticação SMTP? (opcional) - bom ter pra não ir pra spam
            $mail->Port       = 587;
            $mail->Username   = 'no-reply@site-homolog.dev'; // Usuário do servidor SMTP
            $mail->Password   = 'PBS2VY@F2GtM'; // Senha do servidor SMTP
            $mail->From     = 'no-reply@salajau.com.br'; // Seu e-mail
            $mail->FromName = 'Sala Jaú - Site'; // Seu nome

            //$mail->addAddress('alo@iodo.digital', 'IODO Digital');     //Add a recipient
            $mail->addAddress('thiago@iodo.digital', 'Thiago');     //Add a recipient
            //$mail->addAddress('ellen@example.com');               //Name is optional
            $mail->addReplyTo($email);
            //$mail->addCC('thiago@iodo.digital', 'Thiago');
            //$mail->addBCC('bcc@example.com');

            $timestamp = date( "Y-m-d H:i:s" );
            $mail->IsHTML( true ); // Define que o e-mail será enviado como HTML
            $mail->CharSet = 'utf-8'; // Charset da mensagem (opcional)

            $mail->Subject = "Aula aberta - Novo cadastro"; // Assunto da mensagem
            $mail->Body    = '<br>Data: ' . $timestamp;
            $mail->Body    .= '<br>Nome: ' . $name;
            $mail->Body    .= '<br>E-mail: ' . $email;
            $mail->Body    .= '<br>Whatsapp: ' . $whatsapp;

            // Envia o e-mail
            $enviado = $mail->Send();

            // Limpa os destinatários e os anexos
            $mail->ClearAllRecipients();
            $mail->ClearAttachments();

            sendDataGoogleSheets($name, $email, $whatsapp);
            //$gs = sendDataGoogleSheets('thiago', 'thi@iodo.digtal', '1198989989');
            //echo '<pre>'.$gs.'</pre>';
            echo '<span style="color:#ffffff;" class="msgRetorno">Cadastro realizado, em breve você receberá novidades sobre a aula aberta.</span>';
            
            

        } else {
            echo '<span style="color:#FC4613;" class="msgRetorno">Não foi possível cadastrar. Tente novamente.</span>';
        }

    } else {
        echo '<span style="color:#FC4613;" class="msgRetorno">Não foi possível cadastrar. Tente novamente.</span>';
    }
} else {
    echo 'Erro.';
}
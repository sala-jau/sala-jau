<div class="wrap-btn-add-course-to-cart">
    <form name="form-add-course-to-cart" method="post">

        <button class="lp-button btn-finished">
            <?php _e( 'Add to cart', 'learnpress-woo-payment' ); ?>
        </button>

    </form>
</div>
<?php
/**
 * @author  ThimPress
 * @package LearnPress/Templates
 * @version 4.0.0
 */

defined( 'ABSPATH' ) || exit;

$metas_mapping = [
    '_lpr_custom_observation'       => 'Pagamento:',    
];
$_lpr_custom_information_course = get_post_meta( get_the_ID(),'_lpr_custom_information_course', true );

?>

<div class="course-sidebar-preview">
	<?php
	if(!empty($_lpr_custom_information_course)){
		?>	
			<div class="information-box-wrapper">
				<div class="metabox-container">
					<div class="meta-item">
						<span class="label">Gravações:</span>
						<span class="value"><?php echo $_lpr_custom_information_course; ?></span>
					</div>
				</div>
			</div>
		<?php 
	}
	// Price box.
	if ( ! in_array( learn_press_user_course_status(), learn_press_course_enrolled_slugs() ) ) {
		LP()->template( 'course' )->course_pricing();
	}
	// Graduation.
	//LP()->template( 'course' )->course_graduation();

	// Buttons.
	LP()->template( 'course' )->course_buttons();
	

	//LP()->template( 'course' )->user_time();

	//LP()->template( 'course' )->user_progress();
	?>

	<div class="information-box-wrapper">

	<div class="metabox-container">
		<?php 
		foreach ( $metas_mapping as $key => $label ) :
			$value = get_post_meta( get_the_ID(), $key, true );
			if ( ! empty( $value ) ) :
		?>
			<div class="meta-item">
				<span class="label"><?php echo $label; ?></span>
				<span class="value"><?php echo $value; ?></span>
			</div>
		<?php endif;?> 
		<?php endforeach;?>
	</div>
</div>

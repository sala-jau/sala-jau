<?php
/**
 * Template for displaying instructor of single course.
 *
 * @author   ThimPress
 * @package  Learnpress/Templates
 * @version  3.0.0
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit();

?>

<div class="course-author">

    <h3 class="section-title"><?php _e( 'Instructor', 'learnpress' ); ?></h3>

	<?php do_action( 'sala-jau/before-single-course-instructor' ); ?>

    <?php hl_print_instructors( get_the_ID(), 'complete' ); ?>

    <?php do_action( 'sala-jau/after-single-course-instructor' ); ?>

</div>
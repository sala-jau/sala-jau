<?php
/**
 * Template for displaying own courses in courses tab of user profile page.
 * Edit by Nhamdv
 *
 * @author   ThimPress
 * @package  Learnpress/Templates
 * @version  4.0.9
 */

defined( 'ABSPATH' ) || exit();
?>
<?php 
if ( $current_page === 1 ) : ?>
<div class="lp-archive-courses">
<?php endif; ?>

	<?php
	global $post;

    //echo '<h1 class="course-status">Em aberto</h1>';
	$open_courses = array();
    $on_going_courses = array();
    $finished_courses = array();

    foreach ( $course_ids as $id ) {
		$course = learn_press_get_course( $id );

        $status = get_post_meta( $id, '_lp_custom_course_status', true );
        switch ($status) {
            case 'open':
                $open_courses[] = $id;
                break;
            case 'on_going':
                $on_going_courses[] = $id;                
                break;
            case 'finished':
                $finished_courses[] = $id;
        }
        
	}
    if ( $open_courses && ! empty( $open_courses ) ) {
        echo '<h4 class="lp-course-status open">Abertos</h4>';
        ?>
        <ul <?php lp_item_course_class( array( 'profile-courses-list' ) ); ?> data-layout="grid" data-size="3">
        <?php
        foreach ( $open_courses as $id ) {
            $course = learn_press_get_course( $id );
            $post   = get_post( $id );
            setup_postdata( $post );
                
            $course_data    = $user->get_course_data( $id );
            $course_results = $course_data->calculate_course_results();
            learn_press_get_template( 'content-course.php' );
        }
        echo '</ul>';
    }
    if ( $on_going_courses && ! empty( $on_going_courses ) ) {
        echo '<h4 class="lp-course-status on-going">Em andamento</h4>';
        ?>
        <ul <?php lp_item_course_class( array( 'profile-courses-list' ) ); ?> data-layout="grid" data-size="3">
        <?php
        foreach ( $on_going_courses as $id ) {
            $course = learn_press_get_course( $id );
            $post   = get_post( $id );
            setup_postdata( $post );
                
            $course_data    = $user->get_course_data( $id );
            $course_results = $course_data->calculate_course_results();
            learn_press_get_template( 'content-course.php' );
        }
        echo '</ul>';
    }
    if ( $finished_courses && ! empty( $finished_courses ) ) {
        echo '<h4 class="lp-course-status finished">Finalizados</h4>';
        ?>
        <ul <?php lp_item_course_class( array( 'profile-courses-list' ) ); ?> data-layout="grid" data-size="3">
        <?php
        foreach ( $finished_courses as $id ) {
            $course = learn_press_get_course( $id );
            $post   = get_post( $id );
            setup_postdata( $post );
                
            $course_data    = $user->get_course_data( $id );
            $course_results = $course_data->calculate_course_results();
            learn_press_get_template( 'content-course.php' );
        }
        echo '</ul>';
    }
    if ( empty( $finished_courses ) && empty( $on_going_courses ) && empty( $open_courses ) ) {
        wc_print_notice( 'Você não adquiriu nenhum dos nossos cursos.', 'notice' );
    }

	wp_reset_postdata();
	?>

<?php if ( $current_page === 1 ) : ?>
</ul>
</div>
<?php endif; ?>

<?php if ( $num_pages > 1 && $current_page < $num_pages && $current_page === 1 ) : ?>
	<div class="lp_profile_course_progress__nav">
		<button data-paged="<?php echo absint( $current_page + 1 ); ?>" data-number="<?php echo absint( $num_pages ); ?>"><?php esc_html_e( 'View more', 'learnpress' ); ?></button>
	</div>
<?php endif; ?>

<?php
/**
 * Template for displaying instructor of course within the loop.
 *
 * This template can be overridden by copying it to yourtheme/learnpress/loop/course/instructor.php.
 *
 * @author   ThimPress
 * @package  Learnpress/Templates
 * @version  4.0.0
 */

defined( 'ABSPATH' ) || exit();

$course = learn_press_get_course();

if ( ! $course ) {
	return;
}
?>

<span class="course-instructor">
	<?php
        if ( is_account_page() ) { 
            printf( 'com <strong>%s</strong>', wp_strip_all_tags( $course->get_instructor_html() ) );
        } else {
            echo wp_kses_post( $course->get_instructor_html() );
        }
    ?>
</span>

<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package buddyx
 */

namespace BuddyX\Buddyx;

?>
	<?php
	$classes = get_body_class();
	if ( ! in_array( 'page-template-full-width', $classes ) ) {
		?>
		</div><!-- .container -->
	<?php } ?>
	<footer id="colophon" class="site-footer pt-5 pb-5">
		<?php if ( ! function_exists( 'elementor_theme_do_location' ) || ! elementor_theme_do_location( 'footer' ) ) { ?>
			<div class="site-footer-wrapper pt-5 pb-5">
				<div class="container">
                    <div class="row">
                        <div class="col-md-12 mb-5">
                            <div class="footer-menu-container buddyx-mobile-menu">
                                <?php buddyx()->display_footer_nav_menu( array( 'menu_id' => 'footer-menu' ) ); ?>
                                <?php buddyx()->display_footer_mobile_nav_menu( array( 'menu_id' => 'footer-mobile' ) ); ?>
                            </div>
                        </div>
                        <div class="col-md-12 mt-5">
                            <div class="left-content">
                                <div class="site-branding">
                                    <div class="site-logo-wrapper">
                                        <?php the_custom_logo(); ?>
                                    </div>
                                </div><!-- .site-branding -->

                                <div class="social-icons">
                                    <a href="https://www.facebook.com/salajau.sp" target="_blank"><img src="<?php bloginfo('template_url')?>/assets/images/facebook.png" width="22" height="22" class="" alt=""></a>
                                    <a href="https://www.instagram.com/sala_jau/" target="_blank"><img src="<?php bloginfo('template_url')?>/assets/images/instagram.png" width="21" height="21" class="" alt=""></a>
                                    <a href="https://www.youtube.com/c/SalaJa%C3%BA" target="_blank"><img src="<?php bloginfo('template_url')?>/assets/images/ico_youtube.png" width="25" height="20" class="youtube" alt=""></a>
                                    <a href="https://twitter.com/sala_jau/" target="_blank"><img src="<?php bloginfo('template_url')?>/assets/images/ico_twitter.png" width="20" height="20" class="" alt=""></a>
                                    <a href="https://open.spotify.com/user/105bbh2uri7y5d93v0qlixswv?si=624e575ed8014a9c" target="_blank"><img src="<?php bloginfo('template_url')?>/assets/images/spotify.png" width="21" height="21" class="" alt="Spotify"></a>
                                    
                                </div>

                            </div>
                            
                            <div class="contact-info">
                                <ul class="m-0 p-0">
                                    <li>
                                        <a href="mailto: contato@salajau.com.br">
                                            <img src="<?= get_template_directory_uri()."/assets/images/icons/mail-b_24px.png" ?>" alt="">
                                            <span>contato@salajau.com.br</span>    
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://api.whatsapp.com/send?phone=5511934718881" target="_blank">
                                            <img src="<?= get_template_directory_uri()."/assets/images/icons/whatsapp-b_24px.png" ?>" alt="">
                                            <span>11 93471-8881</span> 
                                        </a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
				</div><!-- .container -->
			</div><!-- .site-footer-wrapper -->
			<?php //get_template_part( 'template-parts/footer/info' ); ?>
		<?php } ?>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M7FP74L"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

</body>
</html>

<?php
/**
 * Template Name: Cursos novos
 */

get_header();

$args = [
	'post_type'      => 'lp_course',
	'posts_per_page' => 99,
	'orderby' 		 => 'meta_value',
	'order'          => 'DESC',
	'meta_key'    => '_lpr_custom_avaliable_until',
	'meta_query' => [
		'relation' => 'AND',
		[
			'key' => '_lpr_custom_avaliable_until'
		],
		[
            'key'     => '_lp_custom_course_status',
			'value' => 'open'
        ],
	],
	'tax_query' => [
		'relation' => 'OR',
		[
			'taxonomy' => 'course_tag',
			'field'    => 'slug',
			'terms'    => 'cursos-online'
		]
	]
];

$courses_open = new WP_Query($args);
?>
<body class="container lp_archive_courses">

	<main class="site-main">
		<div class="index-wrapper">
			<div class="container lp_archive_courses">
				<div class="row">
					<header class="learn-press-courses-header">
						<a href="#"><h1><?= get_the_title() ?></h1></a>
						<p><?php do_action('lp/template/archive-course/description'); ?></p>
					</header>
				</div>
				<div class="row">
					<div class="col-md-12">
                    <?php
							$terms = [];
							$args = [
								'post_type'      => 'lp_course',
								'posts_per_page' => -1, // Get all posts
								'meta_query' => [
									'relation' => 'AND',
									[
										'key'     => '_lp_custom_course_status',
										'value'   => 'open'
									],
								],
								'tax_query' => [
									'relation' => 'AND',
									[
										'taxonomy' => 'course_tag',
										'field'    => 'slug',
										'terms'    => 'cursos-online'
									]
								]
							];

							$posts = new WP_Query($args);

							foreach ($posts->posts as $post) {
								$termsList = get_the_terms($post->ID, 'course_category');
								$terms[] = $termsList[0];
							}
							$termsFilter = array_unique($terms, SORT_REGULAR);

							echo '<ul class="filtro-areas"><li class="filtro-area active" data-rel="all">Todos</li>';
							foreach ($termsFilter as $term) {
								echo '<li class="filtro-area" data-rel="' . $term->slug . '">' . $term->name . '</li>';
							}
							echo '</ul>';
						?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="cards-courses-list">
							<?php if ($courses_open->have_posts()) : ?>
								<?php while ($courses_open->have_posts()) : $courses_open->the_post(); ?>
									<?php get_template_part('template-parts/cards/card-course', ''); ?>
								<?php endwhile; ?>
							<?php endif; ?>
							<?php wp_reset_postdata(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
</body>

<?php get_footer(); ?>

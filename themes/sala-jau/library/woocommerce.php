<?php

/**
* Adjust WooCommerce membership discount, ignores products in cart discount.
* Only allow membership discounts for active levels.
* Add this code to your site by following this guide - https://www.paidmembershipspro.com/create-a-plugin-for-pmpro-customizations/
*/

function my_pmpro_woo_custom_discount( $membership_price, $level_id, $price, $product ) {
global $pmprowoo_member_discounts, $current_user;

// If they don't have a level, let the original filter figure this out.
if ( ! pmpro_hasMembershipLevel() ) {
return $membership_price;
}

$discount_price = (float)$price;
$level_id = $current_user->membership_level->id;

if( $product->get_type() === 'variation' ){
$product_id = $product->get_parent_id(); //for variations
} else {
$product_id = $product->get_id();
}

// Get specific membership level price.
$level_price = get_post_meta( $product_id, '_level_' . $level_id . '_price', true );
if ( ! empty( $level_price ) || $level_price === '0' || $level_price === '0.00' || $level_price === '0,00' ) {
$discount_price = $level_price;
}

// apply discounts if there are any for this level
if ( isset( $level_id ) && ! empty( $pmprowoo_member_discounts ) && ! empty( $pmprowoo_member_discounts[ $level_id ] ) ) {
$discount_price = $price - ( $price * $pmprowoo_member_discounts[ $level_id ] );
}

return $discount_price;
}
add_filter( 'pmprowoo_get_membership_price', 'my_pmpro_woo_custom_discount', 20, 4 );

/* adiciona notificação verde acima do campo de login */
function sala_jau_add_notice_checkout(){
        if(is_checkout()):
    ?>
    <div class="woocommerce-notices-wrapper">
        <div class="woocommerce-info woocommerce-info-step1">
            Caso já possua uma conta no site da Sala Jaú, preencha os dados abaixo. Se ainda não possui cadastro, siga para a próxima etapa.</div>
        </div>
    <?php
    endif;
}
add_action('woocommerce_login_form_start','sala_jau_add_notice_checkout');

?>
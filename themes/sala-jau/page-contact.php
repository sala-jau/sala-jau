<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * 
 * Template Name: Contact page
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package buddyx
 */

namespace BuddyX\Buddyx;

get_header();

buddyx()->print_styles( 'buddyx-content' );
buddyx()->print_styles( 'buddyx-sidebar', 'buddyx-widgets' );

$default_sidebar = get_theme_mod( 'sidebar_option', buddyx_defaults( 'sidebar-option' ) );

/**
 * Remove sidebar from singular lp_course by LearnPress plugin
 */
if ( is_singular( 'lp_course' ) || is_post_type_archive( 'lp_course' ) ) {
    $default_sidebar = 'none';
}

?>
	<main id="primary" class="site-main contact-container">
		<div class="row">
			<div class="col-md-6">
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7314.530842489965!2d-46.658959!3d-23.558909!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x34166339e4cc57c6!2sUAM%20Campus%20Paulista%201!5e0!3m2!1spt-BR!2sbr!4v1620915622716!5m2!1spt-BR!2sbr" width="564" height="801" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
			</div>
			<div class="col-md-6 info-col">
					<div class="row">
						<div class="col-md-12">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div>
					</div>
					<div class="row info-container">
						<div class="col-md-12">
							<div class="row info">
								<div class="icon col-md-2 col-xs-2">
									<img src="https://salajau.com.br/wp-content/themes/salajau_theme/img/localizacao.jpg">
								</div>
								<div class="col-md-10 col-xs-10">
									<p class="mt-3 mb-0">Al. Ministro Rocha Azevedo 456, Cj 901</p>
									<p class="m-0">São Paulo - SP</p>
								</div>		
							</div>
							<div class="row info">
								<a href="https://api.whatsapp.com/send?phone=5511934718881" target="_blank">
									<div class="icon col-md-2 col-xs-2">
										<img src="https://salajau.com.br/wp-content/themes/salajau_theme/img/telefone.png">
									</div>
									<div class="col-md-10 col-xs-10">
									<a href="https://api.whatsapp.com/send?phone=5511934718881" target="_blank">
										WhatsApp - 11 93471-8881
									</a>
									</div>		
								</a>
							</div>
							<div class="row info">
								<div class="icon col-md-2 col-xs-2">
									<img src="https://salajau.com.br/wp-content/themes/salajau_theme/img/contato.png">
								</div>
								<div class="col-md-10 col-xs-10">
									<a href="mailto:contato@salajau.com.br" class="text-secondary">contato@salajau.com.br</a>
								</div>		
							</div>
						</div>
					</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 newsletter-container">
				<div class="newsletter card-course--content">
				    <h2 class="cor-principal">Entre em contato</h2>
				    <p class="mb-3">Estamos disponíveis para tirar qualquer dúvida.</p>
					<?= do_shortcode("[contact-form-7 id='1485' title='Contato']"); ?>
				</div>
			</div>
		</div>

	</main><!-- #primary -->
	

	<?php do_action( 'buddyx_after_content' ); ?>
<?php
get_footer();

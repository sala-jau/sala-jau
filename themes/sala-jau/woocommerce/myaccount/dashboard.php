<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$allowed_html = array(
	'a' => array(
		'href' => array(),
	),
);
?>

<h3 class="dashboard-hello">
    <?php _e( 'Olá, ', 'buddyx' ); ?>
	<?php echo esc_html( $current_user->display_name ); ?>
</h3>

<p>
	<?php

	/* translators: 1: Orders URL 2: Courses URL 3: Activities URL 4: Certificates URL 5: Account URL. */
	$dashboard_desc = __( 'A partir do painel de contole de sua conta, você pode acompanhar seus <a href="%1$s">pedidos</a>, acessar seus <a href="%2$s">cursos</a> e <a href="%3$s">atividades</a>, emitir seus <a href="%4$s">certificados</a>, editar sua senha e seus <a href="%5$s">dados pessoais</a>.', 'buddyx' );
	
	printf(
		wp_kses( $dashboard_desc, $allowed_html ),
		esc_url( wc_get_endpoint_url( 'orders' ) ),
		get_permalink( wc_get_page_id( 'myaccount' ) ) . 'courses/',
		get_permalink( wc_get_page_id( 'myaccount' ) ) . 'atividades/',
		get_permalink( wc_get_page_id( 'myaccount' ) ) . 'certificates/',
		esc_url( wc_get_endpoint_url( 'edit-account' ) )
	);
	?>
</p>
<p>
    <?php printf( 
        '(não é <strong>%s</strong>? Faça <a href="%s"><strong>logout</strong></a>)',
        esc_html( $current_user->display_name ),
        esc_url( wc_logout_url() )
        );?>
</p>

<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */
	do_action( 'woocommerce_account_dashboard' );

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_before_my_account' );

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_after_my_account' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
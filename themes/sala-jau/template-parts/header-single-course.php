<?php

$course_id      = get_the_ID();
$course_terms   = ( has_term( ['atividade', 'atividades'], 'course_tag' ) ) ? get_html_terms( $course_id, 'modality', true ) : get_html_terms( $course_id, 'course_category', true );
$starting_point = get_post_meta( $course_id, '_lpr_custom_init_date', true );
$avaible_until  = get_post_meta( $course_id, '_lpr_custom_avaliable_until', true );
$format_date    = get_post_meta( $course_id, '_lpr_custom_format_date', true ) ?: 'default';

?>

<header class="header-single-course">

    <?php if ( has_post_thumbnail( $course_id ) ) : ?>
        <div class="featured-image">
            <?php the_post_thumbnail( 'full' ); ?>
        </div><!-- /.featured-image -->
    <?php endif; ?>

    <div class="information-card">

        <?php echo $course_terms; ?>

        <div class="content">
            <h3 class="course-title"><?php echo apply_filters( 'the_title', get_the_title( $course_id ) ); ?></h3>

            <?php echo hl_print_instructors( $course_id, 'list' ); ?>
        </div><!-- /.content -->

        <?php if ( ! empty( $starting_point ) ) : ?>
            <div class="metabox-container">
                <?php echo format_date_course( $course_id, $format_date, true ); ?>
            </div><!-- /.metabox-container -->
        <? elseif( ! empty( $avaible_until ) ) : ?>
            <div class="metabox-container">
                <?php echo format_date_course_until( $course_id, $avaible_until, true ); ?>
            </div><!-- /.metabox-container -->
        <?php endif; ?>
    </div><!-- /.information-card.spotlight -->

</header><!-- /.header-single-course -->
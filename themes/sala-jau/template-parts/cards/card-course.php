<?php
    // Get params of the get_template_part()
    $block_params    = $args;
    $full_width_card = get_query_var( 'full-width' );
    $thumbnail       = ( has_post_thumbnail() && ! post_password_required() && is_singular() ) ? get_the_post_thumbnail_url() : false;
?>

<?php if ( $full_width_card ) : ?>
    <div class="card-course <?= (get_query_var("cursos-de-ferias")) ? 'cursos-de-ferias-card' : '' ?> col-md-12 no-side-paddings full-width">
        <div class="card-course--thumbnail col-md-8">
            <a href="<?php the_permalink() ?>">
                <?php the_post_thumbnail() ?>
            </a>
        </div>

        <?php get_template_part( 'template-parts/cards/card-course-content', '', $args ); ?>
    </div>
<?php else : ?><!-- ! $full_width_card -->
    <?php 
    $post_categories = get_the_terms( $post->ID, 'course_category' ) ? get_the_terms( $post->ID, 'course_category' ) : [];
    $categories_class = '';

    foreach ( $post_categories as $category ) {
        $categories_class .= $category->slug . ' ';
    }

    ?>
   <div class="card-course 
        <?= is_page_template('page-course-going.php') || is_page_template('page-course-open.php') ? $categories_class : ''; ?>
        <?= get_query_var("cursos-de-ferias") ? 'cursos-de-ferias-card' : ''; ?>
        <?= is_post_type_archive('lp_course') || is_page_template('page-course-going.php') || is_page_template('page-course-open.php') ? 'col-sm-6' : 'col-md-4'; ?>
    ">
        <?php get_template_part( 'template-parts/cards/card-course-content', '', $block_params ); ?>
    </div>
<?php endif ?>
<?php set_query_var( 'cursos-de-ferias', false ); ?>

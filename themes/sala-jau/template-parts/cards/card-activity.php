<?php 
    $index          = get_query_var('index');
    $course_id      = get_the_ID();
    $choosen_term   = get_query_var('term');
    $course         = LP_Global::course();
    $starting_point = get_post_meta( $course_id, '_lpr_custom_init_date', true );
    $format_date    = get_post_meta( $course_id, '_lpr_custom_format_date', true) ?: 'default';

    global $wp_query;
?>

<div class="card-activity col-md-12 no-side-paddings">
    <div class="card-activity--thumbnail">
        <?php the_post_thumbnail() ?>
    </div>

    <a href="<?php the_permalink() ?>" class="card-activity--content">
        <div class="course-terms">
            <?php echo get_html_terms( $course_id, 'modality' ); ?>
        </div>

        <h3 class="course-title">
            <?php the_title(); ?>
        </h3>

        <?php echo hl_print_instructors( $course_id, 'list' ); ?>

        <?php if ( $excerpt = hl_excerpt_by_characters( 200 ) ) : ?>
            <div class="course-excerpt">
                <?php echo apply_filters( 'the_content', $excerpt ); ?>
            </div>
        <?php endif; ?>

        <?php
        if ( ! empty( $starting_point ) ) : ?>
            <div class="metabox-container">
                <?php echo format_date_course( $course_id, $format_date, true ); ?>
            </div><!-- /.metabox-container -->
        <?php endif; ?>

    </a>
</div>
<?php

    // Get params of the get_template_part()
    $block_params = $args;

    // Get model
    $model = isset( $block_params['templateModel'] ) ? $block_params['templateModel'] : 'complete'; // Complete model as standard

    $course       = LP_Global::course();
    $course_id    = get_the_ID();

    if ( ! is_object( $course ) ) {
        $instructor = '';
    } else {
        $instructor = $course->get_instructor();
    }

    $modality        = get_html_terms( $course_id, 'modality', true );
    $course_tag      = get_html_terms( $course_id, 'course_tag', true );
    $format          = get_the_terms($course_id, 'format');
    $live_classes     = get_post_meta( $course_id, '_lpr_custom_live_classes', true );
    $course_category = get_the_terms($course_id, 'course_category');
    $avaible_until  = get_post_meta( $course_id, '_lpr_custom_avaliable_until', true );
    $starting_point  = get_post_meta( $course_id, '_lpr_custom_init_date', true );
    $finish_date     = get_post_meta( $course_id, '_lpr_custom_finish_date', true );
    $format_date     = get_post_meta( $course_id, '_lpr_custom_format_date', true ) ?: 'default';
    $observation     = get_post_meta( $course_id, '_lpr_custom_observation', true );
    $information_course  = get_post_meta( $course_id, '_lpr_custom_information_course', true );
    $thumbnail       = ( has_post_thumbnail() && ! post_password_required() ) ? get_the_post_thumbnail_url() : false;
    $index           = ( isset( $block_params['index'] ) && ! empty( $block_params['index'] ) ) ?: false;

    $is_ferias       = stripos( $course_tag, 'Curso de Férias' );

?>

    <?php if ( $index >= 1 && $thumbnail || $thumbnail && is_archive() || $thumbnail && defined('REST_REQUEST') || $thumbnail && is_page_template('page-course-going.php')|| is_page_template('page-course-open.php') || $thumbnail && 'reduced' == $model ) : ?>
        <div class="card-course--content">
            <div class="course-img" style="background-image:url( '<?php echo esc_url( $thumbnail ); ?>' )"></div>
    <?php elseif ( $index == 1 ) : ?>
        <div class="card-course--content col-sm-4">
            <div class="course-img">
                <a href="<?php the_permalink(); ?>">
                    <img src="<?= get_the_post_thumbnail_url(); ?>"> 
                </a>
            </div>
    <?php else : ?>
        <div class="card-course--content col-sm-4">
    <?php endif; ?>

    <div class="card-course--text">

        <div class="header">

            <?php if ( $course_category || $format ) : ?>
                <div class="option-row">
                    <?php if(isset($course_category[0]->name)) :  ?>
                        <div class="course-terms">
                            <a> <?php echo $course_category[0]->name; ?> </a> 
                        </div>
                    <?php endif; ?>
                    <div class="format">
                        <a> <?php echo ($format ? $format[0]->name : ''); ?> </a> 
                    </div>
                </div><!-- /.option-row -->
            <?php endif; ?>
            
            <div class="header-title">
                <?php
                /**
                 * Define tag heading de acordo com a contagem de posts
                 */
                if ( $index < 1 ) : ?>
                    <h3 class="course-title">
                        <a href="<?php the_permalink() ?>">
                            <?php the_title() ?>
                        </a>
                    </h3>
                <?php else : ?>
                    <h4 class="course-title">
                        <a href="<?php the_permalink() ?>">
                            <?php the_title() ?>
                        </a>
                    </h4>
                <?php endif; ?>

                <div class="authorship">
                    <div class="author-wrapper">
                        <div class="author-name">
                            <?php if ( is_object( $instructor ) && ! empty( $instructor->get_display_name() ) ) : ?>

                                <h3>
                                    <span><?= __("com", "buddyx") ?></span>
                                    <?php
                                        //echo $instructor->get_display_name();
                                    
                                        $instructors = hl_get_instructors( $course_id );
                                        $count_instructors = count( $instructors );
                                        $iterator          = 0;
    
                                        foreach( $instructors as $value ) {
                                            $iterator++;
    
                                            echo get_the_author_meta( 'display_name', $value );
    
                                            if ( $count_instructors > 2 && $iterator < $count_instructors ) {
                                                if ( ( $iterator + 1 ) != $count_instructors ) {
                                                    _e( ', ', 'buddyx' );
                                                }
                                            }
    
                                            if ( $count_instructors >= 2 && ( $iterator + 1 ) == $count_instructors ) {
                                                _e( ' and ', 'buddyx' );
                                            }
    
                                        } ?>

                                </h3>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="spacer">&nbsp;</div> 

        </div><!-- /.header -->

        <div class="content">

            <div class="side">
                <div class="metabox-container">
                    <?php echo format_date_course( $course_id, $format_date, true ); ?>
                </div><!-- /.metabox-container -->
                <div class="metabox-container">
                      <?php echo format_date_course_until( $course_id, $avaible_until, true ); ?>
                  </div><!-- /.metabox-container -->

                <a href="<?php the_permalink() ?>" class="btn-bg-white">
                    <?php _e( 'informações', 'buddyx' ); ?>
                </a>
            </div>
        </div>

    </div><!-- /.card-course--text -->

</div><!-- /.card-course--content -->
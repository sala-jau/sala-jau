<div class="history-area">
    <h1 class="history-main-title">
        <?= __("Histórico", "buddyx") ?>
    </h1>

    <div class="selectYear">
        <?
            $years = get_history_years();
            $actualYear = date('Y');
            foreach($years as $year){
                echo "<button class=\"btnYear ".( $year == $actualYear ? 'active' : '' )."\" ano=\"".$year."\" onclick=\"changeYear(event)\"> ".$year." </button>";
            }
        ?>
    </div>

    <div class="courses-cards">
    <?php 
    
    global $post;

    $posts = get_posts( array(
        'post_type'  => ['lp_course', 'open_lesson'],
        'nopaging'   => true,
        'orderby'    => 'meta_value',
        'meta_key'   => '_lpr_custom_init_date',
        'order'      => 'DESC',
        'meta_query' => [
            [
                'key'     => '_lp_custom_course_status',
                'compare' => '=',
                'value'   => 'finished'
            ],
            [
                'key'     => '_lpr_custom_init_date',
                'compare' => 'EXISTS'
            ]
        ]
    ) );

    $_year_mon = '';   // previous year-month value
    $_year = '';
    $_has_grp = false; // TRUE if a group was opened
    
    foreach ( $posts as $post ) {
        setup_postdata( $post );
        $time = strtotime( $post->_lpr_custom_init_date );
        $year = date( 'Y', $time );
        $mon = date( 'F', $time );
        $year_mon = "$year-$mon";

        // Open a new group.
        if ( $year_mon !== $_year_mon ) {
            // Close previous group, if any.
            if ( $_has_grp ) {
                echo '</div><!-- .month -->';
                if($year !== $_year) {
                    echo '</div><!-- .year -->';
                }
            }
            $_has_grp = true;

            if($year !== $_year) echo '<div class="year" showYear="'.$year.'">';

            echo '<div class="month">';
            echo "<div class='month-title'>" . date_i18n( 'F', $time ). "</div>";
        }

        $course_tag = get_the_terms(get_the_ID(), 'course_tag', true);
        $tag = ($course_tag) ? $course_tag[0]->name : '';

        $course_modality = get_the_terms(get_the_ID(), 'modality', true);
        $modality = ($course_modality) ? $course_modality[0]->name : ''; 
        
        $course_category   = get_the_terms(get_the_ID(), 'course_category');
        $category = ($course_category) ? $course_category[0]->name : ''; 

        ?>

        <div class="
                    history-card 
                    <?= has_post_thumbnail()? "has-thumb" : "no-thumb" ?>
                    <?= $tag=="Curso de férias" ? 'ferias' : ($tag=="Cursos" ? 'curso' : '') ?> 
                    ">
            <div class="card-img">
                <?php if(has_post_thumbnail()): ?>
                <a href="<?php the_permalink() ?>" class="thumbnail">
                    <?php the_post_thumbnail() ?>
                </a>
                <?php endif; ?>
            </div>

            <div class="sideway">
                <div class="course-terms">
                    <?php echo $tag." | ".($tag == 'Atividades' ? $modality : $category); ?>
                </div>

                <div class="title">
                    <a href="<?php the_permalink() ?>">
                        <?php the_title() ?>
                    </a>
                </div>

                <div class="excerpt">
                    <?php 
                        echo '<p>'.str_replace('...', ' [...]', hl_excerpt(55)).'</p>';
                    ?>
                </div>
            </div>
        </div><!-- /.history-card -->

        <?php
        $_year_mon = $year_mon;
        $_year = $year;
    }

    // Close the last group, if any.
    if ( $_has_grp ) {
        echo '</div><!-- .month -->';
        echo '</div><!-- .year -->';
    }

    wp_reset_postdata(); ?>
    </div>
</div>
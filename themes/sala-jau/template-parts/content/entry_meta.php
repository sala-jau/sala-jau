<?php

/**
 * Template part for displaying a post's metadata
 *
 * @package buddyx
 */

namespace BuddyX\Buddyx;

$post_type_obj = get_post_type_object(get_post_type());

$time_string = '';

// Show date only when the post type is 'post' or has an archive.
if ('post' === $post_type_obj->name || $post_type_obj->has_archive) {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if (get_the_time('U') !== get_the_modified_time('U')) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf(
		$time_string,
		esc_attr(get_the_date('c')),
		esc_html(get_the_date()),
		esc_attr(get_the_modified_date('c')),
		esc_html(get_the_modified_date())
	);

	$time_string = '<a href="' . esc_url(get_permalink()) . '" rel="bookmark">' . $time_string . '</a>';
}

$author_string = '';

// Show author only if the post type supports it.
if (post_type_supports($post_type_obj->name, 'author')) {
	$author_string = sprintf(
		'<span class="author vcard"><a class="url fn n">%2$s</a></span>',
		esc_url(get_author_posts_url(get_the_author_meta('ID'))),
		esc_html(get_the_author())
	);
}

$parent_string = '';

// Show parent post only if available and if the post type is 'attachment'.
if (!empty($post->post_parent) && 'attachment' === get_post_type()) {
	$parent_string = sprintf(
		'<a href="%1$s">%2$s</a>',
		esc_url(get_permalink($post->post_parent)),
		esc_html(get_the_title($post->post_parent))
	);
}

?>

<div class="entry-meta">
	<?php
	if (!empty($author_string)) {
	?>
		<span class="posted-by">
			<?php
			/* translators: %s: post author */
			if (class_exists('BuddyPress')) {
				echo get_avatar(get_the_author_meta('user_email'), '26');
				printf(_x('by %s', 'post author', 'buddyx'), bp_core_get_userlink($post->post_author));
			} else {
				echo get_avatar(get_the_author_meta('user_email'), $size = '26');
				$author_byline = _x('by %s', 'post author', 'buddyx');
			}
			if (!empty($time_string)) {
				/* translators: %s: post author */
				if (!class_exists('BuddyPress')) {
					$author_byline = _x('by %s', 'post author', 'buddyx');
				}
			}
			if (!class_exists('BuddyPress')) {
				printf(
					esc_html($author_byline),
					$author_string // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
				);
			}
			?>
		</span>
	<?php
	}

	if (!empty($parent_string)) {
	?>
		<span class="posted-in">
			<?php
			/* translators: %s: post parent title */
			$parent_note = _x('In %s', 'post parent', 'buddyx');
			if (!empty($time_string) || !empty($author_string)) {
				/* translators: %s: post parent title */
				$parent_note = _x('in %s', 'post parent', 'buddyx');
			}
			printf(
				esc_html($parent_note),
				$parent_string // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			);
			?>
		</span>
	<?php
	}

	if (!empty($time_string)) {
	?>
		<span class="posted-on">
		<svg class="fa-calendar-day" width="16" height="18" viewBox="0 0 16 18" xmlns="http://www.w3.org/2000/svg" style="margin-right:5px">
			<path d="M0 16.3125C0 17.2441 0.755859 18 1.6875 18H14.0625C14.9941 18 15.75 17.2441 15.75 16.3125V6.75H0V16.3125ZM2.25 9.5625C2.25 9.25313 2.50312 9 2.8125 9H6.1875C6.49687 9 6.75 9.25313 6.75 9.5625V12.9375C6.75 13.2469 6.49687 13.5 6.1875 13.5H2.8125C2.50312 13.5 2.25 13.2469 2.25 12.9375V9.5625ZM14.0625 2.25H12.375V0.5625C12.375 0.253125 12.1219 0 11.8125 0H10.6875C10.3781 0 10.125 0.253125 10.125 0.5625V2.25H5.625V0.5625C5.625 0.253125 5.37187 0 5.0625 0H3.9375C3.62812 0 3.375 0.253125 3.375 0.5625V2.25H1.6875C0.755859 2.25 0 3.00586 0 3.9375V5.625H15.75V3.9375C15.75 3.00586 14.9941 2.25 14.0625 2.25Z"></path>
		</svg>
		<path d="M0 16.3125C0 17.2441 0.755859 18 1.6875 18H14.0625C14.9941 18 15.75 17.2441 15.75 16.3125V6.75H0V16.3125ZM2.25 9.5625C2.25 9.25313 2.50312 9 2.8125 9H6.1875C6.49687 9 6.75 9.25313 6.75 9.5625V12.9375C6.75 13.2469 6.49687 13.5 6.1875 13.5H2.8125C2.50312 13.5 2.25 13.2469 2.25 12.9375V9.5625ZM14.0625 2.25H12.375V0.5625C12.375 0.253125 12.1219 0 11.8125 0H10.6875C10.3781 0 10.125 0.253125 10.125 0.5625V2.25H5.625V0.5625C5.625 0.253125 5.37187 0 5.0625 0H3.9375C3.62812 0 3.375 0.253125 3.375 0.5625V2.25H1.6875C0.755859 2.25 0 3.00586 0 3.9375V5.625H15.75V3.9375C15.75 3.00586 14.9941 2.25 14.0625 2.25Z"></path>
			<?php
			printf(
				/* translators: %s: post date */
				esc_html_x(' %s', 'post date', 'buddyx'),
				$time_string // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			);
			?>
		</span>
	<?php
	}
	buddyx_posted_on();
	?>
</div><!-- .entry-meta -->
<?php

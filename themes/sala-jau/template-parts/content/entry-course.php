<?php
/**
 * Template part for displaying a post's content
 *
 * @package buddyx
 */

namespace BuddyX\Buddyx;

$course      = \LP_Global::course();
$instructor  = $course->get_instructor();
$thumbnail   = ( has_post_thumbnail() && ! post_password_required() && is_singular() ) ? get_the_post_thumbnail_url() : false;
$format_date = get_post_meta( $course_id, '_lpr_custom_format_date', true ) ?: 'default';

?>

<div class="site-sub-header">
		<div class="container">
    <?php 
        //get_template_part( 'template-parts/content/entry_title', get_post_type() );
        $breadcrumbs = get_theme_mod( 'site_breadcrumbs', buddyx_defaults( 'site-breadcrumbs' ) );

        if ( ! empty( $breadcrumbs ) ) {
            buddyx_the_breadcrumb();
        }
    ?>
        </div>
</div>

<div class="entry-content course-content">

    <div class="featured-area" style="background-image:url( '<?php echo esc_url( $thumbnail ); ?>' )">
        <div class="overlay"></div>
        <div class="course-basic">
            <div class="terms">
                <?php 
                    $terms = get_the_terms(get_the_ID(), 'course_category');
                    if($terms):
                        foreach($terms as $term):
                ?>
                        <a href="<?= get_term_link($term) ?>"> <?= $term->name ?> </a>
                <?php 
                        endforeach; 
                    endif;
                ?>
            </div>

            <h3 class="course-title">
                <?php the_title() ?>
            </h3>

            <h6 class="author">
                <div class="author-wrapper">
                    <div class="author-name">
                        <?php if ( is_object( $instructor ) && ! empty( $instructor->get_display_name() ) ) : ?>
                            <span><?= __("Com", "buddyx") ?></span>
                            <?php echo $instructor->get_display_name(); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </h6>

            <div class="metabox-container">
                <?php echo format_date_course( $course_id, $format_date, true ); ?>
            </div><!-- /.metabox-container -->

        </div>
    </div><!-- ./featured-area -->
    
    <?php 
	the_content();

	wp_link_pages(
		array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'buddyx' ),
			'after'  => '</div>',
		)
	);
	// Show comments only when the post type supports it and when comments are open or at least one comment exists.
	if ( post_type_supports( get_post_type(), 'comments' ) && ( comments_open() || get_comments_number() ) ) {
		comments_template();
	}
	?>
</div><!-- .entry-content -->

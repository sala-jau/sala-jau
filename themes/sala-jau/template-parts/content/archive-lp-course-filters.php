<?php
    
    global $wp_query;

    $taxonomy = ( is_tax( 'course_tag', 'atividades' ) ) ? 'modality' : 'course_category';
    $termSlug = $wp_query->get_queried_object()->slug;

    $primary_filters = [
        [   
            "type" => "quick-selection",
            "placeholder" => '', 
            "filterSettings" => [ 
                "route" => $taxonomy."/active",
                "params" => [
                        "taxonomy" => $taxonomy,
                        "term" => $termSlug,
                ]
            ] 
        ],
    ];

    $card_model = "CourseCard";
    $baseURL = get_rest_url(null, '/wp/v2/lp_course');

    $terms = get_terms('course_category', [
        'orderby' => 'title',
        'order' => 'desc',
        'hide_empty' => false,
    ]);

    $terms_meta = [];

    foreach($terms as $term) {
        $icon_meta  = get_term_meta($term->term_id, 'icon', true);
        $color_meta = get_term_meta($term->term_id, 'color', true);

        $terms_meta[$term->term_id] = [
            'color' => $color_meta,
            'icon'  => $icon_meta,
        ];
    }

    /**
     * Get terms of the `course_category` taxonomy
     */
    $postArea = get_terms('course_category', [
        'orderby'    => 'course_category',
        'hide_empty' => false,
    ]);

    $data_building = [
        'filters'               => [],
        'primaryFilters'        => $primary_filters,
        'cardModel'             => $card_model,
        'baseURL'               => $baseURL,
        'addicionalCardMeta'    => [],
        'additionalQueryParams' => [
            'taxonomy' => isset( $wp_query->get_queried_object()->taxonomy ) ? $wp_query->get_queried_object()->taxonomy : false,
            'term'     => isset( $wp_query->get_queried_object()->term_id ) ? $wp_query->get_queried_object()->term_id : false,
            'termSlug' => isset( $termSlug ) ? $termSlug : false,
        ]
    ];

?>

<div id="faceted-posts-search" data-building="<?= htmlentities(json_encode($data_building)) ?>"></div>
<?php
/**
 * Template part for displaying a post's content
 *
 * @package buddyx
 */

namespace BuddyX\Buddyx;

$course_id      = get_the_ID();
$starting_point = get_post_meta( $course_id, '_lpr_custom_init_date', true );
$format_date    = get_post_meta( $course_id, '_lpr_custom_format_date', true ) ?: 'default'; ?>

<div class="entry-content">

    <header>
        <div class="course-terms">
            <?php echo get_html_terms( $course_id, 'modality', true ); ?>
        </div>

        <h1 class="entry-title">
            <?php the_title(); ?>
        </h1>

        <div class="entry-meta">
            <?php hl_print_instructors( $course_id, 'list' ); ?>
            <?php
            if ( ! empty( $starting_point ) ) : ?>
                <div class="metabox-container">
                    <?php echo format_date_course( $course_id, $format_date, true ); ?>
                </div><!-- /.metabox-container -->
            <?php endif; ?>
        </div>
    </header>

	<?php
	if ( has_post_thumbnail() && ! post_password_required() && is_singular() ) {
		?>
	
		<figure class="featured-media">
			<div class="featured-media-inner section-inner">
				<?php the_post_thumbnail(); ?>
			</div><!-- .featured-media-inner -->
		</figure><!-- .featured-media -->
	
		<?php
	} ?>

    <div class="the-content">
        <?php the_content(); ?>
    </div><!-- /.the-content -->
</div><!-- /.entry-content -->

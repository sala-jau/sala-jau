<div class="courses-area">
    <h1 class="entry-title">CURSOS</h1>
    <div class="courses-cards">
    <?php 
    $args = [
        'post_type' => 'lp_course'
    ];

    $the_query = new \WP_Query( $args );

    // The Loop
    if ( $the_query->have_posts() ) {
        $count = 0;
        while ( $the_query->have_posts() ) {
            $the_query->the_post(); ?>
            
            <?php if($count == 0): ?>
                <div class="course-card first">
                    <div class="course-thumbnail">
                        <?php the_post_thumbnail() ?>
                    </div>
                    <div class="sideway">
                        <div class="taxonomy">
                            <?php 
                                $terms = get_the_terms(get_the_ID(), 'course_category');

                                if(!empty($terms) && sizeof($terms)): ?>
                                    <a href="<?= get_term_link($terms[0]) ?>"><?= $terms[0]->name ?></a>
                                <?php endif;
                            ?>
                        </div>

                        <h3><a href="<?php the_permalink() ?>" class="title"><?php the_title() ?></a></h3>

                    </div>
                </div>

                <div class="row">
            <?php else: ?>
                <div class="course-card col-md-4">
                    <?php the_post_thumbnail() ?>
                    <div class="heading">
                        <div class="taxonomy">
                            <?php 
                                $terms = get_the_terms(get_the_ID(), 'course_category');

                                if(!empty($terms) && sizeof($terms)): ?>
                                    <a href="<?= get_term_link($terms[0]) ?>"><?= $terms[0]->name ?></a>
                                <?php endif;
                            ?>
                        </div>

                        <h3><a href="<?php the_permalink() ?>" class="title"><?php the_title() ?></a></h3>
                    </div>
                </div>
            <?php endif; ?>
            <?php
            $count++;
        }
        ?> </div> <?php
    } else {
        // no posts found
    }
    /* Restore original Post Data */
    wp_reset_postdata();

    ?>
    </div>
</div>
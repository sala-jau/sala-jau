<?php
/**
 * Template for displaying course content within the loop.
 *
 * This template can be overridden by copying it to yourtheme/learnpress/content-single-course.php
 *
 * @author  ThimPress
 * @package LearnPress/Templates
 * @version 3.0.0
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit();

if ( post_password_required() ) {
	echo get_the_password_form();

	return;
}

$metas_mapping = [
    '_lpr_custom_init_date'         => 'Início:',
    '_lpr_custom_finish_date'       => 'Término:',
    '_lpr_custom_avaliable_until'   => 'Gravações até: ',
    '_lpr_custom_live_classes'      => 'Aulas (ao vivo): ',
    '_lpr_custom_days'              => 'Dias:',
    '_lpr_custom_time'              => 'Horário:',
    '_lpr_custom_number_of_classes' => 'Número de aulas:',
    
]; 
$_lpr_custom_live_classes = get_post_meta( get_the_ID(),'_lpr_custom_live_classes', true );
?>

<div class="information-box-wrapper">

    <span class="title">Informações</span>
    <div class="metabox-container">
        <?php 
       
        foreach ( $metas_mapping as $key => $label ) :
            $value = get_post_meta( get_the_ID(), $key, true );
            if ( ! empty( $value ) ) :
                if ( $key == '_lpr_custom_init_date' || $key == '_lpr_custom_finish_date' ) {
                    $value = date_i18n( get_option( 'date_format' ), strtotime( $value ) );
                }
                elseif($key == '_lpr_custom_avaliable_until'){
                    $value = date_i18n ('d/m/y', strtotime($value));
                } ?>

            <div class="meta-item">

                <span class="label"><?php echo $label; ?></span>
                <span class="value"><?php echo $value; ?></span>
            </div>
        <?php
            endif; 
        endforeach;

        $course_terms = get_html_terms( get_the_ID(), 'format' );

        if ( ! empty( $course_terms ) ) : ?>
            <div class="meta-item">
                <span class="label">Formato: </span>
                <span class="value"><?php echo $course_terms; ?></span>
            </div>
        <?php endif; ?>
    </div>
    

</div>
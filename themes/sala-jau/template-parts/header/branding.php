<?php
/**
 * Template part for displaying the header branding
 *
 * @package buddyx
 */

namespace BuddyX\Buddyx;

?>
<div class="left-content">
	<div class="site-branding">
		<div class="site-logo-wrapper">
			<?php the_custom_logo(); ?>
		</div>
	</div><!-- .site-branding -->

	<div class="social-icons">
		<a href="https://www.facebook.com/salajau.sp/" target="_blank"><img src="<?php bloginfo('template_url')?>/assets/images/facebook.png" width="20" height="20" alt="Facebook"></a>
		<a href="https://www.instagram.com/sala_jau/" target="_blank"><img src="<?php bloginfo('template_url')?>/assets/images/instagram.png" width="20" height="20" class="" alt="Instagram"></a>
		<a href="https://www.youtube.com/c/SalaJa%C3%BA" target="_blank"><img src="<?php bloginfo('template_url')?>/assets/images/ico_youtube.png" class="" alt="Youtube"></a>
		<a href="https://twitter.com/sala_jau" target="_blank"><img src="<?php bloginfo('template_url')?>/assets/images/ico_twitter.png" class="" alt="Twitter"></a>
		<a href="https://open.spotify.com/user/105bbh2uri7y5d93v0qlixswv?si=624e575ed8014a9c" target="_blank"><img src="<?php bloginfo('template_url')?>/assets/images/spotify.png" class="" alt="Spotify"></a>
	</div>
</div>

<?php

$args = [
    'post_type'      => 'slider',
    'posts_per_page' => 10,
    'order'          => 'ASC',
    'orderby'        => 'menu_order'
];
$sliders = new WP_Query( $args );

if ( $sliders->have_posts() ) : ?>

<div class="container container-slider">

    <?php get_template_part( 'template-parts/slider-controls' ); ?>

    <ul id="home-slider">
        <?php while ( $sliders->have_posts() ) :
            $sliders->the_post();

            $slider_id = get_the_ID();

            $course = get_post_meta( $slider_id, 'course', true );
            $activity = get_post_meta( $slider_id, 'open_lesson', true );
            $item = false;

            if(!empty($course) && isset($course)){
                $item = $course;
            }elseif(!empty($activity) && isset($activity)){
                $item = $activity;
            }

            if(!empty($item)):
                // Get metadata
                $thumbnail = get_the_post_thumbnail_url($item['ID']);
                $teacher = get_post_meta( $slider_id, 'teacher', true );
                $inicial_date = get_post_meta( $slider_id, 'inicial_date', true );
                $title  = $item['post_title'];
                $link   = get_the_permalink( $item['ID'] );
                $area   = get_html_terms( $slider_id, 'course_category', true );
                
                $spotlight = ( $title || $area ) ? true : false;
                
                // Course option defined to slider
                if ( isset($thumbnail) &&  !empty($thumbnail)  && $item ) :
                    
                    // Get course information
                    
                    $instructors     = hl_get_instructors( $item['ID'] );
                    $course_category = get_html_terms( $item['ID'], 'course_category', true );
                    $modality = get_html_terms( $item['ID'], 'modality', true );
                    $starting_point  = get_post_meta( $item['ID'], '_lpr_custom_init_date', true );
                    $format_date     = get_post_meta( $item['ID'], '_lpr_custom_format_date', true ) ?: 'default';
                    ?>

                    <li>
                        <img src="<?= $thumbnail ?>">
                        
                        <?php if ( $spotlight ) : ?>
                            <div class="spotlight">
                                <?= $course ? $course_category : $modality; ?>

                                <div class="content">
                                    <?php if ( $title ) : ?>
                                        <h3 class="course-title"><?php echo apply_filters( 'the_title', $title ); ?></h3>
                                    <?php endif; ?>

                                    <?php echo hl_print_instructors( $item['ID'], 'list' ); ?>
                                </div><!-- /.content -->

                                <?php
                                if ( ! empty( $starting_point ) ) : ?>
                                    <div class="metabox-container">
                                    <?php echo format_date_course( $item['ID'], $format_date, true ); ?>
                                    </div><!-- /.metabox-container -->
                                <?php endif; ?>

                            </div>
                        <?php endif; ?>

                        <?php if ( isset( $link ) && ! empty( $link ) ) : ?>
                            <a class="link-full" href="<?php echo esc_url( $link ); ?>"></a>
                        <?php endif; ?>

                    </li>
            <?php
                    endif; 
                else:
                    $image  = get_post_meta( $slider_id, 'image', true );
                    $link = get_post_meta( $slider_id, 'link', true );
                    
                    if(!empty($image['guid']) && isset($image['guid'])): ?>
                        <li>
                            <a href="<?= (isset($link) && !empty($link)) ? $link : '#';  ?>">
                                <img src="<?= esc_url( $image['guid']); ?>">
                            </a>
                        </li>
            <?php   
                endif; 
            endif;
        endwhile; ?>
    </ul>

</div>

<?php endif; ?>
<?php wp_reset_postdata(); ?>

<?php

$post_id = get_the_ID();

$terms = get_the_terms( $post_id, 'category' );

if ( $terms && ! is_wp_error( $terms ) ) {
    $terms = wp_list_pluck( $terms, 'term_id' );
}

$args = [
    'post_type'      => 'blog',
    'posts_per_page' => 3,
    'post__not_in'   => [ $post_id ],
    'order'          => 'DESC',
    'tax_query'      => [
        [
            'taxonomy' => 'category',
            'terms'    => $terms
        ]
    ],
];

$related_posts = new WP_Query( $args );

if ( $related_posts->have_posts() ) : ?>

<h2 class="related-blog-title">Postagens recentes</h2>
<div class="related related-blog">
        <?php while( $related_posts->have_posts() ) :
            $related_posts->the_post();

            // Thumbnail
            $thumbnail = ( has_post_thumbnail() ) ? get_the_post_thumbnail() : '<img src="' . get_stylesheet_directory_uri() . '/assets/images/default-image.png">'; ?>
            <a href="<?php the_permalink(); ?>">
                <div class="related-post">
                    <div class="related-post-image"><?php echo $thumbnail; ?></div>
                    <div class="related-post-content">
                        <span class="category"><p><?php echo get_html_terms( get_the_ID(), 'category' ); ?></p></span>
                        <div class="title">
                            <h6><?php the_title(); ?></h6>
                        </div>
                        <div class="entry-dados">
                            <div class="author">
                              <?php echo 'Por ', get_the_author(); ?> 
                            </div>
                           <div class="date">
                               <?php echo get_the_date(); ?>
                           </div>
                        </div>
                        
                        <div class="resumo">
                            <?php echo get_the_excerpt(); ?>
                        </div>
                      
                    </div>
                </div>
            </a>
        <?php endwhile; ?>
    </div>

<?php endif;
wp_reset_postdata(); ?>

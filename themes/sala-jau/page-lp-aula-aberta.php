<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

$temaUrl = get_stylesheet_directory_uri() . '/';
$siteUrl = site_url() . '/';

//Campos personalizados
$bannerPrincipal = get_field('banner_principal'); //grupo
$sobreCurso = get_field('sobre_o_curso'); //grupo
$aulaAberta = get_field('aula_aberta'); //grupo
$faixaFooter = get_field('faixa_footer'); //grupo

?>


<html class="lp-cursos">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-M7FP74L');</script>
    <!-- End Google Tag Manager -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-205292219-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-205292219-1');
    </script>

    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!---YOST WORDPRESS TITLE AND TAGS / TAG MANAGER-->
    <title>Aula Aberta | Sala Jaú</title>
    <link rel="stylesheet" href="<?php echo $temaUrl; ?>assets-lp/css/styles.css" type="text/css">

    <link rel="stylesheet" type="text/css" href="<?php echo $temaUrl; ?>assets-lp/scripts/slick.css"/>

    <!--Video Custaom Player-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.mb.YTPlayer/3.3.9/css/jquery.mb.YTPlayer.min.css"
          media="all" rel="stylesheet" type="text/css">

    <!--animations -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M7FP74L"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<header>
    <div class="container">
        <div class="todoTopo gridFlex">
            <div class="logo">
                <a href="<?php echo $siteUrl; ?>">
                    <img src="<?php echo $temaUrl; ?>assets-lp/images/logo-salajau.png" alt="Sala Jau">
                </a>
            </div>

            <nav class="menu">
                <ul>
                    <li><a href="">Inscreva-se</a></li>
                    <li><a href="#sobre-o-curso" class="samePage">Curso</a></li>
                    <li><a href="#aula-especial" class="samePage">Aula Aberta</a></li>
                    <li><a href="#proximosCursos" class="samePage">Outros Cursos</a></li>
                </ul>
            </nav>

            <div class="menuMobile">
                <div class="iconMenu">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>

        </div>
    </div>
</header>

<section id="Topo">
    <div class="bannerContent">
        <div class="container">
            <div class="gridFlex">

                <div class="colFlex Text" data-aos="fade-right"
                     data-aos-duration="1000">
                    <div class="contentText">
                        <h1><?php echo $bannerPrincipal['chamada_banner']; ?></strong></h1>
                        <p><?php echo $bannerPrincipal['descricao']; ?></p>
                        <p class="destaque"><?php echo $bannerPrincipal['texto_destaque']; ?></p>

                        <div class="mensagemForm" style="display:none;"></div>

                        <form id="frmInscricao" method="post">

                            <div class="itemForm">
                                <label class="obrigatorio">Nome</label>
                                <input type="text" name="txtNome" id="txtNome" class="inputPadrao"/>
                            </div>


                            <div class="itemForm">
                                <label class="obrigatorio">Email</label>
                                <input type="text" name="txtEmail" id="txtEmail" class="inputPadrao"/>
                            </div>


                            <div class="itemForm">
                                <label class="Opcional">Whatsapp</label>
                                <input type="text" name="txtWhatsapp" id="txtWhatsapp" class="inputPadrao"
                                       onFocus="criaMascara(this,'(##) #####-####');"
                                       onBlur="criaMascara(this,'(##) #####-####');"
                                       onKeyPress="criaMascara(this,'(##) #####-####');"
                                       onKeyUp="criaMascara(this,'(##) #####-####');"/>
                            </div>

                            <input type="hidden" name="recaptcha_response" id="recaptchaResponse">

                            <div class="Botao">
                                <input type="submit" id="btnInscrever" value="me inscrever"/>
                            </div>

                            <div id="loader" style="display: none;">Aguarde...</div>

                            <p>Ao se cadastrar você aceita receber conteúdos exclusivos sobre a Sala Jaú e concorda com
                                nossa <a href="https://salajau.com.br/politica-de-privacidade/" target="_blank">Política
                                    de Privacidade</a>.</p>

                        </form>

                    </div>
                </div>

                <div class="colFlex Images" data-aos="fade-left"
                     data-aos-duration="1000">
                    <div class="contentImages">
                        <div class="spot1">
                            <img src="<?php echo $bannerPrincipal['imagem_banner']['url']; ?>"
                                 alt="<?php echo $bannerPrincipal['imagem_banner']['alt']; ?>">
                        </div>

                        <div class="spot2">
                            <img src="<?php echo $bannerPrincipal['imagem_banner_2']['url']; ?>"
                                 alt="<?php echo $bannerPrincipal['imagem_banner_2']['alt']; ?>">
                        </div>

                        <div class="spot3">
                            <img src="<?php echo $bannerPrincipal['imagem_banner_3']['url']; ?>"
                                 alt="<?php echo $bannerPrincipal['imagem_banner_2']['alt']; ?>">
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <div class="btnTopoScroll">
                <span class="icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="17.414" height="10.121" viewBox="0 0 17.414 10.121">
                        <path id="Path_2208" data-name="Path 2208" d="M1543.75,7270.031l8-8,8,8"
                              transform="translate(1560.457 7270.738) rotate(180)" fill="none" stroke="#fff"
                              stroke-width="2"/>
                    </svg>
                </span>
        </div>

    </div>


</section>


<main class="mainContent">
    <section id="sobre-o-curso" data-aos="fade-up"
             data-aos-duration="1000">
        <div class="container">
            <div class="gridFlex">
                <div class="colFlex TxtSobre">
                    <div class="contItem">

                        <!--<h2>Sobre o curso</h2>-->
                        <h2><?php echo $sobreCurso['chamada']; ?></h2>
                        <p><?php echo $sobreCurso['descricao_do_curso']; ?></p>

                        <div class="programaCurso">
                            <h3>Programa do Curso</h3>
                            <div class="acordeonPrograma">
                                
                                <?php foreach ($sobreCurso['programa_do_curso'] as $programa): ?>
                                <div class="itemAcordeon">
                                    <div class="titleAcordeon">
                                        <h4><?php echo $programa['titulo_aula']; ?></h4>
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="17.414"
                                                 height="10.121" viewBox="0 0 17.414 10.121">
                                            <path id="Path_2211" data-name="Path 2211"
                                                  d="M1543.75,7270.031l8-8,8,8"
                                                  transform="translate(1560.457 7270.738) rotate(180)"
                                                  fill="none" stroke="#666" stroke-width="2"/>
                                            </svg>
                                        </span>
                                    </div>
                                    <div class="contentAcordeon">
                                        <p><?php echo $programa['descricao_aula']; ?></p>
                                    </div>
                                </div>
                                <?php endforeach; ?>

                            </div>
                        </div>

                    </div>
                </div>

                <div class="colFlex cardCurso">
                    <div class="contentCard contentCardLateral">
                        <div class="imgCard imgCardLateral" style="">
                            <img src="<?php echo $sobreCurso['card_curso']['imagem_card']['url']; ?>"
                                 alt="<?php echo $sobreCurso['card_curso']['imagem_card']['alt']; ?>">
                        </div>

                        <div class="caixaCard">

                            <div class="tags">
                                <span><strong><?php echo $sobreCurso['card_curso']['categoria']; ?></strong></span>
                                <span><?php echo $sobreCurso['card_curso']['local']; ?></span>
                            </div>

                            <div class="tituloCard">
                                <h4><?php echo $sobreCurso['card_curso']['titulo_card']; ?></h4>
                            </div>

                            <div class="com">
                                <p><?php echo $sobreCurso['card_curso']['participantes']; ?></p>
                            </div>


                            <div class="moreInfo">
                                <div class="agenda">
                                    <img src="<?php echo $temaUrl; ?>assets-lp/images/icon-calendar.png"
                                         alt="Calendário">

                                    <p><?php echo $sobreCurso['card_curso']['data']; ?></p>
                                </div>


                                <div class="btnInfo">
                                    <a href="<?php echo $sobreCurso['card_curso']['link_botao']; ?>" class="btnPrimary"><?php echo $sobreCurso['card_curso']['texto_botao']; ?></a>
                                </div>


                            </div>


                        </div>


                    </div>
                </div>


            </div>

        </div>
    </section>

    <div class="separador"></div>

    <section id="vantagens" data-aos="fade-up"
             data-aos-duration="1000">
        <div class="container">
            <div class="gridFlex">

                <div class="itemVantagem">
                    <div class="contentItem">
                        <div class="icon">
                            <img src="<?php echo $temaUrl; ?>assets-lp/images/icon-desconto.jpg"
                                 alt="Descontos Especiais">
                        </div>
                        <h3>Descontos Especiais</h3>
                        <p>15% de desconto para <strong>Estudantes e Professores</strong> em todos os cursos e
                            atividades.</p>
                    </div>
                </div>


                <div class="itemVantagem">
                    <div class="contentItem">
                        <div class="icon">
                            <img src="<?php echo $temaUrl; ?>assets-lp/images/icon-pagamento.jpg"
                                 alt="Facilidade de Pagamento">
                        </div>
                        <h3>Facilidade de Pagamento</h3>
                        <p>Pagamento à vista - por transferência, PIX, boleto e PayPal - ou em até 2 vezes por cartão de
                            crédito.</p>
                    </div>
                </div>


                <div class="itemVantagem">
                    <div class="contentItem">
                        <div class="icon">
                            <img src="<?php echo $temaUrl; ?>assets-lp/images/icon-gravacao.jpg" alt="Gravações">
                        </div>
                        <h3>Gravações</h3>
                        <p>As aulas serão gravadas e ficarão disponíveis por dois meses após o término do curso.</p>
                    </div>
                </div>

                <div class="itemVantagem">
                    <div class="contentItem">
                        <div class="icon">
                            <img src="<?php echo $temaUrl; ?>assets-lp/images/icon-certificate.jpg" alt="Certificado">
                        </div>
                        <h3>Certificado</h3>
                        <p>A Sala Jaú emitirá um certificado de participação no curso.</p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    
    <!-- SE TIVER PROXIMA AULA ABERTA, MOSTRA ESSA SESSÃO -->
    <?php if($aulaAberta['proxima_aula_agendada'] == 'sim'){ ?>
    <section id="aula-especial" data-aos="fade-up" data-aos-duration="1000">
        <div class="container">


            <h2>Assista à <strong>aula aberta</strong> especial</h2>

            <div class="video">
                <div id="MyPlayer" class="video1 player"
                     data-property="{videoURL:'<?php echo $aulaAberta['link_da_aula']; ?>',autoPlay:false,ratio:'16/9', abundance: 0,containment:'self', startAt:0, opacity:1, showAnnotations:false,anchor:'top,left',realfullscreen:false,coverImage:'<?php echo $aulaAberta['imagem_da_aula']['url']; ?>'}">
                </div>

            </div>
        </div>
    </section>
    <?php } ?>


    <section id="proximosCursos" data-aos="fade-up"
             data-aos-duration="1000">
        <div class="container">

            <h2>Próximos cursos na Sala Jaú</h2>

            <div class="todoCarroselCursos">

                <?php
                $args = array(
                    //'orderby' => '_lp_duration',
                    //'meta_key' => '_lpr_custom_init_date',
                    //'meta_key' => '_lp_duration',
                    //'order' => 'DESC',
                    'post_type' => 'lp_course',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                    'suppress_filters' => true
                );
                
                $the_query = new WP_Query($args);
                
                //var_dump($the_query);

                if ($the_query->have_posts()) {
                    while ($the_query->have_posts()) {
                        $the_query->the_post();

                        $course = get_post_meta( $post->ID );
                        //var_dump($course);


                        $dateFormat = get_post_meta( $post->ID, '_lpr_custom_init_date', true );
                        $today = date("Y-m-d");


                        $date1 = new DateTime($dateFormat);
                        $date2 = new DateTime($today);
                        
                        $varMostra = false;
                        
                        if($date1>$date2){
                            $varMostra = true;
                        }
                        

                        $courseCat = get_the_terms($post->ID, 'course_category');
                        $formato = get_the_terms($post->ID, 'format');
                        if(format_date_course($post->ID, 'Y-m-d') < date('Y-m-d') || !$varMostra ){continue;}
                        ?>

                <div class="itemCurso">
                    <div class="cardCurso">
                        <div class="contentCard">
                            <div class="imgCard" style="background-image:url('<?php the_post_thumbnail_url(); ?>');" >
                                <img src="<?php the_post_thumbnail_url(); ?>"
                                     alt="<?php the_title(); ?>">
                            </div>

                            <div class="caixaCard">
                             
         
                                <div class="tags">
                                    <span><strong><?php echo $courseCat[0]->name; ?></strong></span>
                                    <span><?php echo $formato[0]->name; ?></span>
                                </div>


                                <div class="tituloCard sameHeight">
                                    <h4><?php the_title(); ?></h4>
                                </div>

                                <div class="com">
                                    <p><?php echo hl_print_instructors( $post->ID, 'list' ); ?></p>
                                </div>
                                
                                <div class="moreInfo">
                                    <div class="agenda">
                                        <img src="<?php echo $temaUrl; ?>assets-lp/images/icon-calendar.png"
                                             alt="Calendário">
                                        
                                        <p>início<br/><strong>
                                                <?php echo format_date_course($post->ID, 'd/m/Y'); ?>
                                            </strong></p>
                                    </div>
                                    <div class="btnInfo">
                                        <a href="<?php the_permalink(); ?>" class="btnPrimary">mais informações</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php 
                    }
                }
                
                ?>
                

            </div>
        </div>
    </section>

</main>

<footer>
    <div class="container">
        <div class="gridFlex">
            <div class="logo">
                <a href=""><img src="<?php echo $temaUrl; ?>assets-lp/images/logo-salajau.png" alt="Sala Jau"></a>
            </div>

            <nav class="menu">
                <ul>
                    <li><a href="">Inscreva-se</a></li>
                    <li><a href="#sobre-o-curso" class="samePage">Curso</a></li>
                    <li><a href="#aula-especial" class="samePage">Aula Aberta</a></li>
                    <li><a href="#proximosCursos" class="samePage">Outros Cursos</a></li>
                </ul>
            </nav>

            <div class="social">
                <nav>
                    <ul>
                        <li>
                            <a href="https://www.instagram.com/sala_jau/" target="_blank">
                                <svg id="icon-instagram" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" width="40" height="40"
                                     viewBox="0 0 40 40">
                                    <defs>
                                        <clipPath id="clip-path">
                                            <rect id="Rectangle_17" data-name="Rectangle 17" width="40" height="40"
                                                  fill="#fff"/>
                                        </clipPath>
                                    </defs>
                                    <g id="Group_4" data-name="Group 4" clip-path="url(#clip-path)">
                                        <path id="Path_3" data-name="Path 3"
                                              d="M52.8,35.554a4.175,4.175,0,0,0-2.39-2.39,6.969,6.969,0,0,0-2.338-.433c-1.328-.06-1.727-.073-5.089-.073s-3.761.013-5.088.073a6.965,6.965,0,0,0-2.338.433,4.173,4.173,0,0,0-2.39,2.39,6.971,6.971,0,0,0-.433,2.338c-.061,1.328-.073,1.726-.073,5.089s.013,3.761.073,5.088a6.971,6.971,0,0,0,.433,2.338,4.171,4.171,0,0,0,2.39,2.39,6.965,6.965,0,0,0,2.338.433c1.328.061,1.726.073,5.088.073s3.761-.013,5.089-.073a6.968,6.968,0,0,0,2.338-.433,4.173,4.173,0,0,0,2.39-2.39,6.971,6.971,0,0,0,.433-2.338c.061-1.328.073-1.726.073-5.088s-.013-3.761-.073-5.089a6.971,6.971,0,0,0-.433-2.338M42.981,49.447a6.467,6.467,0,1,1,6.467-6.466,6.466,6.466,0,0,1-6.467,6.466M49.7,37.77a1.511,1.511,0,1,1,1.511-1.511A1.511,1.511,0,0,1,49.7,37.77"
                                              transform="translate(-22.982 -22.981)" fill="#fff"/>
                                        <path id="Path_4" data-name="Path 4"
                                              d="M20,0A20,20,0,1,0,40,20,20,20,0,0,0,20,0M32.517,25.192a9.243,9.243,0,0,1-.585,3.057,6.441,6.441,0,0,1-3.683,3.683,9.245,9.245,0,0,1-3.057.585c-1.343.061-1.772.076-5.192.076s-3.849-.015-5.192-.076a9.242,9.242,0,0,1-3.057-.585,6.439,6.439,0,0,1-3.683-3.683,9.242,9.242,0,0,1-.585-3.057C7.422,23.849,7.407,23.42,7.407,20s.015-3.849.076-5.192a9.246,9.246,0,0,1,.585-3.057,6.439,6.439,0,0,1,3.683-3.683,9.243,9.243,0,0,1,3.057-.585c1.343-.061,1.772-.076,5.192-.076s3.849.015,5.192.076a9.246,9.246,0,0,1,3.057.585,6.441,6.441,0,0,1,3.683,3.683,9.246,9.246,0,0,1,.585,3.057c.061,1.343.076,1.772.076,5.192s-.015,3.849-.076,5.192"
                                              transform="translate(0 0)" fill="#fff"/>
                                        <path id="Path_5" data-name="Path 5"
                                              d="M57.531,53.333a4.2,4.2,0,1,0,4.2,4.2,4.2,4.2,0,0,0-4.2-4.2"
                                              transform="translate(-37.531 -37.531)" fill="#fff"/>
                                    </g>
                                </svg>

                            </a>
                        </li>

                        <li>
                            <a href="https://www.facebook.com/salajau.sp" target="_blank">
                                <svg id="icon-fb" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" width="40" height="40"
                                     viewBox="0 0 40 40">
                                    <defs>
                                        <clipPath id="clip-path">
                                            <rect id="Rectangle_18" data-name="Rectangle 18" width="40" height="40"
                                                  fill="#fff"/>
                                        </clipPath>
                                    </defs>
                                    <g id="Group_6" data-name="Group 6" clip-path="url(#clip-path)">
                                        <path id="Path_6" data-name="Path 6"
                                              d="M40,20.122A20,20,0,1,0,16.875,40V25.939H11.8V20.122h5.078V15.689c0-5.043,2.986-7.829,7.554-7.829a30.562,30.562,0,0,1,4.477.393v4.952H26.384c-2.484,0-3.259,1.551-3.259,3.142v3.775h5.547l-.887,5.817h-4.66V40A20.09,20.09,0,0,0,40,20.122"
                                              fill="#fff"/>
                                    </g>
                                </svg>
                            </a>
                        </li>

                        <li><a href="https://twitter.com/sala_jau/" target="_blank">
                                <svg id="icon-twitter" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" width="40" height="40"
                                     viewBox="0 0 40 40">
                                    <defs>
                                        <clipPath id="clip-path">
                                            <rect id="Rectangle_21" data-name="Rectangle 21" width="40" height="40"
                                                  fill="#fff"/>
                                        </clipPath>
                                    </defs>
                                    <g id="Group_12" data-name="Group 12" clip-path="url(#clip-path)">
                                        <path id="Path_9" data-name="Path 9"
                                              d="M20,0A20,20,0,1,0,40,20,20,20,0,0,0,20,0M31.054,15.722c.011.228.015.457.015.687A15.1,15.1,0,0,1,7.818,29.133a10.772,10.772,0,0,0,1.267.074,10.655,10.655,0,0,0,6.6-2.273,5.317,5.317,0,0,1-4.961-3.689,5.307,5.307,0,0,0,2.4-.091,5.313,5.313,0,0,1-4.261-5.207c0-.023,0-.046,0-.068a5.276,5.276,0,0,0,2.405.664,5.315,5.315,0,0,1-1.644-7.09A15.077,15.077,0,0,0,20.566,17a5.314,5.314,0,0,1,9.05-4.843,10.635,10.635,0,0,0,3.372-1.289,5.327,5.327,0,0,1-2.335,2.938,10.6,10.6,0,0,0,3.05-.836,10.8,10.8,0,0,1-2.649,2.75"
                                              fill="#fff" fill-rule="evenodd"/>
                                    </g>
                                </svg>
                            </a>
                        </li>

                        <li>
                            <a href="https://www.youtube.com/c/SalaJa%C3%BA" target="_blank">
                                <svg id="icon-youtube" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" width="40" height="40"
                                     viewBox="0 0 40 40">
                                    <defs>
                                        <clipPath id="clip-path">
                                            <rect id="Rectangle_16" data-name="Rectangle 16" width="40" height="40"
                                                  fill="#fff"/>
                                        </clipPath>
                                    </defs>
                                    <path id="Path_1" data-name="Path 1" d="M58.5,62l6.928-4L58.5,54Z"
                                          transform="translate(-41.167 -38)" fill="#fff"/>
                                    <g id="Group_2" data-name="Group 2">
                                        <g id="Group_1" data-name="Group 1" clip-path="url(#clip-path)">
                                            <path id="Path_2" data-name="Path 2"
                                                  d="M20,0A20,20,0,1,0,40,20,20,20,0,0,0,20,0M32.776,26.419a3.34,3.34,0,0,1-2.358,2.358c-2.079.557-10.418.557-10.418.557s-8.339,0-10.419-.557a3.34,3.34,0,0,1-2.358-2.358A34.709,34.709,0,0,1,6.667,20a34.715,34.715,0,0,1,.557-6.419,3.339,3.339,0,0,1,2.358-2.358C11.661,10.668,20,10.668,20,10.668s8.339,0,10.418.557a3.339,3.339,0,0,1,2.358,2.358A34.7,34.7,0,0,1,33.333,20a34.692,34.692,0,0,1-.557,6.418"
                                                  transform="translate(0 -0.001)" fill="#fff"/>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>

        <div class="separador"></div>

        <div class="copyright">
            <div class="gridFlex">
                <p>Copyright © <?php echo date('Y'); ?> Sala Jaú. Todos os direitos reservados.</p>

                <div class="btnTopo">
                    <a href="#" class="btnSecundario">topo <i>
                            <svg xmlns="http://www.w3.org/2000/svg" width="17.414" height="10.121"
                                 viewBox="0 0 17.414 10.121">
                                <path id="Path_2208" data-name="Path 2208" d="M1543.75,7270.031l8-8,8,8"
                                      transform="translate(1560.457 7270.738) rotate(180)" fill="none" stroke="#fff"
                                      stroke-width="2"/>
                            </svg>
                        </i>
                    </a>
                </div>
            </div>
        </div>


    </div>
</footer>


<section id="FaixaFixa">
    <div class="container">
        <div class="gridFlex">
            <div class="inscreva-se">
                <h4><?php echo $faixaFooter['chamada_faixa']; ?></h4>
                <p><?php echo $faixaFooter['complemento_faixa']; ?></p>
            </div>

            <div class="btns">
                <div class="inscreva-seBtn">
                    <a href="<?php echo $faixaFooter['link_do_botao']; ?>" class="btn"><?php echo $faixaFooter['texto_do_botao']; ?></a>
                </div>

                <div class="closeFaixa">
                    <img src="<?php echo $temaUrl; ?>assets-lp/images/close.jpg" alt="Fechar">
                </div>
            </div>

        </div>
    </div>
</section>


<!--scripts-->
<script src="<?php echo $temaUrl; ?>assets-lp/scripts/jquery-1.12.4.min.js"></script>


<script type="text/javascript" src="<?php echo $temaUrl; ?>assets-lp/scripts/slick.min.js"></script>

<!--Plugins Requiridos Para Esta Página de Video  -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mb.YTPlayer/3.3.9/jquery.mb.YTPlayer.min.js"></script>

<!--animations -->
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="https://www.google.com/recaptcha/api.js?render=6Lfjja0gAAAAAFRhPxns7JTYj0YVeOcdVqNk40F_"></script>

<script>

    if ($('#recaptchaResponse').length > 0) {
        grecaptcha.ready(function () {
            grecaptcha.execute('6Lfjja0gAAAAAFRhPxns7JTYj0YVeOcdVqNk40F_', {action: 'salaJauAulaAberta'}).then(function (token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });
    }

    //Flag Fechar Faixa Rodape
    flagClose = false;

    $(document).ready(function () {

        //Animations Start
        AOS.init();

        //Video
        jQuery(function () {
            jQuery("#MyPlayer").YTPlayer();
        });

        //Carrossel de Cursos
        $('.todoCarroselCursos').slick({
            variableWidth: false,
            arrows: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: false,
            touchThreshold: 10000000,
            infinite: false,
            responsive: [
                {
                    breakpoint: 901,
                    settings: {
                        variableWidth: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: true
                    }
                }
            ]
        });


        //Pega a altura do maior elemento para que os demais sigam
        maior = 0;
        $(".sameHeight").each(function () {
            heightAtual = $(this).innerHeight();
            if (heightAtual > maior) {
                maior = heightAtual;
            }
        });
        $(".sameHeight").css("min-height", maior + "px");

        //FixedMenu e Faixa
        VerificaMenuFixo(150);

        //Funcões Digitação no Input
        $(".inputPadrao").each(function () {
            if ($(this).val() != "") {
                $(this).parent().addClass("itemFormAtivo");
            } else {
                $(this).parent().removeClass("itemFormAtivo");
            }
        });

        $(".inputPadrao").on("focus", function () {
            $(this).parent().addClass("itemFormAtivo");
        });

        $(".inputPadrao").on("blur", function () {
            if ($(this).val() != "") {
                $(this).parent().addClass("itemFormAtivo");
            } else {
                $(this).parent().removeClass("itemFormAtivo");
            }
        });

        $('#frmInscricao').submit(function (e) {
            e.preventDefault();

            var name = $('#txtNome').val();
            var email = $('#txtEmail').val();
            var whatsapp = $('#txtWhatsapp').val();
            var recaptcha = $('#recaptchaResponse').val();

            $('#btnInscrever').css('display', 'none');
            $('#loader').css('display', 'block');

            $.ajax({
                method: "post",
                data: {
                    'name': name,
                    'email': email,
                    'whatsapp': whatsapp,
                    'aceite': 1,
                    'recaptcha': recaptcha
                },
                dataType: "text",
                url: '<?php echo $temaUrl; ?>envia-aula-aberta.php',
                error: function (retorno) {
                    $('#frmInscricao').css('display', 'none');
                    $('.mensagemForm').css('display', 'block');
                    $('.mensagemForm').addClass('mensagemErro');
                    $('.mensagemForm').html(retorno);
                    console.log(retorno);
                },
                success: function (retorno) {
                    $('#frmInscricao').css('display', 'none');
                    $('.mensagemForm').css('display', 'block');
                    $('.mensagemForm').addClass('mensagemSuccess');
                    $('.mensagemForm').html(retorno);
                    console.log(retorno);
                }

            });
        });


    });

    //Accordeon
    $(document).on("click", ".titleAcordeon", function (e) {
        e.preventDefault();
        if (!$(this).parent().hasClass("acordAtivo")) {
            $(".contentAcordeon").slideUp();
            $(".itemAcordeon").removeClass("acordAtivo");

            $(this).parent().children(".contentAcordeon").slideDown();
            $(this).parent().addClass("acordAtivo");
        } else {

        }
    })

    //Resize
    $(window).resize(function (e) {
        //Pega a altura do maior elemento para que os demais sigam
        maior = 0;
        $(".sameHeight").each(function () {
            heightAtual = $(this).innerHeight();
            if (heightAtual > maior) {
                maior = heightAtual;
            }
        });
        $(".sameHeight").css("min-height", maior + "px");
    });

    //Click samePage
    $(document).on("click", ".samePage", function (e) {
        e.preventDefault();
        $(".menuMobile").removeClass("mobileMenuAtivo");
        $("header .menu").removeClass("menuAberto");
        samePageLink = $(this).attr("href");
        pos = $(samePageLink).offset().top - 200;
        $("html, body").animate({scrollTop: pos}, 500);
    });


    //ScrollBTN
    $(document).on("click", ".btnTopoScroll", function (e) {
        e.preventDefault();
        pos = $("#sobre-o-curso").offset().top - 200;
        $("html, body").animate({scrollTop: pos}, 500);
    });

    //BtnTopo
    $(document).on("click", ".btnTopo", function (e) {
        e.preventDefault();
        pos = 0;
        $("html, body").animate({scrollTop: pos}, 500);
    });

    //MenuMobile
    $(document).on("click", ".menuMobile", function (e) {
        e.preventDefault();
        $(this).toggleClass("mobileMenuAtivo");
        $("header .menu").toggleClass("menuAberto");
    });

    //Mobile on Resize
    $(window).resize(function () {
        if ($(window).innerWidth() > 899) {
            $(".menuMobile").removeClass("mobileMenuAtivo");
            $("header .menu").removeClass("menuAberto");
        }
    });


    //Close Faixa
    $(document).on("click", ".closeFaixa", function (e) {
        e.preventDefault();
        flagClose = true;
        $("#FaixaFixa").removeClass("faixaAtivo");
    })

    //OnScroll
    $(window).scroll(function () {
        VerificaMenuFixo(150);
    });


    //Funcao FixedMenu e Faixa;
    function VerificaMenuFixo(alturaMin) {
        scrollPos = $(window).scrollTop();

        if (scrollPos > alturaMin) {
            $("header").addClass("headerAtivo");
        } else {
            $("header").removeClass("headerAtivo");
        }


        if (scrollPos > (alturaMin + 300)) {
            if (flagClose == false) {
                $("#FaixaFixa").addClass("faixaAtivo");
            }
        } else {
            $("#FaixaFixa").removeClass("faixaAtivo");
        }
    }

    function criaMascara(s, r) {
        var t = s.value, u = "", a = 0, b = 0, e = "", l = !0;
        for (i = 0; i < r.length; i++) "#" != r.substr(i, 1) && (t = t.replace(r.substr(i, 1), ""));
        for (i = 0; i < t.length; i++) isNaN(parseFloat(t.substr(i, 1))) || (u += t.substr(i, 1));
        for (i = 0; i < r.length; i++) "#" == r.substr(i, 1) ? "" != u.substr(a, 1) ? (e += u.substr(a, 1), a++, b++) : l = !1 : l && "" != u.substr(a, 1) && (e += r.substr(b, 1), b++);
        s.value = e
    }

</script>
</body>
</html>
<?php

/**
 * Buddyx functions and definitions
 *
 * This file must be parseable by PHP 5.2.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package buddyx
 */

define('BUDDYX_MINIMUM_WP_VERSION', '4.5');
define('BUDDYX_MINIMUM_PHP_VERSION', '7.0');

/**
 * custom  REST API Endpoint
 */
require 'inc/custom-api-resources.php';

/**
 * Checks if the BP Template pack is Nouveau when BuddyPress is active.
 *
 * @return boolean False if BuddyPress is not active or Nouveau is the active Template Pack.
 *                 True otherwise.
 */
function buddyx_template_pack_check()
{
    $retval = false;

    if (function_exists('buddypress')) {
        $retval = 'nouveau' !== bp_get_theme_compat_id();
    }

    return $retval;
}

function buddyx_buddypress_legacy_notice()
{
    if (buddyx_template_pack_check()) {
?>
        <div class="error">
            <p>
                <?php printf(esc_html__('BuddyX requires the BuddyPress Template Pack "BP Nouveau" to be active. Please activate this Template Pack from the %sBuddyPress Options.%s', 'buddyx'), '<a href="' . admin_url('admin.php?page=bp-settings') . '" >', '</a>'); ?>
            </p>
        </div>
    <?php
    }
}


// Bail if requirements are not met.
if (version_compare($GLOBALS['wp_version'], BUDDYX_MINIMUM_WP_VERSION, '<') || version_compare(phpversion(), BUDDYX_MINIMUM_PHP_VERSION, '<') || buddyx_template_pack_check()) {
    require get_template_directory() . '/inc/back-compat.php';
    add_action('admin_notices', 'buddyx_buddypress_legacy_notice');
    return;
}

// Include WordPress shims.
require get_template_directory() . '/inc/wordpress-shims.php';

// Include Kirki
require get_template_directory() . '/external/require_plugins.php';
require_once get_template_directory() . '/external/include-kirki.php';
require_once get_template_directory() . '/external/kirki-utils.php';

// Include LearnPress custom functions
require_once get_template_directory() . '/inc/learnpress-functions.php';

// Setup autoloader (via Composer or custom).
if (file_exists(get_template_directory() . '/vendor/autoload.php')) {
    require get_template_directory() . '/vendor/autoload.php';
} else {
    /**
     * Custom autoloader function for theme classes.
     *
     * @access private
     *
     * @param string $class_name Class name to load.
     * @return bool True if the class was loaded, false otherwise.
     */
    function _buddyx_autoload($class_name)
    {
        $namespace = 'BuddyX\Buddyx';

        if (strpos($class_name, $namespace . '\\') !== 0) {
            return false;
        }

        $parts = explode('\\', substr($class_name, strlen($namespace . '\\')));

        $path = get_template_directory() . '/inc';
        foreach ($parts as $part) {
            $path .= '/' . $part;
        }
        $path .= '.php';

        if (!file_exists($path)) {
            return false;
        }

        require_once $path;

        return true;
    }
    spl_autoload_register('_buddyx_autoload');
}

// Load the `buddyx()` entry point function.
require get_template_directory() . '/inc/functions.php';

// Initialize the theme.
call_user_func('BuddyX\Buddyx\buddyx');

// Require plugin.php to use is_plugin_active() below
if (!function_exists('is_plugin_active')) {
    include_once(ABSPATH . 'wp-admin/includes/plugin.php');
}

// Load theme breadcrubms function.
require get_template_directory() . '/inc/class-buddyx-breadcrubms.php';

// Load BuddyPress PRofile Completion widget.
require get_template_directory() . '/inc/widgets/bp-profile-completion-widget.php';

// Load theme extra function.
require get_template_directory() . '/inc/extra.php';

// bp_nouveau_appearance default option
$optionKey = 'buddyx_theme_is_activated';
if (!get_option($optionKey)) {

    $bp_nouveau_appearance = array(
        'members_layout'         => 3,
        'members_friends_layout' => 3,
        'groups_layout'          => 3,
        'members_group_layout'   => 3,
        'group_front_page'       => 0,
        'group_front_boxes'      => 0,
        'user_front_page'        => 0,
        'user_nav_display'       => 1,
        'group_nav_display'      => 1,
    );
    update_option('bp_nouveau_appearance', $bp_nouveau_appearance);
    update_option($optionKey, 1);
}

//
// Add WooCommerce Support
// ------------------------------------------------------------------------------
if (!function_exists('buddyx_woocommerce_support')) {

    function buddyx_woocommerce_support()
    {
        add_theme_support('woocommerce');
        add_theme_support('wc-product-gallery-zoom');
        add_theme_support('wc-product-gallery-lightbox');
        add_theme_support('wc-product-gallery-slider');
    }

    add_action('after_setup_theme', 'buddyx_woocommerce_support');
}

//
// force add theme support for BP nouveau
// ------------------------------------------------------------------------------
if (!function_exists('buddyx_buddypress_nouveau_support')) {

    function buddyx_buddypress_nouveau_support()
    {
        add_theme_support('buddypress-use-nouveau');
    }

    add_action('after_setup_theme', 'buddyx_buddypress_nouveau_support');
}

/**
 * Remove WooCommerce the breadcrumbs
 */
add_action('init', 'buddyx_remove_wc_breadcrumbs');
function buddyx_remove_wc_breadcrumbs()
{
    remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
}

/**
 * Remove WooCommerce CSS if WooCommerce not activated
 */
function buddyx_woo_dequeue_styles()
{
    wp_dequeue_style('buddyx-woocommerce');
    wp_deregister_style('buddyx-woocommerce');
}
if (!class_exists('WooCommerce')) {
    add_action('wp_print_styles', 'buddyx_woo_dequeue_styles');
}

function add_new_meta_fields($default_fields)
{

    $default_fields['_lpr_custom_init_date'] = new LP_Meta_Box_Text_Field(
        esc_html__('Data de início', 'learnpress'),
        '',
        '',
        [
            'wrapper_class' => esc_html__('_lpr_custom_init_date_wrap', 'learnpress'),
            'placeholder'   => '',
            'type_input'    => 'date'
        ]
    );

    $default_fields['_lpr_custom_finish_date'] = new LP_Meta_Box_Text_Field(
        esc_html__('Data de término', 'learnpress'),
        '',
        '',
        [
            'wrapper_class' => esc_html__('_lpr_custom_finish_date_wrap', 'learnpress'),
            'placeholder'   => '',
            'type_input'    => 'date'
        ]
    );
    $default_fields['_lpr_custom_avaliable_until'] = new LP_Meta_Box_Text_Field(
        esc_html__('Gravações até:', 'learnpress'),
        '',
        'default',
        [
            'wrapper_class' => esc_html__('_lpr_custom_finish_date_wrap', 'learnpress'),
            'placeholder'   => '',
            'type_input'    => 'date'
        ]
    );

    $default_fields['_lpr_custom_format_date'] = new LP_Meta_Box_Select_Field(
        esc_html__('Formato da data', 'learnpress'),
        '',
        'default',
        [
            'options' => array(
                'default'     => 'Padrão (Definido no WordPress)',
                'init'        => 'Início (início 31 de maio)',
                'init_finish' => 'Início e Término (01/12 a 14/12)'
            ),
        ]
    );

    $default_fields['_lpr_custom_days'] = new LP_Meta_Box_Text_Field(
        esc_html__('Dias', 'learnpress'),
        '',
        ''
    );

    $default_fields['_lpr_custom_number_of_classes'] = new LP_Meta_Box_Text_Field(
        esc_html__('Nº de aulas', 'learnpress'),
        '',
        ''
    );

    $default_fields['_lpr_custom_time'] = new LP_Meta_Box_Text_Field(
        esc_html__('Horário', 'learnpress'),
        '',
        ''
    );

    $default_fields['_lp_custom_course_status'] = new LP_Meta_Box_Radio_Field(
        esc_html__('Status do curso', 'learnpress'),
        '',
        'open',
        [
            'options' => array(
                'open'     => 'Novo',
                'on_going' => 'Em andamento',
                'finished' => 'Finalizado',
            ),
        ]
    );

    $default_fields['_lpr_custom_observation'] = new LP_Meta_Box_Text_Field(
        esc_html__('Pagamento', 'learnpress'),
        '',
        ''
    );
    $default_fields['_lpr_custom_information_course'] = new LP_Meta_Box_Text_Field(
        esc_html__('Gravações', 'learnpress'),
        '',
        ''
    );

    $default_fields['_lpr_custom_live_classes'] = new LP_Meta_Box_Text_Field(
        esc_html__('Aulas ao vivo', 'learnpress'),
        '',
        ''
    );

    return $default_fields;
}

add_filter('lp/course/meta-box/fields/general', 'add_new_meta_fields');

function before_course_content()
{
    // echo "<pre>";
    // var_dump(get_post_meta(get_the_ID(), '_lp_custom_course_status', true));
    // echo "</pre>";
    ob_start();
}

function after_course_content()
{
    $content = ob_get_contents();
    ob_end_clean();

    global $wp_query;
    $course_status = get_post_meta(get_the_ID(), '_lp_custom_course_status', true);

    //pega o primeiro termo (tipo de curso) do curso
    $terms = get_the_terms($wp_query->get_queried_object_id(), 'course_tag');
    if (!empty($terms)) {
        $term = array_shift($terms);
        $typeCourse = $term->slug;
    }

    if ($course_status === 'on_going') {
        echo $content;
    } elseif ($course_status === 'open') {

        //learn_press_display_message( 'O link de acesso ao Zoom, as gravações e outras informações relativas às aulas estarão disponíveis logo após o início do curso. ');

        //frase condicional exibida caso o tipo de produto no esteja iniciado
        if ($typeCourse === 'cursos') {
            learn_press_display_message('O link de acesso ao Zoom, as gravações e outras informações relativas às aulas estarão disponíveis logo após o início do curso.');
        } else {
            learn_press_display_message('O link de acesso ao Zoom, as gravações e outras informações relativas às aulas estarão disponíveis logo após o início da atividade.');
        }
    } elseif ($course_status === 'finished') {
        learn_press_display_message('curso encerrado');
    }
}

add_action('learn-press/before-course-item-content', 'before_course_content');
add_action('learn-press/after-course-item-content', 'after_course_content');

/**
 * Adds `current-menu-item` CSS class to the LearnPress course page
 */
function add_menu_class_learn_press_course_page($classes, $item)
{

    global $post;

    $learn_press_courses_page_id = get_option('learn_press_courses_page_id', false);

    if (isset($post->ID) && $post->ID == $learn_press_courses_page_id && $post->ID == $item->object_id) {
        $classes[] = 'current-menu-item';
    }

    return $classes;
}
add_action('nav_menu_css_class', 'add_menu_class_learn_press_course_page', 10, 2);

/**
 * 
 * Create list of the terms by taxonomy
 * 
 * @param int $post_id Post ID
 * @param string $tax Slug tax to get terms
 * @param bool $use_link Define if is use link to the terms
 * 
 * @link https://developer.wordpress.org/reference/functions/get_the_terms/
 * @link https://developer.wordpress.org/reference/functions/sanitize_title/
 * @link https://developer.wordpress.org/reference/functions/esc_url/
 * @link https://developer.wordpress.org/reference/functions/get_term_link/
 * 
 * @return string $html
 * 
 */
function get_html_terms(int $post_id, string $tax, bool $use_link = false)
{

    $terms = get_the_terms($post_id, sanitize_title($tax));

    if (!$terms) {
        return false;
    }

    $html = '<ul class="list-terms tax-' . sanitize_title($tax) . '">';

    foreach ($terms as $term) {

        $html .= '<li class="term-' . sanitize_title($term->slug) . '">';

        if ($use_link) {
            $html .= '<a href="' . esc_url(get_term_link($term->term_id, $tax)) . '">';
        }

        $html .= esc_attr($term->name);

        if ($use_link) {
            $html .= '</a>';
        }

        $html .= '</li>';
    }

    $html .= '</ul>';

    return $html;
}

/**
 * 
 * Remove prefix `Archive:` of the post type archive `lp_course`
 * 
 * @link https://developer.wordpress.org/reference/hooks/get_the_archive_title_prefix/
 * @link https://developer.wordpress.org/reference/functions/is_post_type_archive/
 * 
 */
function filter_course_archive_title_prefix($prefix)
{

    if (is_post_type_archive('lp_course'))
        return;

    return $prefix;
}

add_filter('get_the_archive_title_prefix', 'filter_course_archive_title_prefix', 1);

/**
 * 
 * Use `card-course` template part on the post type archive `lp_course`
 * 
 * @link https://developer.wordpress.org/reference/functions/is_post_type_archive/
 * @link https://developer.wordpress.org/reference/functions/locate_template/
 * 
 */
function filter_course_archive_template_part($template)
{

    if (is_post_type_archive('lp_course') || is_tax('course_category') || is_tax('course_tag'))
        $template = locate_template(['template-parts/cards/card-course.php']);

    return $template;
}

add_filter('learn_press_get_template_part', 'filter_course_archive_template_part');

remove_action('learn-press/content-landing-summary', 'learn_press_course_buttons', 30);

/**
 * Adds sub title on archive course
 */
function archive_course_add_sub_title($template_name, $template_path, $located, $args)
{

    if (is_post_type_archive('lp_course') && stripos($located, 'loop-begin.php')) {
        get_template_part('template-parts/content/archive-lp-course-sub-title');
    }
}
//add_action( 'learn_press_before_template_part', 'archive_course_add_sub_title', 10, 4 );

/**
 * Remove search field from archive course
 */
function archive_course_remove_search($located, $template_name, $args, $template_path, $default_path)
{

    if (is_post_type_archive('lp_course') && stripos($located, 'search-form.php'))
        return;

    return $located;
}
add_filter('learn_press_get_template', 'archive_course_remove_search', 10, 5);

/**
 * Adds css class `simplified-header` to pages that should use the simplified header
 */
function add_simplified_header($classes)
{
    if (
        is_post_type_archive('lp_course')
        || is_post_type_archive('blog')
        || is_category()
        || is_tax('course_category')
        || is_tax('categoria_do_blog')
        || is_tax('course_tag')
        || is_page_template('template-history.php')
        || is_page_template('page-simplified.php')
        || is_page_template('page-course-going.php')
        || is_page_template('page-course-open.php')
        || is_page('contato-v2') 
        || is_page('cultura-artistica')
        || is_singular('open_lesson')
        || is_singular('blog')
    ) {
        $classes = array_merge($classes, ['simplified-header']);
    }

    // If WooCommerce is active
    if ( function_exists( 'wc_get_page_id' ) ) {
        if ( is_page( wc_get_page_id( 'myaccount' ) ) || is_cart() || is_checkout() ) {
            $classes = array_merge($classes, ['simplified-header']);
        }
    }

    return $classes;
}
add_filter('body_class', 'add_simplified_header');

/**
 * Remove sub header from pages with simplified header template
 */
function remove_buddyx_sub_header()
{

    if (in_array('simplified-header', get_body_class())) {
        if (function_exists('buddyx_sub_header')) {
            remove_action('buddyx_sub_header', 'buddyx_sub_header');
        }
    }
}
add_action('wp_head', 'remove_buddyx_sub_header');

/**
 * Sort the main posts in the lp_course archive
 */
$GLOBALS['hacklab_archive_query_num'] = 0;
function order_archive_lp_course($query)
{
    if (is_admin()) {
        return;
    }
  
    if (($query->get('post_type') == 'lp_course') || ($query->is_main_query() && (is_tax('course_category') || is_tax('course_tag')))) {
        if  ($query->is_tax('course_tag','cursos-gravados')){
            $query->set('order', 'DESC');

            $query->set("orderby","meta_value");
            $query->set("meta_key",'_lpr_custom_avaliable_until');
            $meta_query = [
                [
                    'key'     => '_lp_custom_course_status',
                    'value'   => ['finished'],
                    'compare' => 'NOT IN',
                ],
                [
                    'key'       => '_lp_custom_course_status',
                    'meta_value'=> ['ongoing']
                ],
            ];
            $query->set('meta_query', $meta_query);
            return $query;
        }

        if (is_page_template('template-history.php')) {
            return $query;
        }

        if  (!$query->is_tax('course_tag','cursos-presenciais')){
            $query->set('orderby', 'meta_value');
            $query->set('meta_key', '_lpr_custom_init_date');
            $query->set('order', 'ASC');
        }

        if(is_page_template('page-course-going.php')){
            $query->set('meta_key', '_lpr_custom_avaliable_until');
            $query->set('order', 'DESC');
        }

        if(is_page_template('page-course-open.php')){
            $query->set('order', 'ASC');

            $query->set("orderby","meta_value");
            $query->set("meta_key",'_lpr_custom_init_date');
            $meta_query = [
                [
                    'key'     => '_lpr_custom_init_date',
                    'value' => array(''),
                    'compare' => 'NOT IN'
                ],
                [
                    'key'     => '_lp_custom_course_status',
                    'value'   => ['finished','ongoing'],
                    'compare' => 'NOT IN',
                ],
                [
                    'key'       => '_lp_custom_course_status',
                    'meta_value'=> ['open']
                ],
            ];
            $query->set('meta_query', $meta_query);
            return $query;
        }

        if ($query->get('post_type') == 'lp_course') {

            /**
             * Fix course_tag term (curso or cursos) to archive lp_course
             */
            // $tax_query = [
            //     [
            //         'taxonomy' => 'course_tag',
            //         'field'    => 'slug',
            //         'terms'    => ['curso', 'cursos'],
            //         'operator' => 'IN'
            //     ]
            // ];

            // $query->set( 'tax_query', $tax_query );
        }
    }

    // If LearnPress is active
    if ( function_exists( 'learn_press_get_page_id' ) ) {
        if ( get_queried_object_id() && get_queried_object_id() == learn_press_get_page_id( 'courses' ) && $query->get( 'post_type' ) == 'lp_course' ) {
            if ($GLOBALS['hacklab_archive_query_num'] == 0) {
                $meta_query = [
                    [
                        'key' => '_lp_custom_course_status',
                        'value' => 'open'
                    ]
                ];
                $GLOBALS['hacklab_archive_query_num']++;
                $query->set('meta_query', $meta_query);
                return $query;
            }
            if ($GLOBALS['hacklab_archive_query_num'] == 1) {
                $meta_query = [
                    [
                        'key' => '_lp_custom_course_status',
                        'value' => 'on_going'
                    ]
                ];
                $GLOBALS['hacklab_archive_query_num']++;
                $query->set('meta_query', $meta_query);
                return $query;
            }
        }
    }

    if (($query->is_main_query() && (is_tax('course_category') || is_tax('course_tag')))) {
        $meta_query = [
            [
                'key' => '_lp_custom_course_status',
                'value' => ['on_going', 'open']
            ]
        ];
        $query->set('meta_query', $meta_query);
        return $query;
    }

    if (is_category()) {
        $query_post_type = $query->get('post_type');
        $query_post_type = is_array($query_post_type) ? $query_post_type : [$query_post_type];
        $query_post_type[] = 'blog';
        $query->set('post_type', $query_post_type);
    }
    return $query;
}

add_action('pre_get_posts', 'order_archive_lp_course', 9999);

/**
 * Adds loop with on going courses on archive course
 */
function archive_course_add_on_going()
{
    if (is_post_type_archive('lp_course') || is_tax('course_tag')) {
        get_template_part('template-parts/content/archive-lp-course-on-going');
    }
}
//add_action( 'learn-press/after-main-content', 'archive_course_add_on_going' );

/**
 * Remove support to comments in the post type lp_courses
 */
function remove_comments_lp_course()
{

    // If in the admin, return.
    if (is_admin()) {
        return;
    }

    if ('lp_course' == get_post_type()) {
        remove_post_type_support('lp_course', 'comments');
    }

    return;
}
add_action('wp_head', 'remove_comments_lp_course');

function custom_body_classes()
{

    $body_classes = ['general'];

    if (strpos($_SERVER['REQUEST_URI'], 'a-sala-jau') !== false && is_page())
        array_push($body_classes, "about-us");

    if (strpos($_SERVER['REQUEST_URI'], 'contato') !== false && is_page())
        array_push($body_classes, "contact");

    if (strpos($_SERVER['REQUEST_URI'], 'cultura-artistica') !== false && is_page())
        array_push($body_classes, "cultura-artistica");    

    return $body_classes;
}

/**
 * Fix sale course individually
 */
function remove_quantity_fields($return, $product)
{
    return true;
}
add_filter('woocommerce_is_sold_individually', 'remove_quantity_fields', 10, 2);

/**
 * Adds slider on home
 */




function home_slider()
{   
    if (is_front_page()) {
        
        get_template_part('template-parts/home/home', 'slider');
    }
}
add_action('buddyx_before_content', 'home_slider');

/**
 * Adds custom css class to body
 */
function custom_body_class($classes)
{

    global $wp_query;

    // If LearnPress is active
    if ( function_exists( 'learn_press_is_course_taxonomy' ) ) {
        
        if ( learn_press_is_course_taxonomy() && isset( $wp_query->queried_object->slug ) ) {
            $add     = 'term-' . $wp_query->queried_object->slug;
            $classes = array_merge( $classes, [$add] );
        }
    }

    if (is_singular(['lp_course', 'open_lesson'])) {

        $get_course_category = get_the_terms($wp_query->get_queried_object_id(), 'course_category');
        $get_course_tag = get_the_terms($wp_query->get_queried_object_id(), 'course_tag');

        $add = [];

        if ($get_course_category && !is_wp_error($get_course_category)) {
            foreach ($get_course_category as $term) {
                $add[] = 'course-category-' . $term->slug;
            }
        }

        if ($get_course_tag && !is_wp_error($get_course_tag)) {
            foreach ($get_course_tag as $term) {
                $add[] = 'course-tag-' . $term->slug;
            }
        }

        $classes = array_merge($classes, $add);
    }

    return array_filter($classes);
}
add_filter('body_class', 'custom_body_class');

/**
 * Add message on frontend panel user
 */
function add_message_panel()
{

    $panel_id   = learn_press_get_page_id('profile');
    $panel      = get_permalink($panel_id);
    $user       = wp_get_current_user();
    $user_login = $user->user_login;

    $link_dashboard_tab = $panel . $user_login . '/dashboard';
    $link_courses_tab   = $panel . $user_login . '/courses/purchased';
    $link_settings_tab  = $panel . $user_login . '/settings/basic-information';
    $link_password_tab  = $panel . $user_login . '/settings/change-password';

    echo '<div class="">';
    echo '<p>';
    echo sprintf(wp_kses('A partir desse seu painel de controle, você pode ver seus <a href="%s">cursos recentes</a>, gerenciar <a href="%s">seus dados e detalhes da sua conta</a>, assim com <a href="%s">editar sua senha</a>.', 'buddyx'), $link_courses_tab, $link_settings_tab, $link_password_tab);
    echo '</p>';
    echo '</div>';
}
add_action('learn-press/profile/dashboard-summary', 'add_message_panel');

function open_container_form_login()
{
    if (($page_id = learn_press_get_page_id('profile')) && is_page($page_id) && !is_user_logged_in()) {
        echo '<div class="container-form-login">';
    }
}
add_action('learn-press/user-profile', 'open_container_form_login', 9);

function close_container_form_login()
{
    if (($page_id = learn_press_get_page_id('profile')) && is_page($page_id) && !is_user_logged_in()) {
        echo '</div>';
        echo '<span id="toggle-form-register">Ainda não tem uma conta? <a href="javascript: void(0);">Crie sua conta!</a></span>';
    }
}
add_action('learn-press/user-profile', 'close_container_form_login', 16);

/**
 * Include Tab Courses in WC My Account Page
 */
require_once get_template_directory() . '/inc/class-courses-woocommerce-my-account.php';

/**
 * Include Filter Users by Course Class
 */
require_once get_template_directory() . '/inc/class-filter-users-by-course.php';

/**
 * Include Post type open lesson manager
 */
require_once get_template_directory() . '/inc/class-post-type-open-lesson.php';

/**
 * Enable override template by theme
 * 
 * @since 4.0.0
 */
add_filter('learn-press/override-templates', function () {
    return true;
});

/**
 * Add additional informal on the sidebar course
 */
add_action('learn-press/course-summary-sidebar', function () {
    get_template_part('template-parts/sidebar-additional-information');
}, 0);


/**
 * Change add cart btn template for finished courses
 */
add_filter('learn-press/tmpl-button-purchase-course', function ($actions, $subscription) {
    global $post;

    $status = get_post_meta($post->ID)['_lp_custom_course_status'][0];
    $actions['template_name'] = $status == 'finished' ? 'learnpress/single-course/sidebar/finished-course-btn.php' : $actions['template_name'];

    return $actions;
}, 14, 2);

/**
 * Change Add to Cart text
 */
add_filter('gettext', function ($translated_text, $untranslated_text, $domain) {
    if ('Add to cart' == $untranslated_text) {
        global $post;

        $status = get_post_meta($post->ID)['_lp_custom_course_status'][0];

        return $status == 'finished' ? 'encerrado' : 'inscrever-se';
    }
    return $translated_text;
}, 10, 3);

add_filter('gettext', function ($translated_text, $untranslated_text, $domain) {
    if ("Notes about your order, e.g. special notes for delivery." == $untranslated_text)
        return "Se desejar, deixe uma mensagem para o atendimento (opcional)";

    return $translated_text;
}, 10, 3);

add_filter('gettext', function ($translated_text, $untranslated_text, $domain) {
    return ("Payment method:" == $untranslated_text) ? "Forma de pagamento:" : $translated_text;
}, 10, 3);

add_filter('gettext', function ($translated_text, $untranslated_text, $domain) {
    $description_after_payment = 'Após o pagamento, a confirmação de seu pedido ocorrerá <strong>em até 24 horas</strong>, em horário comercial. Qualquer dúvida, entre em contato conosco <strong>clicando aqui</strong>.';

    if ($description_after_payment  == $untranslated_text)
        return str_replace('clicando aqui', '<a href="' . site_url() . '/contato">clicando aqui</a>',  $untranslated_text);

    return $translated_text;
}, 10, 3);

/**
 * Redireciona após o cadastro/login em cursos gratuitos
 */
add_action('template_redirect', function () {
    if (is_admin()) {
        return;
    }
    global $wp;
    // verifica se é a página de finalização do learnpress
    if (isset($wp->query_vars['pagename']) && 'lp-checkout-2' == $wp->query_vars['pagename'] && isset($wp->query_vars['lp-order-received'])) {
        $order = learn_press_get_order($wp->query_vars['lp-order-received']);
        if (!$order || !is_object($order)) {
            return;
        }
        $total = (int) $order->get_total();
        if (0 != $total) {
            return;
        }
        $items = $order->get_items();
        if (1 != count($items)) {
            return;
        }
        foreach ($items as $item) {
            $course_url = get_permalink($item['course_id']);
            wp_redirect($course_url);
            break;
        }
    }
}, 99);


/**
 * Change `course_tag` to hierarchical
 * 
 * @see https://developer.wordpress.org/reference/hooks/init/
 * @see https://developer.wordpress.org/reference/functions/get_taxonomy/
 * @see https://developer.wordpress.org/reference/functions/register_taxonomy/
 */
function change_course_tag_object()
{

    $args = get_taxonomy('course_tag');

    if ($args) {

        // Values to change
        $args->hierarchical = true;

        register_taxonomy('course_tag', [LP_COURSE_CPT], $args);
    }
}

add_action('init', 'change_course_tag_object', 999, 3);

/**
 * Add custom taxonomies to the tabs on the course admin page
 * 
 * @see https://www.php.net/manual/pt_BR/function.array-key-last.php
 * @see https://developer.wordpress.org/reference/functions/get_object_taxonomies/
 * @see https://www.php.net/manual/pt_BR/function.in-array.php
 * @see https://www.php.net/manual/pt_BR/function.array-column.php
 */
function add_taxonomies_tabs($tabs)
{

    $priority = array_key_last($tabs);

    $taxonomies = get_object_taxonomies('lp_course', 'objects');

    if ($taxonomies) {
        foreach ($taxonomies as $tax) {
            if (!in_array($tax->label, array_column($tabs, 'name'))) {
                $tabs[$priority] = [
                    "link" => 'edit-tags.php?taxonomy=' . $tax->name . '&post_type=lp_course',
                    "name" => $tax->label,
                    "id"   => 'edit-' . $tax->name
                ];

                $priority += 10;
            }
        }
    }

    return $tabs;
}

add_filter('learn_press_admin_tabs_info', 'add_taxonomies_tabs');

/**
 * Add pages to print tabs
 * 
 * @see https://developer.wordpress.org/reference/functions/get_object_taxonomies/
 */
function add_screens_to_tabs($screens)
{

    $taxonomies = get_object_taxonomies('lp_course');

    foreach ($taxonomies as $tax) {
        $screens[] = 'edit-' . $tax;
    }

    return $screens;
}

add_filter('learn_press_admin_tabs_on_pages', 'add_screens_to_tabs');

/**
 * Format and print of the course
 * 
 * @param $course_id - ID of the course
 * @param $format - Format fo the print (default, init or init_finish)
 * @oaram $icon - If show calendar icon
 * 
 * @see https://developer.wordpress.org/reference/functions/date_i18n/
 * 
 * @todo i18n add tranlations (a, início:)
 * 
 * @return string html formated
 */
function format_date_course($course_id, $format = 'default', $icon = false)
{

    $start_date = get_post_meta($course_id, '_lpr_custom_init_date', true) ?: false;

    if ($start_date) {

        $html = '';

        if ($format == 'init') {
            $html .= '<span class="with-init">início</span>';
            $html .= date_i18n('d \d\e F', strtotime($start_date));
        } elseif ($format == 'init_finish') {
            $finish_date = get_post_meta($course_id, '_lpr_custom_finish_date', true) ?: false;
            if ($finish_date) {
                $html = date_i18n('d/m', strtotime($start_date)) . ' e ' . date_i18n('d/m', strtotime($finish_date));
            } else {
                $html = date_i18n('d/m', strtotime($start_date));
            }
        } else {
            //default and others formats
            $html = date_i18n(get_option('date_format'), strtotime($start_date));
        }

        if ($icon) {
            $icon = '<svg class="fa-calendar-day" width="16" height="18" viewBox="0 0 16 18" xmlns="http://www.w3.org/2000/svg"><path d="M0 16.3125C0 17.2441 0.755859 18 1.6875 18H14.0625C14.9941 18 15.75 17.2441 15.75 16.3125V6.75H0V16.3125ZM2.25 9.5625C2.25 9.25313 2.50312 9 2.8125 9H6.1875C6.49687 9 6.75 9.25313 6.75 9.5625V12.9375C6.75 13.2469 6.49687 13.5 6.1875 13.5H2.8125C2.50312 13.5 2.25 13.2469 2.25 12.9375V9.5625ZM14.0625 2.25H12.375V0.5625C12.375 0.253125 12.1219 0 11.8125 0H10.6875C10.3781 0 10.125 0.253125 10.125 0.5625V2.25H5.625V0.5625C5.625 0.253125 5.37187 0 5.0625 0H3.9375C3.62812 0 3.375 0.253125 3.375 0.5625V2.25H1.6875C0.755859 2.25 0 3.00586 0 3.9375V5.625H15.75V3.9375C15.75 3.00586 14.9941 2.25 14.0625 2.25Z" /></svg>';
            return '<span class="formated-date-course format-' . $format . '">' . $icon . $html . '</span>';
        } else {
            return '<span class="formated-date-course format-' . $format . '">' . $html . '</span>';
        }
    }

    // Nothing is returned if the $starting_point is empty

}
function format_date_course_until($course_id, $format = 'default', $icon = false)
{

    $start_date = get_post_meta($course_id, '_lpr_custom_avaliable_until', true) ?: false;
    

        if ($start_date) {
            $html = '';
            $html .= '<span class="with-init">Gravações até: </span>';
            $html .= date_i18n('d/m/y', strtotime($start_date));

            if ($icon) {
                $icon = '<svg class="fa-calendar-day" width="16" height="18" viewBox="0 0 16 18" xmlns="http://www.w3.org/2000/svg"><path d="M0 16.3125C0 17.2441 0.755859 18 1.6875 18H14.0625C14.9941 18 15.75 17.2441 15.75 16.3125V6.75H0V16.3125ZM2.25 9.5625C2.25 9.25313 2.50312 9 2.8125 9H6.1875C6.49687 9 6.75 9.25313 6.75 9.5625V12.9375C6.75 13.2469 6.49687 13.5 6.1875 13.5H2.8125C2.50312 13.5 2.25 13.2469 2.25 12.9375V9.5625ZM14.0625 2.25H12.375V0.5625C12.375 0.253125 12.1219 0 11.8125 0H10.6875C10.3781 0 10.125 0.253125 10.125 0.5625V2.25H5.625V0.5625C5.625 0.253125 5.37187 0 5.0625 0H3.9375C3.62812 0 3.375 0.253125 3.375 0.5625V2.25H1.6875C0.755859 2.25 0 3.00586 0 3.9375V5.625H15.75V3.9375C15.75 3.00586 14.9941 2.25 14.0625 2.25Z" /></svg>';
                return '<span class="formated-date-course format-' . $format . '">' . $icon . $html . '</span>';
            } else {
                return '<span class="formated-date-course format-' . $format . '">' . $html . '</span>';
            }
        }
    
    // Nothing is returned if the $starting_point is empty
}

if (function_exists('LP')) {

    /**
     * Remove courses top bar pf the LearnPress
     */
    function remove_courses_top_bar()
    {
        remove_action('learn-press/before-courses-loop', LP()->template('course')->func('courses_top_bar'), 10);
    }

    add_action('wp_head', 'remove_courses_top_bar');
}

/**
 * Return the author name of the course in response api
 */
function hl_get_author_name($post)
{

    $instructors = hl_get_instructors($post["id"]);
    $count_instructors = count($instructors);
    $iterator          = 0;

    $return_instructors = '';

    foreach ($instructors as $value) {
        $iterator++;

        $return_instructors .= get_the_author_meta('display_name', $value);

        if ($count_instructors > 2 && $iterator < $count_instructors) {
            if (($iterator + 1) != $count_instructors) {
                $return_instructors .= __(', ', 'buddyx');
            }
        }

        if ($count_instructors >= 2 && ($iterator + 1) == $count_instructors) {
            $return_instructors .= __(' and ', 'buddyx');
        }
    }

    return $return_instructors;
}

/**
 * Return formated data of the course in response api
 */
function hl_get_formated_date_course($object, $field_name, $request)
{
    $format = get_post_meta($object['id'], '_lpr_custom_format_date', true) ?: 'default';
    return apply_filters('the_title', format_date_course($object['id'], $format, true));
}
function hl_get_formated_date_until($object, $field_name, $request)
{
    $format = get_post_meta($object['id'], '_lpr_custom_format_date', true) ?: 'default';
    /*    return apply_filters( 'the_title', format_date_course( $object['id'], $format, true ) ); */
    return apply_filters('the_title', format_date_course_until($object['id'], $field_name, true));
}

/**
 * Wrap to function get_post_meta, to use in response api
 */
function hl_get_post_meta_api($object, $field_name)
{
    return get_post_meta($object['id'], $field_name, true);
}

/**
 * Function to expose custom fields/values on response api
 * 
 * @see https://developer.wordpress.org/reference/hooks/rest_api_init/
 */
add_action('rest_api_init', 'hl_add_custom_fields_to_api');


function hl_add_custom_fields_to_api()
{
    register_rest_field(
        'lp_course',
        'author_name',
        [
            'get_callback' => 'hl_get_author_name'
        ]
    );

    register_rest_field(
        'lp_course',
        'formated_date_course',
        [
            'get_callback' => 'hl_get_formated_date_course'
        ]
    );

    register_rest_field(
        'lp_course',
        '_lp_custom_course_status',
        [
            'get_callback' => 'hl_get_post_meta_api'
        ]
    );

    register_rest_field(
        'lp_course',
        '_lpr_custom_avaliable_until',
        [
            'get_callback' => 'hl_get_formated_date_until'
        ]
    );
}

/**
 * Filter posts by meta value in api query
 */
add_filter('rest_lp_course_query', 'lp_course_request_params', 99, 2);

function lp_course_request_params($args, $request)
{


    $query_params = $request->get_query_params();
    if (isset($query_params['history_by_year']) && !empty($query_params['history_by_year'])) {

        $year      = date('Y', strtotime(sanitize_text_field($query_params['history_by_year'])));
        $init_year = date('Y-m-d', strtotime('first day of January ' .  $year));
        $end_year  = date('Y-m-d', strtotime('last day of December ' .  $year));

        $args['meta_query'] = array(
            [
                'key'     => '_lpr_custom_init_date',
                'value'   => [$init_year, $end_year],
                'compare' => 'BETWEEN',
                'type'    => 'DATE'
            ],
            [
                'key'       => '_lp_custom_course_status',
                'value'     => 'finished',
                'compare'   => '=',
            ]
        );
    } elseif (isset($query_params['meta_key']) && isset($query_params['meta_value'])) {

        $meta_value = explode(',', $query_params['meta_value']);
        $meta_value = array_map('sanitize_text_field', $meta_value);

        $args['meta_query'] = array(
            [
                'key'   => sanitize_text_field($query_params['meta_key']),
                'value' => $meta_value
            ]
        );
    }

    return $args;
}

/**
 * Add tab on thje course to add additional information
 * Use meta value of the key 'additional_information'
 */
add_filter('learn-press/course-tabs', 'hl_add_tab_course');

function hl_add_tab_course($defaults)
{

    $content = get_post_meta(get_the_ID(), 'additional_information', true);

    if (!empty($content)) {
        $defaults['additional_information'] = [
            'title'    => __('Additional information', 'buddyx'),
            'priority' => 100,
            'callback' => function () use ($content) {
                echo apply_filters('the_content', $content);
            }
        ];
    }

    return $defaults;
}

/**
 *
 * Add Active class for edit-account when access edit-address
 * 
 */
function edit_address_woocommerce_account_menu_item_classes($classes, $endpoint)
{
    global $wp;
    if ($endpoint == 'edit-account' && isset($wp->query_vars['edit-address'])) {
        $classes[] = 'is-active';
    }
    return $classes;
}
add_filter('woocommerce_account_menu_item_classes', 'edit_address_woocommerce_account_menu_item_classes', 10, 2);

function reorder_my_account_tabs($menu_links)
{


    $menu_links = array(
        'dashboard'          => __('Dashboard', 'woocommerce'),
        'edit-account'       => 'Dados pessoais',
        'orders'             => 'Pedidos',
        'customer-logout'    => __('Logout', 'woocommerce'),
    );

    return $menu_links;
}
add_filter('woocommerce_account_menu_items', 'reorder_my_account_tabs', 10, 1);


/*
 *
 * Change LP Profile page url
 * Fix issue #124
 * 
 */
function sala_jau_lp_profile_redirect_to_wc_my_account()
{
    if ( is_admin() ) {
        return;
    }

    if ( class_exists( 'LP' ) ) {
        $page_id = LP()->settings->get( 'profile_page_id' );
        $queried = get_queried_object();
        if ( $queried && is_object( $queried ) && 'WP_Post' == get_class( $queried ) ) {
            if ( $queried->ID == absint( $page_id ) ) {
                wp_redirect( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) );
                exit;
            }
        }
    }

}
add_action('wp', 'sala_jau_lp_profile_redirect_to_wc_my_account', 20);

/**
 * Add button `Keep shopping` on checkout page
 */
add_action('woocommerce_checkout_order_review', function () {
    echo '<div class="wrap-button">';
    echo '<a href="' . home_url('tipo/cursos-online') . '" class="button">' .  __('Continuar comprando', 'buddyx') . '</a>';
    echo '</div>';
}, 10);

/**
 * Add title of the payment methods on checkout page
 */
add_action('woocommerce_checkout_order_review', function () {
    echo '<h3 class="title-payment">' . __('Formas de pagamento', 'buddyx') . '</h3>';
}, 10);

/**
 * Change general address fields
 */
add_filter('woocommerce_default_address_fields', function ($fields) {

    $fields['address_2']['label_class'] = '';
    $fields['address_2']['label']       = __('Complemento', 'buddyx');
    $fields['postcode']['class'][0]     = 'form-row-last';
    $fields['address_2']['class'][0]    = 'form-row-last';
    $fields['city']['class'][0]         = 'form-row-first';
    $fields['state']['class'][0]        = 'form-row-last';

    return $fields;
});

/**
 * Change address fields on checkout
 */
add_filter('woocommerce_checkout_fields', function ($fields) {

    $fields['billing']['billing_email']['class'][0]        = 'form-row-wide';
    $fields['billing']['billing_phone']['class'][0]        = 'form-row-wide';
    $fields['billing']['billing_country']['class'][0]      = 'form-row-first';
    $fields['billing']['billing_postcode']['class'][0]     = 'form-row-first';
    $fields['billing']['billing_neighborhood']['class'][0] = 'form-row-wide';
    $fields['billing']['billing_phone']['class'][0]        = 'form-row-first';
    $fields['billing']['billing_cellphone']['class'][0]    = 'form-row-last';
    $fields['billing']['billing_email']['label']           = __('E-mail', 'buddyx');

    return $fields;
});

/**
 * Change address fields of the plugin Brazilian Market on WooCommerce
 */
add_filter('wcbcf_billing_fields', function ($fields) {

    $fields['billing_persontype']['class'][0] = 'form-row-first';
    $fields['billing_cpf']['class'][0]        = 'form-row-last';
    $fields['billing_cnpj']['class'][0]       = 'form-row-last';
    $fields['billing_company']['priority']    = 27;

    return $fields;
});

/**
 * Change the PayPal logo
 */
add_filter('woocommerce_gateway_icon', function ($icon) {

    if (strpos($icon, 'paypal.png')) {
        $icon = str_replace('plugins/woocommerce/includes/gateways/paypal/assets/images/paypal.png', 'themes/' . wp_get_theme()->get_stylesheet() . '/assets/images/paypal.svg', $icon);
    }

    return $icon;
});

add_action('woocommerce_before_thankyou', function ($order_id) {
    echo '<h2 class="title-thankyou">' . __('Finalização do pedido', 'buddyx') . '</h2>';
});

/**
 * Add description on the order received page
 */
add_filter('woocommerce_bacs_accounts', function ($fields) {

    $add_field = [
        [
            'bank_name'      => __('Para concluir seu pedido, basta realizar o pagamento. Seguem os dados de nossa conta bancária:', 'buddyx'),
            'account_name'   => '',
            'account_number' => '',
            'sort_code'      => '',
            'iban'           => '',
            'bic'            => ''
        ]
    ];

    return array_merge($add_field, $fields);
});


/**
 * 
 * Checkbox terms & conditions in register form
 * 
 */
// Add term and conditions check box on registration form
function add_terms_and_conditions_to_registration()
{

    if (wc_get_page_id('terms') > 0 && is_account_page()) {
    ?>
        <p class="form-row terms wc-terms-and-conditions">
            <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
                <input type="checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" name="terms" <?php checked(apply_filters('woocommerce_terms_is_checked_default', isset($_POST['terms'])), true); ?> id="terms" /> <span><?php printf('Eu li e aceito os <a href="%s" target="_blank" class="woocommerce-terms-and-conditions-link">termos e condições</a>', esc_url(wc_get_page_permalink('terms'))); ?></span> <span class="required">*</span>
            </label>
            <input type="hidden" name="terms-field" value="1" />
        </p>
<?php
    }
}
// Validate required term and conditions check box
function terms_and_conditions_validation($username, $email, $validation_errors)
{
    if (!isset($_POST['terms']))
        $validation_errors->add('terms_error', 'Para se registrar é necessário concordar com nossos termos e condições');

    return $validation_errors;
}
add_action('woocommerce_register_form', 'add_terms_and_conditions_to_registration', 20);
add_action('woocommerce_register_post', 'terms_and_conditions_validation', 20, 3);


if (is_plugin_active('pmpro-woocommerce/pmpro-woocommerce.php') || is_plugin_active('paid-memberships-pro/paid-memberships-pro.php') ) {
    require __DIR__ . '/library/woocommerce.php';
}


function description_archive_courses(){

    //Cursos online
    if(is_tax('course_tag','cursos-online') ){
        echo '
        São cursos on-line transmitidos ao vivo (via Zoom), que ainda não começaram. Nessa modalidade, as aulas são também gravadas e ficam disponíveis por dois meses após a data de término de cada curso.';
    }

    //Cursos gravados
    if(is_tax('course_tag','cursos-gravados') ){
        
        echo '
        São cursos online que foram ministrados ao vivo na Sala Jaú e que podem ser assistidos por meio das gravações.';
    }

    //Cursos em andamento
    else if(is_page_template('page-course-going.php')){
        
        echo '
        São cursos on-line transmitidos ao vivo (via Zoom), que estão em andamento ou que já terminaram, mas que seguem com inscrições abertas enquanto as gravações estiverem disponíveis.';
    }

     //Cursos em andamento
     else if(is_page_template('page-course-open.php')){
        
        echo '
        São cursos on-line novos com inscrições abertas.';
    }


}
add_action('lp/template/archive-course/description', 'description_archive_courses', 20);


/**function salajau_redirect_archive() {
    $status = get_post_meta( $id, '_lp_custom_course_status', true );

    if ( $status === 'on_going'){
        wp_redirect( home_url( 'em-andamento' ) );
        exit;
    }

add_action( 'template_redirect', 'salajau_redirect_archive', 10 );
}*/

function learn_press_user_course_status( $user_id = 0, $course_id = 0 ) {
	if ( ! $user = learn_press_get_user( $user_id ? $user_id : get_current_user_id() ) ) {
		return false;
	}

	if ( ! $userCourse = $user->get_course_data( $course_id ) ) {
		return false;
	}

	return $userCourse->get_status();
}

/**
 * @return array
 * @since 4.0.0
 */
function learn_press_course_enrolled_slugs() {
	return apply_filters(
		'learn-press/course-enrolled-slugs',
		array(
			'passed',
			'failed',
			'in-progress',
			'enrolled',
			'finished', // deprecated
		)
	);
}

function salajau_disable_button_finished_course(bool $can_show, LP_User $user, LP_Course $course ): bool  {
    global $post;
    $LP_WC_Hooks = LP_WC_Hooks::instance();
    $course_status = get_post_meta($post->ID, '_lp_custom_course_status', true);
    if ($course_status === 'finished' ) {
        remove_action('learn-press/course-buttons', array($LP_WC_Hooks, 'btn_add_to_cart'));
        learn_press_display_message('curso encerrado');
        return false;
    }
    else if (!$can_show){
        return false;
    }
    else{
      return true;
    }
}

add_filter('learnpress/course/template/button-enroll/can-show', 'salajau_disable_button_finished_course', 10,3);
add_filter('learnpress/course/template/button-purchase/can-show', 'salajau_disable_button_finished_course', 10,3);

// Desconto progressivo

// Adiciona o menu de configurações de desconto no painel de administração
function adicionar_menu_desconto_progressivo() {
    add_menu_page(
        'Configurações de Desconto Progressivo', 
        'Desconto Progressivo', 
        'manage_options', 
        'desconto-progressivo', 
        'render_configuracao_desconto_progressivo',
        'dashicons-admin-generic'
    );
}
add_action('admin_menu', 'adicionar_menu_desconto_progressivo');

// Função que renderiza a página de configurações
function render_configuracao_desconto_progressivo() {
    ?>
    <div class="wrap">
        <h1><?php _e('Configurações de Desconto Progressivo', 'woocommerce'); ?></h1>
        <form method="post" action="options.php">
            <?php
            settings_fields('desconto_progressivo_settings');
            do_settings_sections('desconto-progressivo');
            submit_button();
            ?>
        </form>
    </div>
    <?php
}

// Registrar configurações e campos
function registrar_configuracoes_desconto_progressivo() {
    register_setting('desconto_progressivo_settings', 'desconto_progressivo');

    add_settings_section(
        'desconto_progressivo_section',
        __('Configurações de Desconto', 'woocommerce'),
        null,
        'desconto-progressivo'
    );

    // Campo para desconto de 2 cursos
    add_settings_field(
        'desconto_2',
        __('Desconto para 2 cursos (%)', 'woocommerce'),
        'render_input_desconto_2',
        'desconto-progressivo',
        'desconto_progressivo_section'
    );

    // Campo para desconto de 3 cursos ou mais
    add_settings_field(
        'desconto_3_mais',
        __('Desconto para 3 cursos ou mais (%)', 'woocommerce'),
        'render_input_desconto_3_mais',
        'desconto-progressivo',
        'desconto_progressivo_section'
    );
}
add_action('admin_init', 'registrar_configuracoes_desconto_progressivo');

// Funções para renderizar os campos de entrada
function render_input_desconto_2() {
    $value = get_option('desconto_progressivo')['desconto_2'] ?? '';
    echo '<input type="number" name="desconto_progressivo[desconto_2]" value="' . esc_attr($value) . '" />';
}

function render_input_desconto_3_mais() {
    $value = get_option('desconto_progressivo')['desconto_3_mais'] ?? '';
    echo '<input type="number" name="desconto_progressivo[desconto_3_mais]" value="' . esc_attr($value) . '" />';
}

// Função para aplicar o desconto progressivo
function aplicar_desconto_progressivo( $cart ) {
    if ( is_admin() || ! is_cart() && ! is_checkout() ) {
        return;
    }

    // Verificar se há um cupom válido aplicado
    if ( $cart->get_applied_coupons() ) {
        return; // Se houver um cupom aplicado, não aplicar o desconto progressivo
    }

    // Recuperar as faixas de desconto configuradas pelo cliente
    $descontos_configurados = get_option('desconto_progressivo', array());
    $desconto_2 = isset( $descontos_configurados['desconto_2'] ) && ! empty( $descontos_configurados['desconto_2'] ) ? $descontos_configurados['desconto_2'] : 0;
    $desconto_3_mais = isset( $descontos_configurados['desconto_3_mais'] ) && ! empty( $descontos_configurados['desconto_3_mais'] ) ? $descontos_configurados['desconto_3_mais'] : 0;

    // Se ambos os campos de desconto estiverem vazios, não aplicar o desconto progressivo
    if ( $desconto_2 == 0 && $desconto_3_mais == 0 ) {
        return;
    }

    $valor_total_cursos = 0;
    $quantidade_cursos = 0;

    // Iterar pelos itens do carrinho
    foreach ( $cart->get_cart() as $cart_item ) {
        $produto = $cart_item['data'];
        $quantidade = $cart_item['quantity'];

        // Verificar se o produto não está na categoria "atividades" (taxonomia 'course_tag')
        if ( ! has_term( 'atividades', 'course_tag', $produto->get_id() ) ) {
            // Somar o valor total do curso (quantidade x preço)
            $valor_total_cursos += $produto->get_price() * $quantidade;
            $quantidade_cursos += $quantidade;
        }
    }

    // Aplicar o desconto de acordo com a quantidade de cursos
    if ( $quantidade_cursos >= 2 ) {
        $desconto = 0;

        if ( $quantidade_cursos == 2 && $desconto_2 > 0 ) {
            $desconto = $desconto_2 / 100; // Desconto configurado para 2 cursos
        } elseif ( $quantidade_cursos >= 3 && $desconto_3_mais > 0 ) {
            $desconto = $desconto_3_mais / 100; // Desconto configurado para 3 cursos ou mais
        }

        // Se houver um desconto aplicável, adicionar a taxa
        if ( $desconto > 0 ) {
            $desconto_total = $valor_total_cursos * $desconto;

            // Adicionar o desconto progressivo ao carrinho
            $cart->add_fee( sprintf( __( 'Desconto Progressivo (%s%%)', 'woocommerce' ), $desconto * 100 ), -$desconto_total );
        }
    }
}
add_action( 'woocommerce_cart_calculate_fees', 'aplicar_desconto_progressivo' );


